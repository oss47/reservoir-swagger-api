/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model263 {
    #[serde(rename = "marketplaces", skip_serializing_if = "Option::is_none")]
    pub marketplaces: Option<Vec<models::Model261>>,
}

impl Model263 {
    pub fn new() -> Model263 {
        Model263 { marketplaces: None }
    }
}
