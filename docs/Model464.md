# Model464

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule_id** | **f64** | The rule ID to update | 
**tier** | Option<**f64**> |  | [optional]
**points** | Option<**f64**> |  | [optional]
**points_to_consume** | Option<**f64**> |  | [optional]
**duration** | Option<**f64**> |  | [optional]
**api_key** | Option<**String**> |  | [optional]
**method** | Option<**String**> |  | [optional]
**payload** | Option<[**Vec<models::Model431>**](Model431.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


