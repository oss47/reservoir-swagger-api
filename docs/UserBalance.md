# UserBalance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_chain** | **String** |  | 
**all_chains** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


