# \TokensApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_tokens_bootstrap_v1**](TokensApi.md#get_tokens_bootstrap_v1) | **GET** /tokens/bootstrap/v1 | Token Events Bootstrap
[**get_tokens_flag_changes_v1**](TokensApi.md#get_tokens_flag_changes_v1) | **GET** /tokens/flag/changes/v1 | Flagged Tokens
[**get_tokens_floor_v1**](TokensApi.md#get_tokens_floor_v1) | **GET** /tokens/floor/v1 | Token Prices
[**get_tokens_ids_v1**](TokensApi.md#get_tokens_ids_v1) | **GET** /tokens/ids/v1 | Token IDs
[**get_tokens_v7**](TokensApi.md#get_tokens_v7) | **GET** /tokens/v7 | Tokens
[**get_users_user_tokens_v10**](TokensApi.md#get_users_user_tokens_v10) | **GET** /users/{user}/tokens/v10 | User Tokens
[**post_tokens_refresh_v2**](TokensApi.md#post_tokens_refresh_v2) | **POST** /tokens/refresh/v2 | Refresh Token
[**post_tokensets_v2**](TokensApi.md#post_tokensets_v2) | **POST** /token-sets/v2 | Create token set



## get_tokens_bootstrap_v1

> models::GetTokensBootstrapV1Response get_tokens_bootstrap_v1(collection, contract, continuation, limit)
Token Events Bootstrap

Get the latest price event per token in a collection, so that you can listen to future events and keep track of prices

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 500]

### Return type

[**models::GetTokensBootstrapV1Response**](getTokensBootstrapV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_flag_changes_v1

> models::GetFlaggedTokensV1Response get_tokens_flag_changes_v1(flag_status, limit, continuation)
Flagged Tokens

This API return the recent flagged/un-flagged tokens across all collections sorted by change time

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**flag_status** | Option<**f64**> | -1 = All tokens (default) 0 = Non flagged tokens 1 = Flagged tokens |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max is 200. |  |[default to 200]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetFlaggedTokensV1Response**](getFlaggedTokensV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_floor_v1

> models::GetTokensFloorV1Response get_tokens_floor_v1(collection, contract)
Token Prices

This API will return the best price of every token in a collection that is currently on sale. Note: Prices are returned in the native currency of the network.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |

### Return type

[**models::GetTokensFloorV1Response**](getTokensFloorV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_ids_v1

> models::GetTokensIdsV1Response get_tokens_ids_v1(collection, contract, token_set_id, flag_status, limit, continuation)
Token IDs

This API is optimized for quickly fetching a list of tokens ids in by collection, contract, token set id. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token_set_id** | Option<**String**> | Filter to a particular token set. Example: `token:CONTRACT:TOKEN_ID` representing a single token within contract, `contract:CONTRACT` representing a whole contract, `range:CONTRACT:START_TOKEN_ID:END_TOKEN_ID` representing a continuous token id range within a contract and `list:CONTRACT:TOKEN_IDS_HASH` representing a list of token ids within a contract. |  |
**flag_status** | Option<**f64**> | -1 = All tokens (default) 0 = Non flagged tokens 1 = Flagged tokens |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 10,000. |  |[default to 100]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetTokensIdsV1Response**](getTokensIdsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_v7

> models::GetTokensV7Response get_tokens_v7(collection, token_name, tokens, attributes, source, native_source, min_rarity_rank, max_rarity_rank, min_floor_ask_price, max_floor_ask_price, flag_status, collections_set_id, community, contract, token_set_id, sort_by, sort_direction, currencies, limit, start_timestamp, end_timestamp, include_top_bid, include_mint_stages, exclude_eoa, exclude_spam, exclude_nsfw, include_attributes, include_quantity, include_dynamic_pricing, include_last_sale, normalize_royalties, continuation, display_currency)
Tokens

Get a list of tokens with full metadata. This is useful for showing a single token page, or scenarios that require more metadata.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token_name** | Option<**String**> | Filter to a particular token by name. This is case sensitive. Example: `token #1` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Attributes are case sensitive. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/tokens/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/tokens/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**source** | Option<**String**> | Domain of the order source. Example `opensea.io` (Only listed tokens are returned when filtering by source) |  |
**native_source** | Option<**String**> | Domain of the order source. Example `www.apecoinmarketplace.com`. For a native marketplace, return all tokens listed on this marketplace, even if better prices are available on other marketplaces. |  |
**min_rarity_rank** | Option<**i32**> | Get tokens with a min rarity rank (inclusive), no rarity rank for collections over 100k |  |
**max_rarity_rank** | Option<**i32**> | Get tokens with a max rarity rank (inclusive), no rarity rank for collections over 100k |  |
**min_floor_ask_price** | Option<**f64**> | Get tokens with a min floor ask price (inclusive); use native currency |  |
**max_floor_ask_price** | Option<**f64**> | Get tokens with a max floor ask price (inclusive); use native currency |  |
**flag_status** | Option<**f64**> | Allowed only with collection and tokens filtering! -1 = All tokens (default) 0 = Non flagged tokens 1 = Flagged tokens |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**token_set_id** | Option<**String**> | Filter to a particular token set. `Example: token:0xa7d8d9ef8d8ce8992df33d8b8cf4aebabd5bd270:129000685` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `floorAskPrice`, `tokenId`, `rarity`, and `updatedAt`. No rarity rank for collections over 100k. |  |[default to floorAskPrice]
**sort_direction** | Option<**String**> |  |  |
**currencies** | Option<[**Vec<String>**](String.md)> | Filter to tokens with a listing in a particular currency. Max limit is 50. `Example: currencies[0]: 0x0000000000000000000000000000000000000000` |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 100, except when sorting by `updatedAt` which has a limit of 1000. |  |[default to 20]
**start_timestamp** | Option<**f64**> | When sorting by `updatedAt`, the start timestamp you want to filter on (UTC). |  |
**end_timestamp** | Option<**f64**> | When sorting by `updatedAt`, the end timestamp you want to filter on (UTC). |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_mint_stages** | Option<**bool**> | If true, mint data for the tokens will be included in the response. |  |[default to false]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. defaults to false |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any tokens marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any tokens marked as nsfw. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be returned in the response. |  |[default to false]
**include_quantity** | Option<**bool**> | If true, quantity filled and quantity remaining will be returned in the response. |  |[default to false]
**include_dynamic_pricing** | Option<**bool**> | If true, dynamic pricing data will be returned in the response. |  |[default to false]
**include_last_sale** | Option<**bool**> | If true, last sale data including royalties paid will be returned in the response. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |

### Return type

[**models::GetTokensV7Response**](getTokensV7Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v10

> models::GetUserTokensV10Response get_users_user_tokens_v10(user, community, collections_set_id, collection, exclude_collections, contract, tokens, normalize_royalties, sort_by, sort_direction, continuation, limit, include_top_bid, include_attributes, include_last_sale, include_raw_data, include_dynamic_pricing, exclude_spam, exclude_nsfw, only_listed, use_non_flagged_floor_ask, display_currency, token_name)
User Tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**collection** | Option<[**Vec<String>**](String.md)> | Array of collections. Max limit is 100. Example: `collections[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**exclude_collections** | Option<[**Vec<String>**](String.md)> |  |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `acquiredAt`, `lastAppraisalValue` and `floorAskPrice`. `lastAppraisalValue` is the value of the last sale. `floorAskPrice` is the collection floor ask |  |[default to acquiredAt]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 200. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be returned in the response. |  |[default to false]
**include_last_sale** | Option<**bool**> | If true, last sale data including royalties paid will be returned in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**include_dynamic_pricing** | Option<**bool**> | If true, dynamic pricing data will be returned in the response. |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any tokens marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any tokens marked as nsfw. |  |[default to false]
**only_listed** | Option<**bool**> | If true, will filter any tokens that are not listed |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will return the collection non flagged floor ask. |  |[default to false]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |
**token_name** | Option<**String**> | Filter to a particular token by name. This is case sensitive. Example: `token #1` |  |

### Return type

[**models::GetUserTokensV10Response**](getUserTokensV10Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokens_refresh_v2

> models::PostTokensRefreshV2Response post_tokens_refresh_v2(body)
Refresh Token

Token metadata is never automatically refreshed, but may be manually refreshed with this API.  Caution: This API should be used in moderation, like only when missing data is discovered. Calling it in bulk or programmatically will result in your API key getting rate limited.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model614**](Model614.md)> |  |  |

### Return type

[**models::PostTokensRefreshV2Response**](postTokensRefreshV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokensets_v2

> models::Model285 post_tokensets_v2(body)
Create token set

Use this API to create a `tokenSetId` to call specific tokens from a collection. Adding or removing a tokenId will change the response. See an example below.   Input of `0xd774557b647330c91bf44cfeab205095f7e6c367:1` and `0xd774557b647330c91bf44cfeab205095f7e6c367:2`   Output of `list:0xd774557b647330c91bf44cfeab205095f7e6c367:0xb6fd98eeb7e08fc521f11511289afe4d8e873fd7a3fb76ab757fa47c23f596e9`   Notes:  - Include `list:` when using this `tokenSetId` for it to work successfully.  - You cannot adjust tokens within a `tokenSetId`. Please create a new set.  - Use the `/tokens/ids` endpoint to get a list of tokens within a set.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model486**](Model486.md)> |  |  |

### Return type

[**models::Model285**](Model285.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

