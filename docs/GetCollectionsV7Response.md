# GetCollectionsV7Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**continuation** | Option<**String**> |  | [optional]
**collections** | Option<[**Vec<models::Model37>**](Model37.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


