# Stats

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_count** | **f64** |  | 
**on_sale_count** | **f64** |  | 
**sample_images** | Option<**Vec<String>**> |  | [optional]
**market** | Option<[**models::Market**](market.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


