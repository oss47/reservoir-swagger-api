# GetCollectionOwnersDistributionV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owners_distribution** | Option<[**Vec<models::Model268>**](Model268.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


