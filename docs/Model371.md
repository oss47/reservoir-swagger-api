# Model371

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_count** | Option<**String**> |  | [optional]
**on_sale_count** | Option<**String**> |  | [optional]
**floor_sell_value** | Option<**f64**> |  | [optional]
**acquired_at** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


