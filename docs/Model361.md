# Model361

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_url** | Option<**String**> |  | [optional]
**discord_url** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**external_url** | Option<**String**> |  | [optional]
**banner_image_url** | Option<**String**> |  | [optional]
**twitter_username** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


