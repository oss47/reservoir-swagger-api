# Model72

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | **String** |  | 
**token_id** | **String** |  | 
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**media** | Option<**String**> |  | [optional]
**collection** | Option<[**models::Model69**](Model69.md)> |  | [optional]
**source** | Option<**String**> |  | [optional]
**source_domain** | Option<**String**> |  | [optional]
**top_bid_value** | Option<**f64**> |  | [optional]
**floor_ask_price** | Option<**f64**> |  | [optional]
**rarity** | Option<**f64**> |  | [optional]
**rarity_rank** | Option<**f64**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**is_flagged** | Option<**bool**> |  | [optional][default to false]
**last_flag_update** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


