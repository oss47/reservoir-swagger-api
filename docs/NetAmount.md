# NetAmount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**raw** | Option<**String**> |  | [optional]
**decimal** | Option<**f64**> |  | [optional]
**usd** | Option<**f64**> |  | [optional]
**native** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


