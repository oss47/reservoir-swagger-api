# Model612

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | Refresh the given token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | 
**liquidity_only** | Option<**bool**> | If true, only liquidity data will be refreshed. | [optional][default to false]
**override_cool_down** | Option<**bool**> | If true, will force a refresh regardless of cool down. Requires an authorized api key to be passed. | [optional][default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


