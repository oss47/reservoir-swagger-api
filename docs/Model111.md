# Model111

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate_limits** | Option<[**Vec<models::Model110>**](Model110.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


