# Model535

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | Option<**String**> |  | [optional]
**recipient** | Option<**String**> |  | [optional]
**bps** | Option<**f64**> |  | [optional]
**amount** | Option<**f64**> |  | [optional]
**raw_amount** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


