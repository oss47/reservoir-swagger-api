/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model112 {
    #[serde(rename = "1day", skip_serializing_if = "Option::is_none")]
    pub param_1day: Option<Box<models::Model1day>>,
    #[serde(rename = "7day", skip_serializing_if = "Option::is_none")]
    pub param_7day: Option<Box<models::Model1day>>,
}

impl Model112 {
    pub fn new() -> Model112 {
        Model112 {
            param_1day: None,
            param_7day: None,
        }
    }
}
