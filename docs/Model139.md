# Model139

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid** | Option<[**crate::models::Model138**](Model138.md)> |  | [optional]
**event** | Option<[**crate::models::Event**](event.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


