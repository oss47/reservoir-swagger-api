# Model137

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid** | Option<[**crate::models::Model136**](Model136.md)> |  | [optional]
**event** | Option<[**crate::models::Model128**](Model128.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


