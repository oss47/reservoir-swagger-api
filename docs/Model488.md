# Model488

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Returns `complete` or `incomplete`. | 
**tip** | Option<**String**> |  | [optional]
**data** | Option<[**serde_json::Value**](.md)> |  | [optional]
**order_indexes** | Option<**Vec<f32>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


