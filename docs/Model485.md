# Model485

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**quantity** | Option<**f32**> |  | [optional]
**source** | Option<**String**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**currency_symbol** | Option<**String**> |  | [optional]
**currency_decimals** | Option<**f32**> |  | [optional]
**quote** | Option<**f32**> |  | [optional]
**raw_quote** | Option<**String**> |  | [optional]
**buy_in_currency** | Option<**String**> |  | [optional]
**buy_in_currency_symbol** | Option<**String**> |  | [optional]
**buy_in_currency_decimals** | Option<**f32**> |  | [optional]
**buy_in_quote** | Option<**f32**> |  | [optional]
**buy_in_raw_quote** | Option<**String**> |  | [optional]
**buy_in** | Option<[**Vec<crate::models::Price>**](price.md)> |  | [optional]
**total_price** | Option<**f32**> |  | [optional]
**total_raw_price** | Option<**String**> |  | [optional]
**gas_cost** | Option<**String**> |  | [optional]
**built_in_fees** | Option<[**Vec<crate::models::Model483>**](Model483.md)> | Can be marketplace fees or royalties | [optional]
**fees_on_top** | Option<[**Vec<crate::models::Model483>**](Model483.md)> | Can be referral fees. | [optional]
**from_chain_id** | Option<**f32**> | Chain id buying from | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


