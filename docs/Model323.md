# Model323

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | Option<**String**> | Case sensitive | [optional]
**kind** | Option<**String**> | Can be `string`, `number, `date, or `range`. | [optional]
**value** | **String** | Case sensitive. | 
**token_count** | Option<**f64**> |  | [optional]
**on_sale_count** | Option<**f64**> |  | [optional]
**floor_ask_price** | Option<[**models::Model82**](Model82.md)> |  | [optional]
**top_bid_value** | Option<**f64**> | Can be null. | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


