/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model153 {
    #[serde(rename = "bid", skip_serializing_if = "Option::is_none")]
    pub bid: Option<Box<models::Bid>>,
    #[serde(rename = "event", skip_serializing_if = "Option::is_none")]
    pub event: Option<Box<models::Event>>,
}

impl Model153 {
    pub fn new() -> Model153 {
        Model153 {
            bid: None,
            event: None,
        }
    }
}
