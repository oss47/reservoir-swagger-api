# Time

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | Option<**f64**> |  | [optional]
**end** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


