# Model443

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**queue_name** | **String** | The queue name to resume | 
**all_chains** | Option<**bool**> |  | [optional][default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


