# Model138

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Collection id | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**banner** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**primary_contract** | Option<**String**> |  | [optional]
**count** | Option<**i32**> |  | [optional]
**volume** | Option<**f64**> |  | [optional]
**volume_percent_change** | Option<**f64**> |  | [optional]
**count_percent_change** | Option<**f64**> |  | [optional]
**floor_ask** | Option<[**models::Model19**](Model19.md)> |  | [optional]
**token_count** | Option<**f64**> | Total tokens within the collection. | [optional]
**owner_count** | Option<**f64**> | Unique number of owners. | [optional]
**volume_change** | Option<[**models::Model28**](Model28.md)> |  | [optional]
**recent_sales** | Option<[**Vec<models::Model135>**](Model135.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


