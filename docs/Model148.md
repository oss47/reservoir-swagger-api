# Model148

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | Option<[**models::Model147**](Model147.md)> |  | [optional]
**event** | Option<[**models::Event**](event.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


