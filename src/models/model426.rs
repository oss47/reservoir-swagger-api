/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model426 {
    #[serde(rename = "contract", skip_serializing_if = "Option::is_none")]
    pub contract: Option<String>,
    #[serde(rename = "tokenId", skip_serializing_if = "Option::is_none")]
    pub token_id: Option<String>,
    #[serde(rename = "kind", skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "image", skip_serializing_if = "Option::is_none")]
    pub image: Option<String>,
    #[serde(rename = "floorAskPrice", skip_serializing_if = "Option::is_none")]
    pub floor_ask_price: Option<Box<models::Price>>,
    #[serde(rename = "lastSalePrice", skip_serializing_if = "Option::is_none")]
    pub last_sale_price: Option<Box<models::Price>>,
    #[serde(rename = "collection", skip_serializing_if = "Option::is_none")]
    pub collection: Option<Box<models::Model425>>,
}

impl Model426 {
    pub fn new() -> Model426 {
        Model426 {
            contract: None,
            token_id: None,
            kind: None,
            name: None,
            image: None,
            floor_ask_price: None,
            last_sale_price: None,
            collection: None,
        }
    }
}
