# Model135

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**timestamp** | Option<**f64**> |  | [optional]
**to_address** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**collection** | Option<[**models::Model134**](Model134.md)> |  | [optional]
**token** | Option<[**models::Model134**](Model134.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


