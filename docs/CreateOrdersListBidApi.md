# \CreateOrdersListBidApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_crosspostingorders_v1**](CreateOrdersListBidApi.md#get_crosspostingorders_v1) | **GET** /cross-posting-orders/v1 | Check cross posting status
[**get_transactions_txhash_synced_v1**](CreateOrdersListBidApi.md#get_transactions_txhash_synced_v1) | **GET** /transactions/{txHash}/synced/v1 | Transaction status
[**post_execute_bid_v5**](CreateOrdersListBidApi.md#post_execute_bid_v5) | **POST** /execute/bid/v5 | Create bids (offers)
[**post_execute_cancel_v3**](CreateOrdersListBidApi.md#post_execute_cancel_v3) | **POST** /execute/cancel/v3 | Cancel orders
[**post_execute_list_v5**](CreateOrdersListBidApi.md#post_execute_list_v5) | **POST** /execute/list/v5 | Create asks (listings)
[**post_order_v4**](CreateOrdersListBidApi.md#post_order_v4) | **POST** /order/v4 | Submit signed orders



## get_crosspostingorders_v1

> crate::models::GetCrossPostingOrdersV1Response get_crosspostingorders_v1(ids, continuation, limit)
Check cross posting status

This API can be used to check the status of cross posted listings and bids.   Input your `crossPostingOrderId` into the `ids` param and submit for the status.    The `crossPostingOrderId` is returned in the `execute/bids` and `execute/asks` response as well as the `onProgess` callback for the SDK.    Note: ReservoirKit does not return a `crossPostingOrderId`.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<[**Vec<f32>**](f32.md)> |  |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**crate::models::GetCrossPostingOrdersV1Response**](getCrossPostingOrdersV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_transactions_txhash_synced_v1

> crate::models::GetTransactionSyncedV1Response get_transactions_txhash_synced_v1(tx_hash)
Transaction status

Get a boolean response on whether a particular transaction was synced or not.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**tx_hash** | **String** |  | [required] |

### Return type

[**crate::models::GetTransactionSyncedV1Response**](getTransactionSyncedV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_bid_v5

> crate::models::GetExecuteBidV5Response post_execute_bid_v5(body)
Create bids (offers)

Generate bids and submit them to multiple marketplaces.   Notes:  - Please use the `/cross-posting-orders/v1` to check the status on cross posted bids.  - We recommend using Reservoir SDK as it abstracts the process of iterating through steps, and returning callbacks that can be used to update your UI.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model451**](Model451.md)> |  |  |

### Return type

[**crate::models::GetExecuteBidV5Response**](getExecuteBidV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_cancel_v3

> crate::models::GetExecuteCancelV3Response post_execute_cancel_v3(body)
Cancel orders

Cancel existing orders on any marketplace

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model489**](Model489.md)> |  |  |

### Return type

[**crate::models::GetExecuteCancelV3Response**](getExecuteCancelV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_list_v5

> crate::models::GetExecuteListV5Response post_execute_list_v5(body)
Create asks (listings)

Generate listings and submit them to multiple marketplaces.   Notes:  - Please use the `/cross-posting-orders/v1` to check the status on cross posted bids.  - We recommend using Reservoir SDK as it abstracts the process of iterating through steps, and returning callbacks that can be used to update your UI.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model502**](Model502.md)> |  |  |

### Return type

[**crate::models::GetExecuteListV5Response**](getExecuteListV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_order_v4

> crate::models::PostOrderV4Response post_order_v4(signature, body)
Submit signed orders

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | Option<**String**> |  |  |
**body** | Option<[**Model426**](Model426.md)> |  |  |

### Return type

[**crate::models::PostOrderV4Response**](postOrderV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

