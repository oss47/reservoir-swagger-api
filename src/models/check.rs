/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use serde_json::Value;

use crate::models;

/// Check : The details of the endpoint for checking the status of the step
#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Check {
    #[serde(rename = "endpoint")]
    pub endpoint: String,
    #[serde(rename = "method")]
    pub method: Method,
    #[serde(rename = "body", skip_serializing_if = "Option::is_none")]
    pub body: Option<Value>,
}

impl Check {
    /// The details of the endpoint for checking the status of the step
    pub fn new(endpoint: String, method: Method) -> Check {
        Check {
            endpoint,
            method,
            body: None,
        }
    }
}
///
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum Method {
    #[serde(rename = "POST")]
    Post,
}

impl Default for Method {
    fn default() -> Method {
        Self::Post
    }
}
