# \AttributesApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_collections_collection_attributes_all_v4**](AttributesApi.md#get_collections_collection_attributes_all_v4) | **GET** /collections/{collection}/attributes/all/v4 | All attributes
[**get_collections_collection_attributes_explore_v4**](AttributesApi.md#get_collections_collection_attributes_explore_v4) | **GET** /collections/{collection}/attributes/explore/v4 | Explore attributes
[**get_collections_collection_attributes_explore_v5**](AttributesApi.md#get_collections_collection_attributes_explore_v5) | **GET** /collections/{collection}/attributes/explore/v5 | Explore attributes



## get_collections_collection_attributes_all_v4

> models::GetAttributesAllV4Response get_collections_collection_attributes_all_v4(collection, display_currency)
All attributes

Use this API to see all possible attributes within a collection.  - `floorAskPrice` for all attributes might not be returned on collections with more than 10k tokens.   - Attributes are case sensitive.   - Attributes will return a maximum of 500 values.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetAttributesAllV4Response**](getAttributesAllV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_explore_v4

> models::GetAttributesExploreV4Response get_collections_collection_attributes_explore_v4(collection, token_id, include_top_bid, exclude_range_traits, exclude_number_traits, attribute_key, max_floor_ask_prices, max_last_sells, continuation, limit)
Explore attributes

Use this API to see stats on a specific attribute within a collection. This endpoint will return `tokenCount`, `onSaleCount`, `sampleImages`, and `floorAsk` by default. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**token_id** | Option<**String**> | Filter to a particular token-id. Example: `1` |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**exclude_range_traits** | Option<**bool**> | If true, range traits will be excluded from the response. |  |[default to false]
**exclude_number_traits** | Option<**bool**> | If true, number traits will be excluded from the response. |  |[default to false]
**attribute_key** | Option<**String**> | Filter to a particular attribute key. Example: `Composition` |  |
**max_floor_ask_prices** | Option<**i32**> | Max number of items returned in the response. |  |[default to 1]
**max_last_sells** | Option<**i32**> | Max number of items returned in the response. |  |[default to 0]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Default limit is 20. Max limit is 5000. |  |[default to 20]

### Return type

[**models::GetAttributesExploreV4Response**](getAttributesExploreV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_explore_v5

> models::GetAttributesExploreV5Response get_collections_collection_attributes_explore_v5(collection, token_id, include_top_bid, exclude_range_traits, exclude_number_traits, attribute_key, max_floor_ask_prices, max_last_sells, continuation, limit)
Explore attributes

Use this API to see stats on a specific attribute within a collection. This endpoint will return `tokenCount`, `onSaleCount`, `sampleImages`, and `floorAskPrices` by default.  - `floorAskPrices` will not be returned on attributes with more than 10k tokens.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**token_id** | Option<**String**> | Filter to a particular token-id. Example: `1` |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**exclude_range_traits** | Option<**bool**> | If true, range traits will be excluded from the response. |  |[default to false]
**exclude_number_traits** | Option<**bool**> | If true, number traits will be excluded from the response. |  |[default to false]
**attribute_key** | Option<**String**> | Filter to a particular attribute key. Example: `Composition` |  |
**max_floor_ask_prices** | Option<**i32**> | Max number of items returned in the response. |  |[default to 1]
**max_last_sells** | Option<**i32**> | Max number of items returned in the response. |  |[default to 0]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Default limit is 20. Max limit is 5000. |  |[default to 20]

### Return type

[**models::GetAttributesExploreV5Response**](getAttributesExploreV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

