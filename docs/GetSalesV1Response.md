# GetSalesV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales** | Option<[**Vec<models::Model51>**](Model51.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


