/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model282 {
    #[serde(rename = "marketplaces", skip_serializing_if = "Option::is_none")]
    pub marketplaces: Option<Vec<models::Model280>>,
}

impl Model282 {
    pub fn new() -> Model282 {
        Model282 { marketplaces: None }
    }
}
