# Model511

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_ids** | Option<**Vec<String>**> |  | [optional]
**raw_orders** | Option<[**Vec<models::Model509>**](Model509.md)> |  | [optional]
**tokens** | Option<**Vec<String>**> | Array of tokens user is buying. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` | [optional]
**quantity** | Option<**i32**> | Quantity of tokens user is buying. Only compatible when buying a single ERC1155 token. Example: `5` | [optional]
**taker** | **String** | Address of wallet filling the order. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | 
**relayer** | Option<**String**> | Address of wallet relaying the filling transaction | [optional]
**only_path** | Option<**bool**> | If true, only the path will be returned. | [optional][default to false]
**force_router** | Option<**bool**> | If true, all fills will be executed through the router. | [optional]
**currency** | Option<**String**> |  | [optional][default to 0x0000000000000000000000000000000000000000]
**normalize_royalties** | Option<**bool**> |  | [optional][default to true]
**preferred_order_source** | Option<**String**> | If there are multiple listings with equal best price, prefer this source over others. NOTE: if you want to fill a listing that is not the best priced, you need to pass a specific order ID. | [optional]
**source** | Option<**String**> | Filling source used for attribution. Example: `reservoir.market` | [optional]
**fees_on_top** | Option<**Vec<String>**> | List of fees (formatted as `feeRecipient:feeAmount`) to be taken when filling. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:1000000000000000` | [optional]
**partial** | Option<**bool**> | If true, any off-chain or on-chain errors will be skipped. | [optional][default to false]
**max_fee_per_gas** | Option<**String**> | Optional. Set custom gas price. | [optional]
**max_priority_fee_per_gas** | Option<**String**> | Optional. Set custom gas price. | [optional]
**skip_balance_check** | Option<**bool**> | If true, balance check will be skipped. | [optional][default to false]
**x2y2_api_key** | Option<**String**> | Override the X2Y2 API key used for filling. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


