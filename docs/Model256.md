# Model256

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> | Possible types returned: `ask`, `ask_cancel`, `bid`, `bid_cancel`, `sale`, `mint, and `transfer`. | [optional]
**from_address** | Option<**String**> |  | [optional]
**to_address** | Option<**String**> |  | [optional]
**price** | Option<[**models::Model124**](Model124.md)> |  | [optional]
**amount** | Option<**f64**> |  | [optional]
**timestamp** | Option<**f64**> | Time when added on the blockchain. | [optional]
**contract** | Option<**String**> |  | [optional]
**token** | Option<[**models::Model254**](Model254.md)> |  | [optional]
**collection** | Option<[**models::Model255**](Model255.md)> |  | [optional]
**tx_hash** | Option<**String**> | Txn hash from the blockchain. | [optional]
**log_index** | Option<**f64**> |  | [optional]
**batch_index** | Option<**f64**> |  | [optional]
**fill_source** | Option<[**models::Source**](source.md)> |  | [optional]
**is_airdrop** | Option<**bool**> |  | [optional]
**order** | Option<[**models::Model127**](Model127.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


