# Model472

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | Option<[**models::Model471**](Model471.md)> |  | [optional]
**orderbook** | Option<**String**> |  | [optional][default to Reservoir]
**orderbook_api_key** | Option<**String**> | Optional API key for the target orderbook | [optional]
**source** | Option<**String**> | The source domain | [optional]
**attribute** | Option<[**models::Attribute**](attribute.md)> |  | [optional]
**collection** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**is_non_flagged** | Option<**bool**> |  | [optional]
**permit_id** | Option<**String**> |  | [optional]
**permit_index** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


