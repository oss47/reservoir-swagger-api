# Model147

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**status** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**quantity_remaining** | Option<**f64**> |  | [optional]
**nonce** | Option<**String**> |  | [optional]
**valid_from** | Option<**f64**> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]
**source** | Option<**String**> |  | [optional]
**is_dynamic** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


