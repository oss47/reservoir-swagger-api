# Model193

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**kind** | **String** |  | 
**side** | **String** |  | 
**status** | Option<**String**> |  | [optional]
**token_set_id** | **String** |  | 
**token_set_schema_hash** | **String** |  | 
**contract** | Option<**String**> |  | [optional]
**maker** | **String** |  | 
**taker** | **String** |  | 
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**valid_from** | **f64** |  | 
**valid_until** | **f64** |  | 
**quantity_filled** | Option<**f64**> |  | [optional]
**quantity_remaining** | Option<**f64**> |  | [optional]
**metadata** | Option<[**models::Model176**](Model176.md)> |  | [optional]
**source** | Option<[**serde_json::Value**](.md)> |  | [optional]
**fee_bps** | Option<**f64**> |  | [optional]
**fee_breakdown** | Option<[**Vec<models::Model177>**](Model177.md)> |  | [optional]
**expiration** | **f64** |  | 
**is_reservoir** | Option<**bool**> |  | [optional]
**created_at** | **String** |  | 
**updated_at** | **String** |  | 
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


