# GetTopSellingCollectionsV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collections** | Option<[**Vec<models::Model138>**](Model138.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


