# Details

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tx** | Option<[**models::Tx**](tx.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


