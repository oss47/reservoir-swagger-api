# \ChainApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_chain_stats_v1**](ChainApi.md#get_chain_stats_v1) | **GET** /chain/stats/v1 | Chain Stats



## get_chain_stats_v1

> models::GetChainStatsV5Response get_chain_stats_v1()
Chain Stats

Get chain mint and sales stats for 1 and 7 days

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::GetChainStatsV5Response**](getChainStatsV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

