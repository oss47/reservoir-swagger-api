# Model240

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<[**models::Model237**](Model237.md)> |  | [optional]
**market** | Option<[**models::Model239**](Model239.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


