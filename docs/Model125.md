# Model125

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_id** | Option<**String**> |  | [optional]
**token_name** | Option<**String**> |  | [optional]
**token_image** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional]
**is_nsfw** | Option<**bool**> |  | [optional]
**rarity_score** | Option<**f64**> |  | [optional]
**rarity_rank** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


