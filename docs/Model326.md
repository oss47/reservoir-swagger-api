# Model326

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**valid_from** | Option<**f64**> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]
**source** | Option<[**models::Source**](source.md)> |  | [optional]
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]
**is_native_off_chain_cancellable** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


