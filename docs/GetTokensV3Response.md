# GetTokensV3Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokens** | Option<[**Vec<models::Model70>**](Model70.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


