# Model394

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | Case sensitive | 
**value** | **String** | Case sensitive | 
**token_count** | **f64** | Total token count with this attribute. | 
**on_sale_count** | **f64** | Token count with this attribute on sale. | 
**sample_images** | Option<**Vec<String>**> |  | [optional]
**floor_ask_prices** | Option<**Vec<f64>**> | Current floor price ask. | [optional]
**last_buys** | Option<[**Vec<models::Model392>**](Model392.md)> |  | [optional]
**last_sells** | Option<[**Vec<models::Model392>**](Model392.md)> |  | [optional]
**top_bid** | Option<[**models::TopBid**](topBid.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


