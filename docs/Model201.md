# Model201

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | **String** |  | 
**token_id** | **String** |  | 
**name** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**collection** | Option<[**crate::models::Model64**](Model64.md)> |  | [optional]
**last_buy** | Option<[**crate::models::LastBuy**](lastBuy.md)> |  | [optional]
**last_sell** | Option<[**crate::models::LastBuy**](lastBuy.md)> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**attributes** | Option<[**Vec<crate::models::Model194>**](Model194.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


