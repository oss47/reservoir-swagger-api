# Model126

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection_id** | Option<**String**> |  | [optional]
**collection_name** | Option<**String**> |  | [optional]
**collection_image** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional]
**is_nsfw** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


