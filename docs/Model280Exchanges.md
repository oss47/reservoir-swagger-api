# Model280Exchanges

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**string** | Option<[**models::Model279**](Model279.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


