# GetUserCollectionsV3Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collections** | Option<[**Vec<models::Model366>**](Model366.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


