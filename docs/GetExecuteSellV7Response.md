# GetExecuteSellV7Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | Option<**String**> |  | [optional]
**steps** | Option<[**Vec<models::Model591>**](Model591.md)> |  | [optional]
**errors** | Option<[**Vec<models::Model533>**](Model533.md)> |  | [optional]
**path** | Option<[**Vec<models::Model593>**](Model593.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


