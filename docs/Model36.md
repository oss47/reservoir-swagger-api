# Model36

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**kind** | **String** |  | 
**side** | **String** |  | 
**fillability_status** | **String** |  | 
**approval_status** | **String** |  | 
**token_set_id** | **String** |  | 
**token_set_schema_hash** | **String** |  | 
**maker** | **String** |  | 
**taker** | **String** |  | 
**price** | **f32** |  | 
**value** | **f32** |  | 
**valid_from** | **f32** |  | 
**valid_until** | **f32** |  | 
**source_id** | Option<**String**> |  | [optional]
**fee_bps** | Option<**f32**> |  | [optional]
**fee_breakdown** | Option<[**Vec<crate::models::Model35>**](Model35.md)> |  | [optional]
**expiration** | **f32** |  | 
**created_at** | **String** |  | 
**updated_at** | **String** |  | 
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


