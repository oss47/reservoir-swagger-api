# Model178

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


