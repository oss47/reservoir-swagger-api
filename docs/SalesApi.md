# \SalesApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_sales_v6**](SalesApi.md#get_sales_v6) | **GET** /sales/v6 | Sales



## get_sales_v6

> models::GetSalesV4Response get_sales_v6(contract, tokens, include_token_metadata, include_deleted, collection, attributes, sort_by, sort_direction, tx_hash, start_timestamp, end_timestamp, limit, continuation)
Sales

Get recent sales for a contract or token. Paid mints are returned in this `sales` endpoint, free mints can be found in the `/activities/` endpoints. Array of contracts max limit is 20.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 20. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**include_token_metadata** | Option<**bool**> | If enabled, also include token metadata in the response. Default is false. |  |
**include_deleted** | Option<**bool**> | If enabled, include sales that have been deleted. In some cases the backfilling process deletes sales that are no longer relevant or have been reverted. |  |[default to false]
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Attributes are case sensitive. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/sales/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/sales/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `price`, `time`, and `updatedAt`. Default is `time`. |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**tx_hash** | Option<**String**> | Filter to a particular transaction. Example: `0x04654cc4c81882ed4d20b958e0eeb107915d75730110cce65333221439de6afc` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive). Relative to the sortBy time filters. |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive). Relative to the sortBy time filters. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 100]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetSalesV4Response**](getSalesV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

