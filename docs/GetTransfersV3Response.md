# GetTransfersV3Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transfers** | Option<[**Vec<models::Model106>**](Model106.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


