# Model226

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_id** | Option<**String**> |  | [optional]
**token_name** | Option<**String**> |  | [optional]
**token_image** | Option<**String**> |  | [optional]
**last_buy** | Option<[**crate::models::LastBuy**](lastBuy.md)> |  | [optional]
**last_sell** | Option<[**crate::models::LastBuy**](lastBuy.md)> |  | [optional]
**token_rarity_score** | Option<**f32**> |  | [optional]
**token_rarity_rank** | Option<**f32**> |  | [optional]
**token_media** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


