# \OrdersApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_orders_asks_v5**](OrdersApi.md#get_orders_asks_v5) | **GET** /orders/asks/v5 | Asks (listings)
[**get_orders_bids_v6**](OrdersApi.md#get_orders_bids_v6) | **GET** /orders/bids/v6 | Bids (offers)
[**get_orders_depth_v1**](OrdersApi.md#get_orders_depth_v1) | **GET** /orders/depth/v1 | Orders depth
[**get_orders_users_user_topbids_v4**](OrdersApi.md#get_orders_users_user_topbids_v4) | **GET** /orders/users/{user}/top-bids/v4 | User Top Bids



## get_orders_asks_v5

> models::GetOrdersAsksV4Response get_orders_asks_v5(ids, token, token_set_id, maker, community, collections_set_id, contracts_set_id, contracts, status, sources, native, include_private, include_criteria_metadata, include_raw_data, include_dynamic_pricing, exclude_eoa, exclude_sources, start_timestamp, end_timestamp, normalize_royalties, sort_by, sort_direction, continuation, limit, display_currency)
Asks (listings)

Get a list of asks (listings), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing.   To get all orders unflitered, select `sortBy` to `updatedAt`. No need to pass any other param. This will return any orders for any collections, token, attribute, etc.   Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<[**Vec<String>**](String.md)> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**maker** | Option<**String**> | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Requires `maker` to be passed. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**contracts** | Option<[**Vec<String>**](String.md)> |  |  |
**status** | Option<**String**> | activeª^º = currently valid inactiveª^ = temporarily invalid expiredª^, cancelledª^, filledª^ = permanently invalid anyªº = any status ª when an `id` is passed ^ when a `maker` is passed º when a `contract` is passed |  |
**sources** | Option<[**Vec<String>**](String.md)> |  |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**include_private** | Option<**bool**> | If true, private orders are included in the response. |  |
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**include_dynamic_pricing** | Option<**bool**> | If true, dynamic pricing data will be returned in the response. |  |[default to false]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. |  |[default to false]
**exclude_sources** | Option<[**Vec<String>**](String.md)> |  |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Sorting by `price` is ascending order only. |  |[default to createdAt]
**sort_direction** | Option<**String**> |  |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetOrdersAsksV4Response**](getOrdersAsksV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_bids_v6

> models::GetOrdersBidsV5Response get_orders_bids_v6(ids, token, token_set_id, maker, community, collections_set_id, contracts_set_id, collection, attribute, contracts, status, sources, order_type, native, include_private, include_criteria_metadata, include_raw_data, include_depth, start_timestamp, end_timestamp, exclude_eoa, exclude_sources, normalize_royalties, sort_by, sort_direction, continuation, limit, display_currency)
Bids (offers)

Get a list of bids (offers), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing.   There are a different kind of bids than can be returned:  - To get all orders unfiltered, select `sortBy` to `updatedAt`. No need to pass any other param. This will return any orders for any collections, token, attribute, etc.   - Inputting a 'contract' will return token and attribute bids.  - Inputting a 'collection-id' will return collection wide bids.  - Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<[**Vec<String>**](String.md)> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set. Example: `token:CONTRACT:TOKEN_ID` representing a single token within contract, `contract:CONTRACT` representing a whole contract, `range:CONTRACT:START_TOKEN_ID:END_TOKEN_ID` representing a continuous token id range within a contract and `list:CONTRACT:TOKEN_IDS_HASH` representing a list of token ids within a contract. |  |
**maker** | Option<**String**> | Filter to a particular user. Must set `sources=blur.io` to reveal maker's blur bids. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Requires `maker` to be passed. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**collection** | Option<**String**> | Filter to a particular collection bids with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attribute** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/orders/bids/v5?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original` or `https://api.reservoir.tools/orders/bids/v5?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original&attribute[Type]=Sibling`(Collection must be passed as well when filtering by attribute) |  |
**contracts** | Option<[**Vec<String>**](String.md)> | Filter to an array of contracts. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**status** | Option<**String**> | activeª^º = currently valid inactiveª^ = temporarily invalid valid^ = both active and inactive orders expiredª^, cancelledª^, filledª^ = permanently invalid anyªº = any status ª when an `id` is passed ^ when a `maker` is passed º when a `contract` is passed |  |
**sources** | Option<[**Vec<String>**](String.md)> |  |  |
**order_type** | Option<**String**> | Filter to a particular order type. Must be one of `token`, `collection`, `attribute`, `custom`. Only valid when a maker is specified. |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**include_private** | Option<**bool**> | If true, private orders are included in the response. |  |
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. Set `sources=blur.io` and make this `true` to reveal individual blur bids. |  |[default to false]
**include_depth** | Option<**bool**> | If true, the depth of each order is included in the response. |  |[default to false]
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. |  |[default to false]
**exclude_sources** | Option<[**Vec<String>**](String.md)> |  |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Sorting by `price` defaults sorting direction to descending.  |  |[default to createdAt]
**sort_direction** | Option<**String**> |  |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetOrdersBidsV5Response**](getOrdersBidsV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_depth_v1

> models::GetOrdersDepthV1Response get_orders_depth_v1(side, token, collection, display_currency)
Orders depth

Get the depth of a token or collection.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**side** | **String** |  | [required] |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123`. |  |
**collection** | Option<**String**> | Filter to a particular collection. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63`. |  |
**display_currency** | Option<**String**> | Return all prices in this currency. |  |

### Return type

[**models::GetOrdersDepthV1Response**](getOrdersDepthV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_users_user_topbids_v4

> models::GetUserTopBidsV4Response get_orders_users_user_topbids_v4(user, collection, contracts_set_id, community, collections_set_id, optimize_checkout_url, include_criteria_metadata, exclude_eoa, normalize_royalties, use_non_flagged_floor_ask, continuation, sort_by, sort_direction, limit, sample_size, display_currency)
User Top Bids

Return the top bids for the given user tokens. Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**collection** | Option<**String**> |  |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**optimize_checkout_url** | Option<**bool**> | If true, urls will only be returned for optimized sources that support royalties. |  |[default to false]
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to true]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will return the collection non flagged floor ask events. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**sort_by** | Option<**String**> | Order of the items are returned in the response. Options are `topBidValue`, `dateCreated`, `orderExpiry`, and `floorDifferencePercentage`. |  |[default to topBidValue]
**sort_direction** | Option<**String**> |  |  |[default to desc]
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 100 |  |[default to 20]
**sample_size** | Option<**i32**> | Amount of tokens considered. Min is 1000, max is default. |  |[default to 10000]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetUserTopBidsV4Response**](getUserTopBidsV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

