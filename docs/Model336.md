# Model336

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**last_buy** | Option<[**models::LastBuy**](lastBuy.md)> |  | [optional]
**last_sell** | Option<[**models::LastBuy**](lastBuy.md)> |  | [optional]
**rarity_score** | Option<**f64**> |  | [optional]
**rarity_rank** | Option<**f64**> |  | [optional]
**media** | Option<**String**> |  | [optional]
**collection** | Option<[**models::Model334**](Model334.md)> |  | [optional]
**top_bid** | Option<[**models::Model335**](Model335.md)> |  | [optional]
**last_appraisal_value** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


