# Model422

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**price** | Option<**f64**> |  | [optional]
**value** | Option<**f64**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**valid_from** | Option<**f64**> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]
**floor_difference_percentage** | Option<**f64**> |  | [optional]
**source** | Option<[**models::Source**](source.md)> |  | [optional]
**fee_breakdown** | Option<[**Vec<models::Model177>**](Model177.md)> |  | [optional]
**criteria** | Option<[**models::Model120**](Model120.md)> |  | [optional]
**token** | Option<[**models::Model417**](Model417.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


