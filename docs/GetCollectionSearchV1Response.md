# GetCollectionSearchV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collections** | Option<[**Vec<models::Model132>**](Model132.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


