# Model604

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**signer** | **String** |  | 
**endpoint** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


