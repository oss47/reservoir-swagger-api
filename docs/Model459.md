# Model459

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_block** | **i32** |  | 
**to_block** | **i32** |  | 
**sync_details** | Option<[**models::Model458**](Model458.md)> |  | [optional]
**blocks_per_batch** | Option<**i32**> |  | [optional]
**backfill** | Option<**bool**> |  | [optional][default to true]
**sync_events_only** | Option<**bool**> |  | [optional][default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


