# GetOwnersV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owners** | Option<[**Vec<models::Model47>**](Model47.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


