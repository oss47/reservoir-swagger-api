/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model507 {
    #[serde(rename = "abiType")]
    pub abi_type: String,
    #[serde(rename = "abiValue")]
    pub abi_value: String,
}

impl Model507 {
    pub fn new(abi_type: String, abi_value: String) -> Model507 {
        Model507 {
            abi_type,
            abi_value,
        }
    }
}


