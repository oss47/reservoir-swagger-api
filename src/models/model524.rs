/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model524 {
    /// Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979`
    #[serde(rename = "tokens")]
    pub tokens: Vec<String>,
    /// API to update the spam status of a token
    #[serde(rename = "spam", skip_serializing_if = "Option::is_none")]
    pub spam: Option<bool>,
}

impl Model524 {
    pub fn new(tokens: Vec<String>) -> Model524 {
        Model524 {
            tokens,
            spam: None,
        }
    }
}


