# GetExecuteCancelV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**steps** | Option<[**Vec<models::Model165>**](Model165.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


