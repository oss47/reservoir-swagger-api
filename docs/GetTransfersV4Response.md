# GetTransfersV4Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transfers** | Option<[**Vec<models::Model108>**](Model108.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


