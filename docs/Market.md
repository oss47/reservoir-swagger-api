# Market

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**floor_ask** | Option<[**models::Model62**](Model62.md)> |  | [optional]
**top_bid** | Option<[**models::TopBid**](topBid.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


