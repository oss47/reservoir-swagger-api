# PaymentProcessorV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**use_off_chain_cancellation** | **bool** |  | 
**cosigner** | Option<**String**> |  | [optional]
**replace_order_id** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


