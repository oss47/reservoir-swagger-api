# Model145

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Collection id | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**banner** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**opensea_verification_status** | Option<**String**> |  | [optional]
**magiceden_verification_status** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**primary_contract** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**volume_percent_change** | Option<**f64**> |  | [optional]
**count_percent_change** | Option<**f64**> |  | [optional]
**creator** | Option<**String**> |  | [optional]
**on_sale_count** | Option<**i32**> |  | [optional]
**floor_ask** | Option<[**models::Model19**](Model19.md)> |  | [optional]
**token_count** | Option<**f64**> | Total tokens within the collection. | [optional]
**owner_count** | Option<**f64**> | Unique number of owners. | [optional]
**is_minting** | Option<**bool**> |  | [optional]
**created_at** | Option<[**String**](string.md)> |  | [optional]
**start_date** | Option<[**String**](string.md)> |  | [optional]
**end_date** | Option<[**String**](string.md)> |  | [optional]
**max_supply** | Option<**f64**> |  | [optional]
**mint_price** | Option<**String**> |  | [optional]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**mint_volume** | Option<**f64**> |  | [optional]
**mint_count** | Option<**f64**> |  | [optional]
**six_hour_count** | Option<**f64**> |  | [optional]
**one_hour_count** | Option<**f64**> |  | [optional]
**mint_type** | Option<**String**> |  | [optional]
**mint_standard** | Option<**String**> |  | [optional]
**mint_status** | Option<**String**> |  | [optional]
**mint_stages** | Option<[**Vec<models::Model143>**](Model143.md)> |  | [optional]
**collection_volume** | Option<[**models::Volume**](volume.md)> |  | [optional]
**volume_change** | Option<[**models::Model28**](Model28.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


