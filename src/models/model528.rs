/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model528 {
    /// List of items to buy.
    #[serde(rename = "items")]
    pub items: Vec<models::Model526>,
    /// Address of wallet filling (receiver of the NFT).
    #[serde(rename = "taker")]
    pub taker: String,
    /// Address of wallet relaying the fill transaction (paying for the NFT).
    #[serde(rename = "relayer", skip_serializing_if = "Option::is_none")]
    pub relayer: Option<String>,
    /// If true, only the path will be returned.
    #[serde(rename = "onlyPath", skip_serializing_if = "Option::is_none")]
    pub only_path: Option<bool>,
    /// If true, all fills will be executed through the router (where possible)
    #[serde(rename = "forceRouter", skip_serializing_if = "Option::is_none")]
    pub force_router: Option<bool>,
    /// If passed, all fills will be executed through the trusted trusted forwarder (where possible)
    #[serde(rename = "forwarderChannel", skip_serializing_if = "Option::is_none")]
    pub forwarder_channel: Option<String>,
    /// Currency to be used for purchases.
    #[serde(rename = "currency", skip_serializing_if = "Option::is_none")]
    pub currency: Option<String>,
    /// The chain id of the purchase currency
    #[serde(rename = "currencyChainId", skip_serializing_if = "Option::is_none")]
    pub currency_chain_id: Option<f64>,
    /// Charge any missing royalties.
    #[serde(rename = "normalizeRoyalties", skip_serializing_if = "Option::is_none")]
    pub normalize_royalties: Option<bool>,
    /// If true, inactive orders will not be skipped over (only relevant when filling via a specific order id).
    #[serde(
        rename = "allowInactiveOrderIds",
        skip_serializing_if = "Option::is_none"
    )]
    pub allow_inactive_order_ids: Option<bool>,
    /// Filling source used for attribution. Example: `reservoir.market`
    #[serde(rename = "source", skip_serializing_if = "Option::is_none")]
    pub source: Option<String>,
    /// List of fees (formatted as `feeRecipient:feeAmount`) to be taken when filling. Unless overridden via the `currency` param, the currency used for any fees on top matches the buy-in currency detected by the backend. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:1000000000000000`
    #[serde(rename = "feesOnTop", skip_serializing_if = "Option::is_none")]
    pub fees_on_top: Option<Vec<String>>,
    /// If true, any off-chain or on-chain errors will be skipped.
    #[serde(rename = "partial", skip_serializing_if = "Option::is_none")]
    pub partial: Option<bool>,
    /// If true, balance check will be skipped.
    #[serde(rename = "skipBalanceCheck", skip_serializing_if = "Option::is_none")]
    pub skip_balance_check: Option<bool>,
    /// Exclude orders that can only be filled by EOAs, to support filling with smart contracts. If marked `true`, blur will be excluded.
    #[serde(rename = "excludeEOA", skip_serializing_if = "Option::is_none")]
    pub exclude_eoa: Option<bool>,
    /// Optional custom gas settings. Includes base fee & priority fee in this limit.
    #[serde(rename = "maxFeePerGas", skip_serializing_if = "Option::is_none")]
    pub max_fee_per_gas: Option<String>,
    /// Optional custom gas settings.
    #[serde(
        rename = "maxPriorityFeePerGas",
        skip_serializing_if = "Option::is_none"
    )]
    pub max_priority_fee_per_gas: Option<String>,
    /// When true, will use permit to avoid approvals.
    #[serde(rename = "usePermit", skip_serializing_if = "Option::is_none")]
    pub use_permit: Option<bool>,
    /// Choose a specific swapping provider when buying in a different currency (defaults to `uniswap`)
    #[serde(rename = "swapProvider", skip_serializing_if = "Option::is_none")]
    pub swap_provider: Option<SwapProvider>,
    #[serde(rename = "executionMethod", skip_serializing_if = "Option::is_none")]
    pub execution_method: Option<ExecutionMethod>,
    /// Referrer address (where supported)
    #[serde(rename = "referrer", skip_serializing_if = "Option::is_none")]
    pub referrer: Option<String>,
    /// Mint comment (where suported)
    #[serde(rename = "comment", skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,
    /// Conduit key to use to fulfill the order
    #[serde(rename = "conduitKey", skip_serializing_if = "Option::is_none")]
    pub conduit_key: Option<String>,
    /// Optional X2Y2 API key used for filling.
    #[serde(rename = "x2y2ApiKey", skip_serializing_if = "Option::is_none")]
    pub x2y2_api_key: Option<String>,
    /// Optional OpenSea API key used for filling. You don't need to pass your own key, but if you don't, you are more likely to be rate-limited.
    #[serde(rename = "openseaApiKey", skip_serializing_if = "Option::is_none")]
    pub opensea_api_key: Option<String>,
    /// Advanced use case to pass personal blurAuthToken; the API will generate one if left empty.
    #[serde(rename = "blurAuth", skip_serializing_if = "Option::is_none")]
    pub blur_auth: Option<String>,
}

impl Model528 {
    pub fn new(items: Vec<models::Model526>, taker: String) -> Model528 {
        Model528 {
            items,
            taker,
            relayer: None,
            only_path: None,
            force_router: None,
            forwarder_channel: None,
            currency: None,
            currency_chain_id: None,
            normalize_royalties: None,
            allow_inactive_order_ids: None,
            source: None,
            fees_on_top: None,
            partial: None,
            skip_balance_check: None,
            exclude_eoa: None,
            max_fee_per_gas: None,
            max_priority_fee_per_gas: None,
            use_permit: None,
            swap_provider: None,
            execution_method: None,
            referrer: None,
            comment: None,
            conduit_key: None,
            x2y2_api_key: None,
            opensea_api_key: None,
            blur_auth: None,
        }
    }
}
/// Choose a specific swapping provider when buying in a different currency (defaults to `uniswap`)
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum SwapProvider {
    #[serde(rename = "uniswap")]
    Uniswap,
    #[serde(rename = "1inch")]
    Variant1inch,
}

impl Default for SwapProvider {
    fn default() -> SwapProvider {
        Self::Uniswap
    }
}
///
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum ExecutionMethod {
    #[serde(rename = "seaport-intent")]
    SeaportIntent,
    #[serde(rename = "intent")]
    Intent,
}

impl Default for ExecutionMethod {
    fn default() -> ExecutionMethod {
        Self::SeaportIntent
    }
}
