# Model192

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | Option<[**crate::models::Currency**](currency.md)> |  | [optional]
**amount** | Option<[**crate::models::Amount**](amount.md)> |  | [optional]
**net_amount** | Option<[**crate::models::NetAmount**](netAmount.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


