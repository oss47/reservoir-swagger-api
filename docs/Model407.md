# Model407

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **String** |  | 
**count** | Option<**f64**> |  | [optional]
**floor_ask_price** | Option<**f64**> | Returned only for attributes with less than 10k tokens | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


