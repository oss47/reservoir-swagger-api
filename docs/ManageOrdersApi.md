# \ManageOrdersApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_crosspostingorders_v1**](ManageOrdersApi.md#get_crosspostingorders_v1) | **GET** /cross-posting-orders/v1 | Check Cross Posting Status
[**get_transactions_txhash_synced_v1**](ManageOrdersApi.md#get_transactions_txhash_synced_v1) | **GET** /transactions/{txHash}/synced/v1 | Check Transaction Status
[**post_order_v4**](ManageOrdersApi.md#post_order_v4) | **POST** /order/v4 | Submit Signed Orders



## get_crosspostingorders_v1

> models::GetCrossPostingOrdersV1Response get_crosspostingorders_v1(ids, continuation, limit)
Check Cross Posting Status

This API can be used to check the status of cross posted listings and bids.   Input your `crossPostingOrderId` into the `ids` param and submit for the status.    The `crossPostingOrderId` is returned in the `execute/bids` and `execute/asks` response as well as the `onProgess` callback for the SDK.    Note: ReservoirKit does not return a `crossPostingOrderId`.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<[**Vec<f64>**](f64.md)> |  |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetCrossPostingOrdersV1Response**](getCrossPostingOrdersV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_transactions_txhash_synced_v1

> models::GetTransactionSyncedV1Response get_transactions_txhash_synced_v1(tx_hash)
Check Transaction Status

Get a boolean response on whether a particular transaction was synced or not.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**tx_hash** | **String** |  | [required] |

### Return type

[**models::GetTransactionSyncedV1Response**](getTransactionSyncedV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_order_v4

> models::PostOrderV4Response post_order_v4(signature, body)
Submit Signed Orders

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | Option<**String**> |  |  |
**body** | Option<[**Model477**](Model477.md)> |  |  |

### Return type

[**models::PostOrderV4Response**](postOrderV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

