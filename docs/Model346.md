# Model346

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_count** | Option<**String**> |  | [optional]
**on_sale_count** | Option<**String**> |  | [optional]
**floor_ask_price** | Option<**f64**> |  | [optional]
**acquired_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


