/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct PostExecuteStatusV1Response {
    #[serde(rename = "status")]
    pub status: Status,
    #[serde(rename = "details", skip_serializing_if = "Option::is_none")]
    pub details: Option<String>,
    #[serde(rename = "txHashes", skip_serializing_if = "Option::is_none")]
    pub tx_hashes: Option<Vec<String>>,
    #[serde(rename = "time", skip_serializing_if = "Option::is_none")]
    pub time: Option<f64>,
}

impl PostExecuteStatusV1Response {
    pub fn new(status: Status) -> PostExecuteStatusV1Response {
        PostExecuteStatusV1Response {
            status,
            details: None,
            tx_hashes: None,
            time: None,
        }
    }
}
///
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum Status {
    #[serde(rename = "unknown")]
    Unknown,
    #[serde(rename = "pending")]
    Pending,
    #[serde(rename = "received")]
    Received,
    #[serde(rename = "success")]
    Success,
    #[serde(rename = "failure")]
    Failure,
}

impl Default for Status {
    fn default() -> Status {
        Self::Unknown
    }
}
