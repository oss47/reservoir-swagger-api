# Model270

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **f64** |  | 
**timestamp** | **f64** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


