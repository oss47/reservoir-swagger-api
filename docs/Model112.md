# Model112

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**param_1day** | Option<[**models::Model1day**](1day.md)> |  | [optional]
**param_7day** | Option<[**models::Model1day**](1day.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


