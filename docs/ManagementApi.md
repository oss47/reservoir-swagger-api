# \ManagementApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_apikeys_key_ratelimits**](ManagementApi.md#get_apikeys_key_ratelimits) | **GET** /api-keys/{key}/rate-limits | Get rate limits for the given API key
[**post_collections_collection_override_v1**](ManagementApi.md#post_collections_collection_override_v1) | **POST** /collections/{collection}/override/v1 | Override collections
[**post_collections_disablemetadata_v1**](ManagementApi.md#post_collections_disablemetadata_v1) | **POST** /collections/disable-metadata/v1 | Disable or reenable metadata for a collection
[**post_collections_nsfwstatus_v1**](ManagementApi.md#post_collections_nsfwstatus_v1) | **POST** /collections/nsfw-status/v1 | Update collections nsfw status
[**post_collections_spamstatus_v1**](ManagementApi.md#post_collections_spamstatus_v1) | **POST** /collections/spam-status/v1 | Update collections spam status
[**post_management_cosigners_v1**](ManagementApi.md#post_management_cosigners_v1) | **POST** /management/cosigners/v1 | Create or update an external cosigner
[**post_management_mints_simulate_v1**](ManagementApi.md#post_management_mints_simulate_v1) | **POST** /management/mints/simulate/v1 | Simulate any given mint
[**post_management_orders_simulate_v1**](ManagementApi.md#post_management_orders_simulate_v1) | **POST** /management/orders/simulate/v1 | Simulate any given order
[**post_orders_invalidate_v1**](ManagementApi.md#post_orders_invalidate_v1) | **POST** /orders/invalidate/v1 | Invalidate stale orders
[**post_tokens_disablemetadata_v1**](ManagementApi.md#post_tokens_disablemetadata_v1) | **POST** /tokens/disable-metadata/v1 | Disable or reenable metadata for a token
[**post_tokens_flag_v1**](ManagementApi.md#post_tokens_flag_v1) | **POST** /tokens/flag/v1 | Update token flag status
[**post_tokens_nsfwstatus_v1**](ManagementApi.md#post_tokens_nsfwstatus_v1) | **POST** /tokens/nsfw-status/v1 | Update the tokens nsfw status
[**post_tokens_spamstatus_v1**](ManagementApi.md#post_tokens_spamstatus_v1) | **POST** /tokens/spam-status/v1 | Update the tokens spam status



## get_apikeys_key_ratelimits

> models::Model111 get_apikeys_key_ratelimits(key)
Get rate limits for the given API key

Get the rate limits for the given API key. Note: API keys are not universal across all available chains; please make a different key for every chain.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**key** | **String** | The API key | [required] |

### Return type

[**models::Model111**](Model111.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_collections_collection_override_v1

> models::PutSetCollectionCommunityV1Response post_collections_collection_override_v1(x_api_key, collection, body)
Override collections

Override collections metadata and royalties

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**collection** | **String** | The collection id to update. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**body** | Option<[**Model622**](Model622.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_collections_disablemetadata_v1

> models::PutSetCollectionCommunityV1Response post_collections_disablemetadata_v1(x_api_key, body)
Disable or reenable metadata for a collection

This API requires an allowed API key for execution. Please contact technical support with more questions.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**body** | Option<[**Model489**](Model489.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_collections_nsfwstatus_v1

> models::PutSetCollectionCommunityV1Response post_collections_nsfwstatus_v1(x_api_key, body)
Update collections nsfw status

This API can be used by allowed API keys to update the nsfw status of a collection.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**body** | Option<[**Model491**](Model491.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_collections_spamstatus_v1

> models::PutSetCollectionCommunityV1Response post_collections_spamstatus_v1(x_api_key, body)
Update collections spam status

This API can be used by allowed API keys to update the spam status of a collection.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**body** | Option<[**Model494**](Model494.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_management_cosigners_v1

> models::PutSetCollectionCommunityV1Response post_management_cosigners_v1(body)
Create or update an external cosigner

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model604**](Model604.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_management_mints_simulate_v1

> models::PutSetCollectionCommunityV1Response post_management_mints_simulate_v1(body)
Simulate any given mint

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model624**](Model624.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_management_orders_simulate_v1

> models::PutSetCollectionCommunityV1Response post_management_orders_simulate_v1(body)
Simulate any given order

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model625**](Model625.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_orders_invalidate_v1

> String post_orders_invalidate_v1(x_api_key, body)
Invalidate stale orders

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**body** | Option<[**Model606**](Model606.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokens_disablemetadata_v1

> models::PutSetCollectionCommunityV1Response post_tokens_disablemetadata_v1(x_api_key, body)
Disable or reenable metadata for a token

This API requires an allowed API key for execution. Please contact technical support with more questions.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**body** | Option<[**Model608**](Model608.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokens_flag_v1

> models::PutSetCollectionCommunityV1Response post_tokens_flag_v1(body)
Update token flag status

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model609**](Model609.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokens_nsfwstatus_v1

> models::PutSetCollectionCommunityV1Response post_tokens_nsfwstatus_v1(x_api_key, body)
Update the tokens nsfw status

This API can be used by allowed API keys to update the nsfw status of a token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**body** | Option<[**Model611**](Model611.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokens_spamstatus_v1

> models::PutSetCollectionCommunityV1Response post_tokens_spamstatus_v1(x_api_key, body)
Update the tokens spam status

This API can be used by allowed API keys to update the spam status of a token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**body** | Option<[**Model619**](Model619.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

