# Model585

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | Token to sell. | 
**quantity** | Option<**i32**> | Quantity of tokens to sell. | [optional]
**order_id** | Option<**String**> | Optional order id to sell into. | [optional]
**raw_order** | Option<[**models::Model584**](Model584.md)> |  | [optional]
**exact_order_source** | Option<**String**> | Only consider orders from this source. | [optional]
**exclusions** | Option<[**Vec<models::Model525>**](Model525.md)> | Items to exclude | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


