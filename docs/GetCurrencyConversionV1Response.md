# GetCurrencyConversionV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conversion** | Option<**String**> |  | [optional]
**usd** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


