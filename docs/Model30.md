# Model30

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | [**models::Price**](price.md) |  | 
**quantity** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


