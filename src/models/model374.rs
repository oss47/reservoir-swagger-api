/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model374 {
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "value", skip_serializing_if = "Option::is_none")]
    pub value: Option<f64>,
}

impl Model374 {
    pub fn new() -> Model374 {
        Model374 {
            id: None,
            value: None,
        }
    }
}
