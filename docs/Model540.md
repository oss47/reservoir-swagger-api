# Model540

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gas** | Option<[**models::Price**](price.md)> |  | [optional]
**relayer** | Option<[**models::Price**](price.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


