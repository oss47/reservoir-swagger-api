# Model102

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain_id** | **f64** |  | 
**contract** | **String** |  | 
**token_id** | **String** |  | 
**name** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**image_small** | Option<**String**> |  | [optional]
**image_large** | Option<**String**> |  | [optional]
**metadata** | Option<[**serde_json::Value**](.md)> |  | [optional]
**media** | Option<**String**> |  | [optional]
**kind** | Option<**String**> | Can be erc721, erc115, etc. | [optional]
**is_flagged** | Option<**bool**> |  | [optional][default to false]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**is_nsfw** | Option<**bool**> |  | [optional][default to false]
**metadata_disabled** | Option<**bool**> |  | [optional][default to false]
**last_flag_update** | Option<**String**> |  | [optional]
**last_flag_change** | Option<**String**> |  | [optional]
**supply** | Option<**f64**> | Can be higher than 1 if erc1155 | [optional]
**remaining_supply** | Option<**f64**> |  | [optional]
**rarity** | Option<**f64**> | No rarity for collections over 100k | [optional]
**rarity_rank** | Option<**f64**> | No rarity rank for collections over 100k | [optional]
**collection** | Option<[**models::Model83**](Model83.md)> |  | [optional]
**last_sale** | Option<[**models::Model58**](Model58.md)> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**attributes** | Option<[**Vec<models::Model95>**](Model95.md)> |  | [optional]
**decimals** | Option<**f64**> | Can be set for ERC1155 tokens according to the standard | [optional]
**mint_stages** | Option<[**Vec<models::Model100>**](Model100.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


