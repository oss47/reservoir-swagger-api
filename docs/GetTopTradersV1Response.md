# GetTopTradersV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**top_traders** | Option<[**Vec<models::Model278>**](Model278.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


