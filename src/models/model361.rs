/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model361 {
    #[serde(rename = "imageUrl", skip_serializing_if = "Option::is_none")]
    pub image_url: Option<String>,
    #[serde(rename = "discordUrl", skip_serializing_if = "Option::is_none")]
    pub discord_url: Option<String>,
    #[serde(rename = "description", skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(rename = "externalUrl", skip_serializing_if = "Option::is_none")]
    pub external_url: Option<String>,
    #[serde(rename = "bannerImageUrl", skip_serializing_if = "Option::is_none")]
    pub banner_image_url: Option<String>,
    #[serde(rename = "twitterUsername", skip_serializing_if = "Option::is_none")]
    pub twitter_username: Option<String>,
}

impl Model361 {
    pub fn new() -> Model361 {
        Model361 {
            image_url: None,
            discord_url: None,
            description: None,
            external_url: None,
            banner_image_url: None,
            twitter_username: None,
        }
    }
}
