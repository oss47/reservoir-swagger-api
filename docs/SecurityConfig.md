# SecurityConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operator_whitelist** | Option<**Vec<String>**> |  | [optional]
**operator_blacklist** | Option<**Vec<String>**> |  | [optional]
**receiver_allow_list** | Option<**Vec<String>**> |  | [optional]
**transfer_security_level** | Option<**f64**> |  | [optional]
**transfer_validator** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


