# Model399

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **String** | Case sensitive | 
**count** | Option<**f64**> |  | [optional]
**floor_ask_price** | Option<[**models::Model398**](Model398.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


