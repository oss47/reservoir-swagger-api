# Model267

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplaces** | Option<[**Vec<models::Model265>**](Model265.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


