# Model108

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<[**models::Model50**](Model50.md)> |  | [optional]
**from** | Option<**String**> |  | [optional]
**to** | Option<**String**> |  | [optional]
**amount** | Option<**String**> | Can be higher than 1 if erc1155. | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**block** | Option<**f64**> |  | [optional]
**log_index** | Option<**f64**> |  | [optional]
**batch_index** | Option<**f64**> |  | [optional]
**timestamp** | Option<**f64**> |  | [optional]
**is_deleted** | Option<**bool**> |  | [optional]
**is_airdrop** | Option<**bool**> |  | [optional]
**updated_at** | Option<**String**> | Time when last updated in indexer | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


