# Model591

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Returns `auth` or `nft-approval` | 
**action** | **String** |  | 
**description** | **String** |  | 
**kind** | **String** | Returns `signature` or `transaction`. | 
**items** | [**Vec<models::Model589>**](Model589.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


