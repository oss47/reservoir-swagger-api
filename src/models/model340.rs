/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model340 {
    #[serde(rename = "tokenCount", skip_serializing_if = "Option::is_none")]
    pub token_count: Option<String>,
    #[serde(rename = "onSaleCount", skip_serializing_if = "Option::is_none")]
    pub on_sale_count: Option<String>,
    #[serde(rename = "floorAsk", skip_serializing_if = "Option::is_none")]
    pub floor_ask: Option<Box<crate::models::Model339>>,
    #[serde(rename = "acquiredAt", skip_serializing_if = "Option::is_none")]
    pub acquired_at: Option<String>,
}

impl Model340 {
    pub fn new() -> Model340 {
        Model340 {
            token_count: None,
            on_sale_count: None,
            floor_ask: None,
            acquired_at: None,
        }
    }
}


