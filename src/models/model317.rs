/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model317 {
    #[serde(rename = "set", skip_serializing_if = "Option::is_none")]
    pub set: Option<Box<crate::models::Set>>,
    #[serde(rename = "primaryOrder", skip_serializing_if = "Option::is_none")]
    pub primary_order: Option<Box<crate::models::PrimaryOrder>>,
    #[serde(rename = "totalValid", skip_serializing_if = "Option::is_none")]
    pub total_valid: Option<f32>,
}

impl Model317 {
    pub fn new() -> Model317 {
        Model317 {
            set: None,
            primary_order: None,
            total_valid: None,
        }
    }
}


