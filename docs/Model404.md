# Model404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**routers** | [**Vec<crate::models::Model403>**](Model403.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


