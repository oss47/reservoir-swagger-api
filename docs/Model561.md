# Model561

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seaport_v1_period_4** | Option<[**models::SeaportV1Period4**](seaport-v1.4.md)> |  | [optional]
**seaport_v1_period_5** | Option<[**models::SeaportV1Period4**](seaport-v1.4.md)> |  | [optional]
**seaport_v1_period_6** | Option<[**models::SeaportV1Period4**](seaport-v1.4.md)> |  | [optional]
**payment_processor_v2** | Option<[**models::PaymentProcessorV2**](payment-processor-v2.md)> |  | [optional]
**alienswap** | Option<[**models::Alienswap**](alienswap.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


