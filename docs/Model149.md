# Model149

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Order Id | [optional]
**status** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**quantity_remaining** | Option<**f64**> | With ERC1155s, quantity can be higher than 1 | [optional]
**nonce** | Option<**String**> |  | [optional]
**valid_from** | Option<**f64**> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**source** | Option<**String**> |  | [optional]
**is_dynamic** | Option<**bool**> |  | [optional]
**criteria** | Option<[**models::Model120**](Model120.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


