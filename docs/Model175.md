# Model175

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection_name** | Option<**String**> |  | [optional]
**token_name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


