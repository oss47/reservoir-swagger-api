# Model572

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | Option<**String**> | Collection to mint. | [optional]
**token** | Option<**String**> | Token to mint. | [optional]
**custom** | Option<[**models::Custom**](custom.md)> |  | [optional]
**quantity** | Option<**i32**> | Quantity of tokens to mint. | [optional]
**preferred_mint_stage** | Option<**String**> | Optionally specify a stage to mint | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


