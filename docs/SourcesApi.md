# \SourcesApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_sources_v1**](SourcesApi.md#get_sources_v1) | **GET** /sources/v1 | Sources List



## get_sources_v1

> models::GetSourcesV1Response get_sources_v1(sort_by, sort_direction, domain, limit, continuation)
Sources List

This API returns a list of sources

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**sort_by** | Option<**String**> | Order of the items are returned in the response. |  |[default to createdAt]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**domain** | Option<**String**> | Filter to a particular domain. Example: `x2y2.io` |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**continuation** | Option<**String**> |  |  |

### Return type

[**models::GetSourcesV1Response**](getSourcesV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

