# Model432

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seaport_offers** | Option<[**Vec<crate::models::Model431>**](Model431.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


