# GetCollectionFloorAskOracleV4Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | **f64** |  | 
**message** | Option<[**models::Model309**](Model309.md)> |  | [optional]
**data** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


