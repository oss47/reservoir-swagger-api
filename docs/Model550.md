# Model550

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bps** | Option<**f32**> |  | [optional]
**recipient** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


