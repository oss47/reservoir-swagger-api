# Model259

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**min_bps** | Option<**f64**> |  | [optional]
**max_bps** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


