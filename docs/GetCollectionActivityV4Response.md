# GetCollectionActivityV4Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**continuation** | Option<**String**> |  | [optional]
**activities** | Option<[**Vec<models::Model116>**](Model116.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


