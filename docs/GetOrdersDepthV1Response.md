# GetOrdersDepthV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depth** | Option<[**Vec<models::Model188>**](Model188.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


