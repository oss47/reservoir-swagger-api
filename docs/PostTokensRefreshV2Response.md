# PostTokensRefreshV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | Option<[**Vec<models::Model615>**](Model615.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


