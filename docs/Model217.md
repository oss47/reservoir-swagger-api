# Model217

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**is_nsfw** | Option<**bool**> |  | [optional][default to false]
**slug** | Option<**String**> |  | [optional]
**rank** | Option<[**models::Model21**](Model21.md)> |  | [optional]
**volume** | Option<[**models::Model216**](Model216.md)> |  | [optional]
**floor_ask_price** | Option<[**models::Model213**](Model213.md)> |  | [optional]
**opensea_verification_status** | Option<**String**> |  | [optional]
**magiceden_verification_status** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


