# Model296

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<[**crate::models::Model294**](Model294.md)> |  | [optional]
**ownership** | Option<[**crate::models::Model295**](Model295.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


