# SeaportV1Period4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conduit_key** | Option<**String**> |  | [optional]
**use_off_chain_cancellation** | **bool** |  | 
**replace_order_id** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


