# GetCollectionsV4Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**continuation** | Option<**String**> |  | [optional]
**collections** | Option<[**Vec<models::Model14>**](Model14.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


