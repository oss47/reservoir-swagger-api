# \XDeprecatedApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_attributes_v1**](XDeprecatedApi.md#get_attributes_v1) | **GET** /attributes/v1 | List of attributes
[**get_collection_v1**](XDeprecatedApi.md#get_collection_v1) | **GET** /collection/v1 | Get detailed information about a single collection
[**get_collection_v2**](XDeprecatedApi.md#get_collection_v2) | **GET** /collection/v2 | Single Collection
[**get_collection_v3**](XDeprecatedApi.md#get_collection_v3) | **GET** /collection/v3 | Single Collection
[**get_collections_activity_v4**](XDeprecatedApi.md#get_collections_activity_v4) | **GET** /collections/activity/v4 | Collection activity
[**get_collections_activity_v5**](XDeprecatedApi.md#get_collections_activity_v5) | **GET** /collections/activity/v5 | Collection activity
[**get_collections_activity_v7**](XDeprecatedApi.md#get_collections_activity_v7) | **GET** /collections/activity/v7 | Collection activity
[**get_collections_collection_activity_v1**](XDeprecatedApi.md#get_collections_collection_activity_v1) | **GET** /collections/{collection}/activity/v1 | Collection activity
[**get_collections_collection_activity_v2**](XDeprecatedApi.md#get_collections_collection_activity_v2) | **GET** /collections/{collection}/activity/v2 | Collection activity
[**get_collections_collection_activity_v3**](XDeprecatedApi.md#get_collections_collection_activity_v3) | **GET** /collections/{collection}/activity/v3 | Collection activity
[**get_collections_collection_attributes_all_v1**](XDeprecatedApi.md#get_collections_collection_attributes_all_v1) | **GET** /collections/{collection}/attributes/all/v1 | Get all attributes in a collection
[**get_collections_collection_attributes_all_v2**](XDeprecatedApi.md#get_collections_collection_attributes_all_v2) | **GET** /collections/{collection}/attributes/all/v2 | All attributes
[**get_collections_collection_attributes_all_v3**](XDeprecatedApi.md#get_collections_collection_attributes_all_v3) | **GET** /collections/{collection}/attributes/all/v3 | All attributes
[**get_collections_collection_attributes_explore_v1**](XDeprecatedApi.md#get_collections_collection_attributes_explore_v1) | **GET** /collections/{collection}/attributes/explore/v1 | Get detailed aggregate about attributes in a collection, e.g. trait floors
[**get_collections_collection_attributes_explore_v2**](XDeprecatedApi.md#get_collections_collection_attributes_explore_v2) | **GET** /collections/{collection}/attributes/explore/v2 | Explore attributes
[**get_collections_collection_attributes_explore_v3**](XDeprecatedApi.md#get_collections_collection_attributes_explore_v3) | **GET** /collections/{collection}/attributes/explore/v3 | Explore attributes
[**get_collections_collection_attributes_explore_v4**](XDeprecatedApi.md#get_collections_collection_attributes_explore_v4) | **GET** /collections/{collection}/attributes/explore/v4 | Explore attributes
[**get_collections_collection_attributes_static_v1**](XDeprecatedApi.md#get_collections_collection_attributes_static_v1) | **GET** /collections/{collection}/attributes/static/v1 | All attributes + token ids
[**get_collections_collection_attributes_v1**](XDeprecatedApi.md#get_collections_collection_attributes_v1) | **GET** /collections/{collection}/attributes/v1 | Get detailed aggregate about attributes in a collection, e.g. trait floors
[**get_collections_collection_marketplaceconfigurations_v1**](XDeprecatedApi.md#get_collections_collection_marketplaceconfigurations_v1) | **GET** /collections/{collection}/marketplace-configurations/v1 | Marketplace configurations by collection
[**get_collections_collection_supportedmarketplaces_v1**](XDeprecatedApi.md#get_collections_collection_supportedmarketplaces_v1) | **GET** /collections/{collection}/supported-marketplaces/v1 | Supported marketplaces by collection
[**get_collections_collection_topbids_v1**](XDeprecatedApi.md#get_collections_collection_topbids_v1) | **GET** /collections/{collection}/top-bids/v1 | Bid distribution
[**get_collections_collectionorslug_v1**](XDeprecatedApi.md#get_collections_collectionorslug_v1) | **GET** /collections/{collectionOrSlug}/v1 | Single collection
[**get_collections_topselling_v1**](XDeprecatedApi.md#get_collections_topselling_v1) | **GET** /collections/top-selling/v1 | Top Selling Collections
[**get_collections_topselling_v2**](XDeprecatedApi.md#get_collections_topselling_v2) | **GET** /collections/top-selling/v2 | Top selling collections
[**get_collections_v1**](XDeprecatedApi.md#get_collections_v1) | **GET** /collections/v1 | List of collections
[**get_collections_v2**](XDeprecatedApi.md#get_collections_v2) | **GET** /collections/v2 | Get a filtered list of collections
[**get_collections_v3**](XDeprecatedApi.md#get_collections_v3) | **GET** /collections/v3 | Get a filtered list of collections
[**get_collections_v4**](XDeprecatedApi.md#get_collections_v4) | **GET** /collections/v4 | Collections
[**get_collections_v5**](XDeprecatedApi.md#get_collections_v5) | **GET** /collections/v5 | Collections
[**get_collections_v6**](XDeprecatedApi.md#get_collections_v6) | **GET** /collections/v6 | Collections
[**get_currencies_conversion_v1**](XDeprecatedApi.md#get_currencies_conversion_v1) | **GET** /currencies/conversion/v1 | Currency Conversions
[**get_events_asks_v2**](XDeprecatedApi.md#get_events_asks_v2) | **GET** /events/asks/v2 | Asks status changes
[**get_events_bids_v1**](XDeprecatedApi.md#get_events_bids_v1) | **GET** /events/bids/v1 | Bid status changes
[**get_events_bids_v2**](XDeprecatedApi.md#get_events_bids_v2) | **GET** /events/bids/v2 | Bid status changes
[**get_events_collections_floorask_v1**](XDeprecatedApi.md#get_events_collections_floorask_v1) | **GET** /events/collections/floor-ask/v1 | Collection floor changes
[**get_events_collections_topbid_v1**](XDeprecatedApi.md#get_events_collections_topbid_v1) | **GET** /events/collections/top-bid/v1 | Collection top bid changes
[**get_events_orders_v1**](XDeprecatedApi.md#get_events_orders_v1) | **GET** /events/orders/v1 | Order status changes
[**get_events_tokens_floorask_v2**](XDeprecatedApi.md#get_events_tokens_floorask_v2) | **GET** /events/tokens/floor-ask/v2 | Token price changes
[**get_events_tokens_floorask_v3**](XDeprecatedApi.md#get_events_tokens_floorask_v3) | **GET** /events/tokens/floor-ask/v3 | Token price changes
[**get_execute_cancel_v2**](XDeprecatedApi.md#get_execute_cancel_v2) | **GET** /execute/cancel/v2 | Cancel order
[**get_liquidity_users_v1**](XDeprecatedApi.md#get_liquidity_users_v1) | **GET** /liquidity/users/v1 | User bid liquidity rankings
[**get_liquidity_users_v2**](XDeprecatedApi.md#get_liquidity_users_v2) | **GET** /liquidity/users/v2 | User bid liquidity rankings
[**get_oracle_collections_floorask_v4**](XDeprecatedApi.md#get_oracle_collections_floorask_v4) | **GET** /oracle/collections/floor-ask/v4 | Collection floor
[**get_oracle_collections_floorask_v5**](XDeprecatedApi.md#get_oracle_collections_floorask_v5) | **GET** /oracle/collections/floor-ask/v5 | Collection floor
[**get_oracle_collections_topbid_v2**](XDeprecatedApi.md#get_oracle_collections_topbid_v2) | **GET** /oracle/collections/top-bid/v2 | Collection top bid oracle
[**get_oracle_tokens_status_v2**](XDeprecatedApi.md#get_oracle_tokens_status_v2) | **GET** /oracle/tokens/status/v2 | Token status oracle
[**get_orders_all_v1**](XDeprecatedApi.md#get_orders_all_v1) | **GET** /orders/all/v1 | Bulk historical orders
[**get_orders_all_v2**](XDeprecatedApi.md#get_orders_all_v2) | **GET** /orders/all/v2 | Bulk historical orders
[**get_orders_asks_v1**](XDeprecatedApi.md#get_orders_asks_v1) | **GET** /orders/asks/v1 | Get a list of asks (listings), filtered by token, collection or maker
[**get_orders_asks_v2**](XDeprecatedApi.md#get_orders_asks_v2) | **GET** /orders/asks/v2 | Asks (listings)
[**get_orders_asks_v3**](XDeprecatedApi.md#get_orders_asks_v3) | **GET** /orders/asks/v3 | Asks (listings)
[**get_orders_asks_v4**](XDeprecatedApi.md#get_orders_asks_v4) | **GET** /orders/asks/v4 | Asks (listings)
[**get_orders_bids_v1**](XDeprecatedApi.md#get_orders_bids_v1) | **GET** /orders/bids/v1 | Get a list of bids (offers), filtered by token, collection or maker
[**get_orders_bids_v2**](XDeprecatedApi.md#get_orders_bids_v2) | **GET** /orders/bids/v2 | Bids (offers)
[**get_orders_bids_v3**](XDeprecatedApi.md#get_orders_bids_v3) | **GET** /orders/bids/v3 | Bids (offers)
[**get_orders_bids_v4**](XDeprecatedApi.md#get_orders_bids_v4) | **GET** /orders/bids/v4 | Bids (offers)
[**get_orders_bids_v5**](XDeprecatedApi.md#get_orders_bids_v5) | **GET** /orders/bids/v5 | Bids (offers)
[**get_orders_executed_v1**](XDeprecatedApi.md#get_orders_executed_v1) | **GET** /orders/executed/v1 | Order status
[**get_orders_users_user_topbids_v1**](XDeprecatedApi.md#get_orders_users_user_topbids_v1) | **GET** /orders/users/{user}/top-bids/v1 | User Top Bids
[**get_orders_users_user_topbids_v2**](XDeprecatedApi.md#get_orders_users_user_topbids_v2) | **GET** /orders/users/{user}/top-bids/v2 | User Top Bids
[**get_orders_users_user_topbids_v3**](XDeprecatedApi.md#get_orders_users_user_topbids_v3) | **GET** /orders/users/{user}/top-bids/v3 | User Top Bids
[**get_orders_v1**](XDeprecatedApi.md#get_orders_v1) | **GET** /orders/v1 | List of valid orders.
[**get_orders_v2**](XDeprecatedApi.md#get_orders_v2) | **GET** /orders/v2 | Submit order batch
[**get_owners_v1**](XDeprecatedApi.md#get_owners_v1) | **GET** /owners/v1 | Owners
[**get_pendingtxs_tokens_v1**](XDeprecatedApi.md#get_pendingtxs_tokens_v1) | **GET** /pending-txs/tokens/v1 | Pending tokens
[**get_redirect_logo_v1**](XDeprecatedApi.md#get_redirect_logo_v1) | **GET** /redirect/logo/v1 | Redirect response to the given source logo
[**get_redirect_token_v1**](XDeprecatedApi.md#get_redirect_token_v1) | **GET** /redirect/token/v1 | Redirect response to the given source token page
[**get_sales_bulk_v1**](XDeprecatedApi.md#get_sales_bulk_v1) | **GET** /sales/bulk/v1 | Bulk historical sales
[**get_sales_v1**](XDeprecatedApi.md#get_sales_v1) | **GET** /sales/v1 | Historical sales
[**get_sales_v2**](XDeprecatedApi.md#get_sales_v2) | **GET** /sales/v2 | Historical sales
[**get_sales_v3**](XDeprecatedApi.md#get_sales_v3) | **GET** /sales/v3 | Historical sales
[**get_sales_v4**](XDeprecatedApi.md#get_sales_v4) | **GET** /sales/v4 | Sales
[**get_sales_v5**](XDeprecatedApi.md#get_sales_v5) | **GET** /sales/v5 | Sales
[**get_search_collections_v1**](XDeprecatedApi.md#get_search_collections_v1) | **GET** /search/collections/v1 | Search collections
[**get_search_collections_v3**](XDeprecatedApi.md#get_search_collections_v3) | **GET** /search/collections/v3 | Search Collections
[**get_stats_v1**](XDeprecatedApi.md#get_stats_v1) | **GET** /stats/v1 | Stats
[**get_sync_asks_v1**](XDeprecatedApi.md#get_sync_asks_v1) | **GET** /sync/asks/v1 | Sync Asks (listings)
[**get_tokens_details_v2**](XDeprecatedApi.md#get_tokens_details_v2) | **GET** /tokens/details/v2 | Get one or more tokens with full details
[**get_tokens_details_v3**](XDeprecatedApi.md#get_tokens_details_v3) | **GET** /tokens/details/v3 | Get one or more tokens with full details
[**get_tokens_details_v4**](XDeprecatedApi.md#get_tokens_details_v4) | **GET** /tokens/details/v4 | Tokens (detailed response)
[**get_tokens_token_activity_v1**](XDeprecatedApi.md#get_tokens_token_activity_v1) | **GET** /tokens/{token}/activity/v1 | Token activity
[**get_tokens_token_activity_v2**](XDeprecatedApi.md#get_tokens_token_activity_v2) | **GET** /tokens/{token}/activity/v2 | Token activity
[**get_tokens_token_activity_v3**](XDeprecatedApi.md#get_tokens_token_activity_v3) | **GET** /tokens/{token}/activity/v3 | Token activity
[**get_tokens_token_activity_v4**](XDeprecatedApi.md#get_tokens_token_activity_v4) | **GET** /tokens/{token}/activity/v4 | Token activity
[**get_tokens_v1**](XDeprecatedApi.md#get_tokens_v1) | **GET** /tokens/v1 | List of tokens
[**get_tokens_v2**](XDeprecatedApi.md#get_tokens_v2) | **GET** /tokens/v2 | List of tokens, with basic details, optimized for speed
[**get_tokens_v3**](XDeprecatedApi.md#get_tokens_v3) | **GET** /tokens/v3 | List of tokens, with basic details, optimized for speed
[**get_tokens_v4**](XDeprecatedApi.md#get_tokens_v4) | **GET** /tokens/v4 | Tokens
[**get_tokens_v5**](XDeprecatedApi.md#get_tokens_v5) | **GET** /tokens/v5 | Tokens
[**get_tokens_v6**](XDeprecatedApi.md#get_tokens_v6) | **GET** /tokens/v6 | Tokens
[**get_tokens_v8**](XDeprecatedApi.md#get_tokens_v8) | **GET** /tokens/v8 | Tokens
[**get_transfers_bulk_v1**](XDeprecatedApi.md#get_transfers_bulk_v1) | **GET** /transfers/bulk/v1 | Bulk historical transfers
[**get_transfers_v2**](XDeprecatedApi.md#get_transfers_v2) | **GET** /transfers/v2 | Historical token transfers
[**get_transfers_v3**](XDeprecatedApi.md#get_transfers_v3) | **GET** /transfers/v3 | Historical token transfers
[**get_users_activity_v2**](XDeprecatedApi.md#get_users_activity_v2) | **GET** /users/activity/v2 | Users activity
[**get_users_activity_v3**](XDeprecatedApi.md#get_users_activity_v3) | **GET** /users/activity/v3 | Users activity
[**get_users_activity_v4**](XDeprecatedApi.md#get_users_activity_v4) | **GET** /users/activity/v4 | Users activity
[**get_users_activity_v5**](XDeprecatedApi.md#get_users_activity_v5) | **GET** /users/activity/v5 | Users activity
[**get_users_user_activity_v1**](XDeprecatedApi.md#get_users_user_activity_v1) | **GET** /users/{user}/activity/v1 | User activity
[**get_users_user_collections_v1**](XDeprecatedApi.md#get_users_user_collections_v1) | **GET** /users/{user}/collections/v1 | Get aggregate stats for a user, grouped by collection
[**get_users_user_collections_v2**](XDeprecatedApi.md#get_users_user_collections_v2) | **GET** /users/{user}/collections/v2 | User collections
[**get_users_user_collections_v3**](XDeprecatedApi.md#get_users_user_collections_v3) | **GET** /users/{user}/collections/v3 | User collections
[**get_users_user_positions_v1**](XDeprecatedApi.md#get_users_user_positions_v1) | **GET** /users/{user}/positions/v1 | Get a summary of a users bids and asks
[**get_users_user_tokens_v1**](XDeprecatedApi.md#get_users_user_tokens_v1) | **GET** /users/{user}/tokens/v1 | User tokens
[**get_users_user_tokens_v2**](XDeprecatedApi.md#get_users_user_tokens_v2) | **GET** /users/{user}/tokens/v2 | User tokens
[**get_users_user_tokens_v3**](XDeprecatedApi.md#get_users_user_tokens_v3) | **GET** /users/{user}/tokens/v3 | User Tokens
[**get_users_user_tokens_v4**](XDeprecatedApi.md#get_users_user_tokens_v4) | **GET** /users/{user}/tokens/v4 | User Tokens
[**get_users_user_tokens_v5**](XDeprecatedApi.md#get_users_user_tokens_v5) | **GET** /users/{user}/tokens/v5 | User Tokens
[**get_users_user_tokens_v6**](XDeprecatedApi.md#get_users_user_tokens_v6) | **GET** /users/{user}/tokens/v6 | User Tokens
[**get_users_user_tokens_v7**](XDeprecatedApi.md#get_users_user_tokens_v7) | **GET** /users/{user}/tokens/v7 | User Tokens
[**get_users_user_tokens_v8**](XDeprecatedApi.md#get_users_user_tokens_v8) | **GET** /users/{user}/tokens/v8 | User Tokens
[**get_users_user_tokens_v9**](XDeprecatedApi.md#get_users_user_tokens_v9) | **GET** /users/{user}/tokens/v9 | User Tokens
[**post_collections_refresh_v1**](XDeprecatedApi.md#post_collections_refresh_v1) | **POST** /collections/refresh/v1 | Refresh Collection
[**post_execute_bid_v4**](XDeprecatedApi.md#post_execute_bid_v4) | **POST** /execute/bid/v4 | Create bid (offer)
[**post_execute_buy_v5**](XDeprecatedApi.md#post_execute_buy_v5) | **POST** /execute/buy/v5 | Buy tokens
[**post_execute_buy_v6**](XDeprecatedApi.md#post_execute_buy_v6) | **POST** /execute/buy/v6 | Buy tokens
[**post_execute_list_v4**](XDeprecatedApi.md#post_execute_list_v4) | **POST** /execute/list/v4 | Create ask (listing)
[**post_execute_sell_v6**](XDeprecatedApi.md#post_execute_sell_v6) | **POST** /execute/sell/v6 | Sell tokens (accept bids)
[**post_order_v2**](XDeprecatedApi.md#post_order_v2) | **POST** /order/v2 | Submit single order
[**post_order_v3**](XDeprecatedApi.md#post_order_v3) | **POST** /order/v3 | Submit signed order
[**post_orders_v1**](XDeprecatedApi.md#post_orders_v1) | **POST** /orders/v1 | Submit order batch
[**post_seaport_offers**](XDeprecatedApi.md#post_seaport_offers) | **POST** /seaport/offers | Submit multiple Seaport offers (compatible with OpenSea's API response)
[**post_tokens_refresh_v1**](XDeprecatedApi.md#post_tokens_refresh_v1) | **POST** /tokens/refresh/v1 | Refresh Token
[**post_tokens_simulatefloor_v1**](XDeprecatedApi.md#post_tokens_simulatefloor_v1) | **POST** /tokens/simulate-floor/v1 | Simulate the floor ask of any token
[**post_tokens_simulatetopbid_v1**](XDeprecatedApi.md#post_tokens_simulatetopbid_v1) | **POST** /tokens/simulate-top-bid/v1 | Simulate the top bid of any token
[**post_tokensets_v1**](XDeprecatedApi.md#post_tokensets_v1) | **POST** /token-sets/v1 | Create Token Set



## get_attributes_v1

> models::GetAttributesV1Response get_attributes_v1(collection)
List of attributes

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |

### Return type

[**models::GetAttributesV1Response**](getAttributesV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collection_v1

> models::GetCollectionV1Response get_collection_v1(id, slug)
Get detailed information about a single collection

Get detailed information about a single collection, including real-time stats.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**slug** | Option<**String**> | Filter to a particular slug, e.g. `boredapeyachtclub` |  |

### Return type

[**models::GetCollectionV1Response**](getCollectionV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collection_v2

> models::GetCollectionV2Response get_collection_v2(id, slug)
Single Collection

Get detailed information about a single collection, including real-time stats.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**slug** | Option<**String**> | Filter to a particular collection slug. Example: `boredapeyachtclub` |  |

### Return type

[**models::GetCollectionV2Response**](getCollectionV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collection_v3

> models::GetCollectionV3Response get_collection_v3(id, slug, include_top_bid)
Single Collection

Get detailed information about a single collection, including real-time stats.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**slug** | Option<**String**> | Filter to a particular collection slug. Example: `boredapeyachtclub` |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]

### Return type

[**models::GetCollectionV3Response**](getCollectionV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_activity_v4

> models::GetCollectionActivityV4Response get_collections_activity_v4(collection, collections_set_id, community, limit, sort_by, continuation, include_metadata, types)
Collection activity

This API can be used to build a feed for a collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**limit** | Option<**i32**> | Amount of items returned in response. If `includeMetadata=true` max limit is 20, otherwise max limit is 1,000. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetCollectionActivityV4Response**](getCollectionActivityV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_activity_v5

> models::GetCollectionActivityV5Response get_collections_activity_v5(collection, collections_set_id, community, attributes, limit, sort_by, continuation, include_metadata, types)
Collection activity

This API can be used to build a feed for a collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Example: `attributes[Type]=Original` |  |
**limit** | Option<**i32**> | Amount of items returned. Max limit is 50. |  |[default to 50]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetCollectionActivityV5Response**](getCollectionActivityV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_activity_v7

> models::GetCollectionActivityV6Response get_collections_activity_v7(collection, collections_set_id, community, attributes, exclude_spam, exclude_nsfw, limit, sort_by, continuation, include_metadata, types, display_currency, index_name)
Collection activity

This API can be used to build a feed for a collection including sales, asks, transfers, mints, bids, cancelled bids, and cancelled asks types.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/collections/activity/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original` or `https://api.reservoir.tools/collections/activity/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original&attribute[Type]=Sibling` |  |
**exclude_spam** | Option<**bool**> | If true, will filter any activities marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any activities marked as nsfw. |  |[default to false]
**limit** | Option<**i32**> | Amount of items returned. Max limit is 50 when `includedMetadata=true` otherwise max limit is 1000. |  |[default to 50]
**sort_by** | Option<**String**> | Order the items are returned in the response. The blockchain event time is `eventTimestamp`. The event time recorded is `createdAt`. |  |[default to eventTimestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. If true, max limit is 50. |  |[default to true]
**types** | Option<[**Vec<String>**](String.md)> |  |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |
**index_name** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |

### Return type

[**models::GetCollectionActivityV6Response**](getCollectionActivityV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_activity_v1

> models::GetUserActivityV2Response get_collections_collection_activity_v1(collection, limit, continuation, types)
Collection activity

This API can be used to build a feed for a collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**continuation** | Option<**f64**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetUserActivityV2Response**](getUserActivityV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_activity_v2

> models::GetCollectionActivityV2Response get_collections_collection_activity_v2(collection, limit, sort_by, continuation, include_metadata, types)
Collection activity

This API can be used to build a feed for a collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. If `includeMetadata=true` max limit is 20, otherwise max limit is 1,000. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetCollectionActivityV2Response**](getCollectionActivityV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_activity_v3

> models::GetCollectionActivityV3Response get_collections_collection_activity_v3(collection, limit, sort_by, continuation, include_metadata, types)
Collection activity

This API can be used to build a feed for a collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. If `includeMetadata=true` max limit is 20, otherwise max limit is 1,000. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetCollectionActivityV3Response**](getCollectionActivityV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_all_v1

> models::GetAttributesV1Response get_collections_collection_attributes_all_v1(collection)
Get all attributes in a collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |

### Return type

[**models::GetAttributesV1Response**](getAttributesV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_all_v2

> models::GetAttributesAllV2Response get_collections_collection_attributes_all_v2(collection)
All attributes

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |

### Return type

[**models::GetAttributesAllV2Response**](getAttributesAllV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_all_v3

> models::GetAttributesAllV3Response get_collections_collection_attributes_all_v3(collection)
All attributes

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |

### Return type

[**models::GetAttributesAllV3Response**](getAttributesAllV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_explore_v1

> models::GetCollectionAttributesV1Response get_collections_collection_attributes_explore_v1(collection, attribute_key, sort_by, offset, limit)
Get detailed aggregate about attributes in a collection, e.g. trait floors

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**attribute_key** | Option<**String**> | Filter to a particular attribute key, e.g. `Composition` |  |
**sort_by** | Option<**String**> |  |  |[default to floorAskPrice]
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetCollectionAttributesV1Response**](getCollectionAttributesV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_explore_v2

> models::GetAttributesExploreV2Response get_collections_collection_attributes_explore_v2(collection, attribute_key, max_floor_ask_prices, max_last_sells, sort_by, offset, limit)
Explore attributes

Get detailed aggregate about attributes in a collection, attribute floors

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**attribute_key** | Option<**String**> | Filter to a particular attribute key. Example: `Composition` |  |
**max_floor_ask_prices** | Option<**i32**> | Max number of items returned in the response. |  |[default to 1]
**max_last_sells** | Option<**i32**> | Max number of items returned in the response. |  |[default to 0]
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to floorAskPrice]
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetAttributesExploreV2Response**](getAttributesExploreV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_explore_v3

> models::GetAttributesExploreV3Response get_collections_collection_attributes_explore_v3(collection, include_top_bid, attribute_key, max_floor_ask_prices, max_last_sells, sort_by, offset, limit)
Explore attributes

Get detailed aggregate about attributes in a collection, attribute floors

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**attribute_key** | Option<**String**> | Filter to a particular attribute key. Example: `Composition` |  |
**max_floor_ask_prices** | Option<**i32**> | Max number of items returned in the response. |  |[default to 1]
**max_last_sells** | Option<**i32**> | Max number of items returned in the response. |  |[default to 0]
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to floorAskPrice]
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetAttributesExploreV3Response**](getAttributesExploreV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_explore_v4

> models::GetAttributesExploreV4Response get_collections_collection_attributes_explore_v4(collection, token_id, include_top_bid, exclude_range_traits, exclude_number_traits, attribute_key, max_floor_ask_prices, max_last_sells, continuation, limit)
Explore attributes

Use this API to see stats on a specific attribute within a collection. This endpoint will return `tokenCount`, `onSaleCount`, `sampleImages`, and `floorAsk` by default. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**token_id** | Option<**String**> | Filter to a particular token-id. Example: `1` |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**exclude_range_traits** | Option<**bool**> | If true, range traits will be excluded from the response. |  |[default to false]
**exclude_number_traits** | Option<**bool**> | If true, number traits will be excluded from the response. |  |[default to false]
**attribute_key** | Option<**String**> | Filter to a particular attribute key. Example: `Composition` |  |
**max_floor_ask_prices** | Option<**i32**> | Max number of items returned in the response. |  |[default to 1]
**max_last_sells** | Option<**i32**> | Max number of items returned in the response. |  |[default to 0]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Default limit is 20. Max limit is 5000. |  |[default to 20]

### Return type

[**models::GetAttributesExploreV4Response**](getAttributesExploreV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_static_v1

> models::GetAttributesStaticV1Response get_collections_collection_attributes_static_v1(collection)
All attributes + token ids

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |

### Return type

[**models::GetAttributesStaticV1Response**](getAttributesStaticV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_attributes_v1

> models::GetCollectionAttributesV1Response get_collections_collection_attributes_v1(collection, attribute_key, sort_by, offset, limit)
Get detailed aggregate about attributes in a collection, e.g. trait floors

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**attribute_key** | Option<**String**> |  |  |
**sort_by** | Option<**String**> |  |  |[default to floorAskPrice]
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetCollectionAttributesV1Response**](getCollectionAttributesV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_marketplaceconfigurations_v1

> models::Model282 get_collections_collection_marketplaceconfigurations_v1(collection, token_id)
Marketplace configurations by collection

This API returns recommended marketplace configurations given a collection id

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**token_id** | Option<**String**> | When set, token-level royalties will be returned in the response |  |

### Return type

[**models::Model282**](Model282.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_supportedmarketplaces_v1

> models::Model267 get_collections_collection_supportedmarketplaces_v1(collection, token_id)
Supported marketplaces by collection

The ReservoirKit `ListModal` client utilizes this API to identify the marketplace(s) it can list on.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**token_id** | Option<**String**> | When set, token-level royalties will be returned in the response |  |

### Return type

[**models::Model267**](Model267.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_topbids_v1

> models::GetCollectionTopBidsV1Response get_collections_collection_topbids_v1(collection)
Bid distribution

When users are placing collection or trait bids, this API can be used to show them where the bid is in the context of other bids, and how many tokens it will be the top bid for.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |

### Return type

[**models::GetCollectionTopBidsV1Response**](getCollectionTopBidsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collectionorslug_v1

> models::GetCollectionDeprecatedV1Response get_collections_collectionorslug_v1(collection_or_slug)
Single collection

Get detailed information about a single collection, including real-time stats.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection_or_slug** | **String** |  | [required] |

### Return type

[**models::GetCollectionDeprecatedV1Response**](getCollectionDeprecatedV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_topselling_v1

> models::GetTopSellingCollectionsV1Response get_collections_topselling_v1(start_time, end_time, fill_type, limit, include_recent_sales)
Top Selling Collections

Get top selling and minting collections

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**start_time** | Option<**f64**> | Start time in unix timestamp. Must be less than 2 weeks ago. defaults to 24 hours |  |[default to 1712471649]
**end_time** | Option<**f64**> | End time in unix timestamp. defaults to now |  |
**fill_type** | Option<**String**> | Fill types to aggregate from (sale, mint, any) |  |[default to any]
**limit** | Option<**i32**> | Amount of items returned in response. Default is 25 and max is 50 |  |[default to 25]
**include_recent_sales** | Option<**bool**> | If true, 8 recent sales will be included in the response |  |[default to false]

### Return type

[**models::GetTopSellingCollectionsV1Response**](getTopSellingCollectionsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_topselling_v2

> models::GetTopSellingCollectionsV2Response get_collections_topselling_v2(period, fill_type, limit, sort_by, include_recent_sales, normalize_royalties, use_non_flagged_floor_ask)
Top selling collections

Get top selling and minting collections

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**period** | Option<**String**> | Time window to aggregate. |  |[default to 1d]
**fill_type** | Option<**String**> | Fill types to aggregate from (sale, mint, any) |  |[default to sale]
**limit** | Option<**i32**> | Amount of items returned in response. Default is 25 and max is 50 |  |[default to 25]
**sort_by** | Option<**String**> |  |  |[default to sales]
**include_recent_sales** | Option<**bool**> | If true, 8 recent sales will be included in the response |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, return the non flagged floor ask. Supported only when `normalizeRoyalties` is false. |  |[default to false]

### Return type

[**models::GetTopSellingCollectionsV2Response**](getTopSellingCollectionsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_v1

> models::GetCollectionsV1Response get_collections_v1(community, contract, name, slug, sort_by, offset, limit)
List of collections

Useful for getting multiple collections to show in a marketplace, or search for particular collections.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**name** | Option<**String**> | Search for collections that match a string, e.g. `bored` |  |
**slug** | Option<**String**> | Filter to a particular slug, e.g. `boredapeyachtclub` |  |
**sort_by** | Option<**String**> |  |  |[default to allTimeVolume]
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetCollectionsV1Response**](getCollectionsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_v2

> models::GetCollectionsV2Response get_collections_v2(community, contract, name, slug, sort_by, offset, limit)
Get a filtered list of collections

Useful for getting multiple collections to show in a marketplace, or search for particular collections.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**name** | Option<**String**> | Search for collections that match a string, e.g. `bored` |  |
**slug** | Option<**String**> | Filter to a particular slug, e.g. `boredapeyachtclub` |  |
**sort_by** | Option<**String**> |  |  |[default to allTimeVolume]
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetCollectionsV2Response**](getCollectionsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_v3

> models::GetCollectionsV3Response get_collections_v3(community, contract, name, slug, sort_by, offset, limit)
Get a filtered list of collections

Useful for getting multiple collections to show in a marketplace, or search for particular collections.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**name** | Option<**String**> | Search for collections that match a string, e.g. `bored` |  |
**slug** | Option<**String**> | Filter to a particular slug, e.g. `boredapeyachtclub` |  |
**sort_by** | Option<**String**> |  |  |[default to allTimeVolume]
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetCollectionsV3Response**](getCollectionsV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_v4

> models::GetCollectionsV4Response get_collections_v4(collections_set_id, community, contract, name, slug, sort_by, include_top_bid, limit, continuation)
Collections

Useful for getting multiple collections to show in a marketplace, or search for particular collections.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**name** | Option<**String**> | Search for collections that match a string. Example: `bored` |  |
**slug** | Option<**String**> | Filter to a particular collection slug. Example: `boredapeyachtclub` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to allTimeVolume]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetCollectionsV4Response**](getCollectionsV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_v5

> models::GetCollectionsV5Response get_collections_v5(id, slug, collections_set_id, community, contract, name, max_floor_ask_price, min_floor_ask_price, include_top_bid, include_attributes, include_sales_count, include_mint_stages, normalize_royalties, use_non_flagged_floor_ask, sort_by, limit, continuation, display_currency)
Collections

Use this API to explore a collection’s metadata and statistics (sales, volume, etc).

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> | Filter to a particular collection with collection id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**slug** | Option<**String**> | Filter to a particular collection slug. Example: `boredapeyachtclub` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**name** | Option<**String**> | Search for collections that match a string. Example: `bored` |  |
**max_floor_ask_price** | Option<**f64**> | Maximum floor price of the collection |  |
**min_floor_ask_price** | Option<**f64**> | Minumum floor price of the collection |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be included in the response. Must filter by `id` or `slug` to a particular collection. |  |
**include_sales_count** | Option<**bool**> | If true, sales count (1 day, 7 day, 30 day, all time) will be included in the response. Must filter by `id` or `slug` to a particular collection. |  |
**include_mint_stages** | Option<**bool**> | If true, mint data for the collection will be included in the response. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, return the non flagged floor ask. Supported only when `normalizeRoyalties` is false. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `#DayVolume`, `createdAt`, or `floorAskPrice` |  |[default to allTimeVolume]
**limit** | Option<**i32**> | Amount of items returned in response. Default and max limit is 20. |  |[default to 20]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetCollectionsV5Response**](getCollectionsV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_v6

> models::GetCollectionsV6Response get_collections_v6(id, slug, collections_set_id, community, contract, creator, name, max_floor_ask_price, min_floor_ask_price, include_attributes, include_sales_count, include_mint_stages, normalize_royalties, use_non_flagged_floor_ask, sort_by, limit, continuation, display_currency)
Collections

Use this API to explore a collection's metadata and statistics (sales, volume, etc).

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> | Filter to a particular collection with collection id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**slug** | Option<**String**> | Filter to a particular collection slug. Example: `boredapeyachtclub` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**creator** | Option<**String**> | Filter by creator |  |
**name** | Option<**String**> | Search for collections that match a string. Example: `bored` |  |
**max_floor_ask_price** | Option<**f64**> | Maximum floor price of the collection |  |
**min_floor_ask_price** | Option<**f64**> | Minumum floor price of the collection |  |
**include_attributes** | Option<**bool**> | If true, attributes will be included in the response. Must filter by `id` or `slug` to a particular collection. |  |
**include_sales_count** | Option<**bool**> | If true, sales count (1 day, 7 day, 30 day, all time) will be included in the response. Must filter by `id` or `slug` to a particular collection. |  |
**include_mint_stages** | Option<**bool**> | If true, mint data for the collection will be included in the response. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, return the non flagged floor ask. Supported only when `normalizeRoyalties` is false. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `#DayVolume`, `createdAt`, or `floorAskPrice` |  |[default to allTimeVolume]
**limit** | Option<**i32**> | Amount of items returned in response. Default and max limit is 20. |  |[default to 20]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetCollectionsV6Response**](getCollectionsV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_currencies_conversion_v1

> models::GetCurrencyConversionV1Response get_currencies_conversion_v1(from, to)
Currency Conversions

Convert an amount in one currency to another

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**from** | Option<**String**> | Currency address or fiat symbol to convert from |  |
**to** | Option<**String**> | Currency address or fiat symbol to convert to |  |

### Return type

[**models::GetCurrencyConversionV1Response**](getCurrencyConversionV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_asks_v2

> models::GetAsksEventsV2Response get_events_asks_v2(contract, start_timestamp, end_timestamp, sort_direction, continuation, limit, normalize_royalties, display_currency)
Asks status changes

Get updates any time an asks status changes

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetAsksEventsV2Response**](getAsksEventsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_bids_v1

> models::GetBidEventsV1Response get_events_bids_v1(contract, start_timestamp, end_timestamp, include_criteria, sort_direction, continuation, limit)
Bid status changes

Get updates any time a bid status changes

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**include_criteria** | Option<**bool**> | If true, bid criteria is included in the response. |  |[default to false]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetBidEventsV1Response**](getBidEventsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_bids_v2

> models::GetBidEventsV2Response get_events_bids_v2(contract, start_timestamp, end_timestamp, include_criteria_metadata, sort_direction, continuation, limit)
Bid status changes

Get updates any time a bid status changes

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to false]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetBidEventsV2Response**](getBidEventsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_collections_floorask_v1

> models::GetCollectionsFloorAskV1Response get_events_collections_floorask_v1(collection, start_timestamp, end_timestamp, normalize_royalties, exclude_flagged_tokens, sort_direction, continuation, limit)
Collection floor changes

Every time the floor price of a collection changes (i.e. the 'floor ask'), an event is generated. This API is designed to be polled at high frequency, in order to keep an external system in sync with accurate prices for any token.  There are multiple event types, which describe what caused the change in price:  - `new-order` > new listing at a lower price  - `expiry` > the previous best listing expired  - `sale` > the previous best listing was filled  - `cancel` > the previous best listing was cancelled  - `balance-change` > the best listing was invalidated due to no longer owning the NFT  - `approval-change` > the best listing was invalidated due to revoked approval  - `revalidation` > manual revalidation of orders (e.g. after a bug fixed)  - `reprice` > price update for dynamic orders (e.g. dutch auctions)  - `bootstrap` > initial loading of data, so that all tokens have a price associated  Some considerations to keep in mind  - Due to the complex nature of monitoring off-chain liquidity across multiple marketplaces, including dealing with block re-orgs, events should be considered 'relative' to the perspective of the indexer, ie _when they were discovered_, rather than _when they happened_. A more deterministic historical record of price changes is in development, but in the meantime, this method is sufficent for keeping an external system in sync with the best available prices.  - Events are only generated if the best price changes. So if a new order or sale happens without changing the best price, no event is generated. This is more common with 1155 tokens, which have multiple owners and more depth. For this reason, if you need sales data, use the Sales API.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**exclude_flagged_tokens** | Option<**bool**> | If true, will exclude floor asks on flagged tokens. (only supported when `normalizeRoyalties` is false) |  |[default to false]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetCollectionsFloorAskV1Response**](getCollectionsFloorAskV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_collections_topbid_v1

> models::GetCollectionsTopbidV1Response get_events_collections_topbid_v1(collection, start_timestamp, end_timestamp, sort_direction, continuation, limit)
Collection top bid changes

Every time the top offer of a collection changes (i.e. the 'top bid'), an event is generated. This API is designed to be polled at high frequency.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetCollectionsTopbidV1Response**](getCollectionsTopbidV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_orders_v1

> models::GetOrderEventsV1Response get_events_orders_v1(contract, start_timestamp, end_timestamp, sort_direction, continuation, limit)
Order status changes

Get updates any time an order status changes

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetOrderEventsV1Response**](getOrderEventsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_tokens_floorask_v2

> models::GetTokensFloorAskV2Response get_events_tokens_floorask_v2(contract, token, start_timestamp, end_timestamp, sort_direction, continuation, limit)
Token price changes

Every time the best price of a token changes (i.e. the 'floor ask'), an event is generated. This API is designed to be polled at high frequency, in order to keep an external system in sync with accurate prices for any token.  There are multiple event types, which describe what caused the change in price:  - `new-order` > new listing at a lower price  - `expiry` > the previous best listing expired  - `sale` > the previous best listing was filled  - `cancel` > the previous best listing was cancelled  - `balance-change` > the best listing was invalidated due to no longer owning the NFT  - `approval-change` > the best listing was invalidated due to revoked approval  - `revalidation` > manual revalidation of orders (e.g. after a bug fixed)  - `reprice` > price update for dynamic orders (e.g. dutch auctions)  - `bootstrap` > initial loading of data, so that all tokens have a price associated  Some considerations to keep in mind  - Due to the complex nature of monitoring off-chain liquidity across multiple marketplaces, including dealing with block re-orgs, events should be considered 'relative' to the perspective of the indexer, ie _when they were discovered_, rather than _when they happened_. A more deterministic historical record of price changes is in development, but in the meantime, this method is sufficent for keeping an external system in sync with the best available prices.  - Events are only generated if the best price changes. So if a new order or sale happens without changing the best price, no event is generated. This is more common with 1155 tokens, which have multiple owners and more depth. For this reason, if you need sales data, use the Sales API.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> |  |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**sort_direction** | Option<**String**> |  |  |[default to desc]
**continuation** | Option<**String**> |  |  |
**limit** | Option<**i32**> |  |  |[default to 50]

### Return type

[**models::GetTokensFloorAskV2Response**](getTokensFloorAskV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_tokens_floorask_v3

> models::GetTokensFloorAskV3Response get_events_tokens_floorask_v3(contract, token, start_timestamp, end_timestamp, sort_direction, normalize_royalties, continuation, limit, display_currency)
Token price changes

Every time the best price of a token changes (i.e. the 'floor ask'), an event is generated. This API is designed to be polled at high frequency, in order to keep an external system in sync with accurate prices for any token.  There are multiple event types, which describe what caused the change in price:  - `new-order` > new listing at a lower price  - `expiry` > the previous best listing expired  - `sale` > the previous best listing was filled  - `cancel` > the previous best listing was cancelled  - `balance-change` > the best listing was invalidated due to no longer owning the NFT  - `approval-change` > the best listing was invalidated due to revoked approval  - `revalidation` > manual revalidation of orders (e.g. after a bug fixed)  - `reprice` > price update for dynamic orders (e.g. dutch auctions)  - `bootstrap` > initial loading of data, so that all tokens have a price associated  Some considerations to keep in mind  - Due to the complex nature of monitoring off-chain liquidity across multiple marketplaces, including dealing with block re-orgs, events should be considered 'relative' to the perspective of the indexer, ie _when they were discovered_, rather than _when they happened_. A more deterministic historical record of price changes is in development, but in the meantime, this method is sufficent for keeping an external system in sync with the best available prices.  - Events are only generated if the best price changes. So if a new order or sale happens without changing the best price, no event is generated. This is more common with 1155 tokens, which have multiple owners and more depth. For this reason, if you need sales data, use the Sales API.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> |  |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**sort_direction** | Option<**String**> |  |  |[default to desc]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**continuation** | Option<**String**> |  |  |
**limit** | Option<**i32**> |  |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetTokensFloorAskV3Response**](getTokensFloorAskV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_execute_cancel_v2

> models::GetExecuteCancelV2Response get_execute_cancel_v2(id, max_fee_per_gas, max_priority_fee_per_gas)
Cancel order

Cancel an existing order on any marketplace

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | Order Id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**max_fee_per_gas** | Option<**String**> | Optional. Set custom gas price |  |
**max_priority_fee_per_gas** | Option<**String**> | Optional. Set custom gas price |  |

### Return type

[**models::GetExecuteCancelV2Response**](getExecuteCancelV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_liquidity_users_v1

> models::GetUsersLiquidityV1Response get_liquidity_users_v1(collection, user, offset, limit)
User bid liquidity rankings

This API calculates the total liquidity created by users, based on the number of tokens they are top bidder for.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**user** | Option<**String**> | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetUsersLiquidityV1Response**](getUsersLiquidityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_liquidity_users_v2

> models::GetUsersLiquidityV1Response get_liquidity_users_v2(collection, offset, limit)
User bid liquidity rankings

This API calculates the total liquidity created by users, based on the number of tokens they are top bidder for.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetUsersLiquidityV1Response**](getUsersLiquidityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_oracle_collections_floorask_v4

> models::GetCollectionFloorAskOracleV4Response get_oracle_collections_floorask_v4(kind, currency, twap_seconds, eip3668_calldata, collection, token, use_non_flagged_floor_ask)
Collection floor

Get a signed message of any collection's floor price (spot or twap). The oracle's address is 0x32dA57E736E05f75aa4FaE2E9Be60FD904492726.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**kind** | Option<**String**> |  |  |[default to spot]
**currency** | Option<**String**> |  |  |[default to 0x0000000000000000000000000000000000000000]
**twap_seconds** | Option<**f64**> |  |  |[default to 86400.0]
**eip3668_calldata** | Option<**String**> |  |  |
**collection** | Option<**String**> |  |  |
**token** | Option<**String**> |  |  |
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will use the collection non flagged floor ask events. |  |[default to false]

### Return type

[**models::GetCollectionFloorAskOracleV4Response**](getCollectionFloorAskOracleV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_oracle_collections_floorask_v5

> models::GetCollectionFloorAskOracleV4Response get_oracle_collections_floorask_v5(kind, currency, twap_seconds, eip3668_calldata, collection, token, use_non_flagged_floor_ask, signer)
Collection floor

Get a signed message of any collection's floor price (spot or twap). The oracle's address is 0xAeB1D03929bF87F69888f381e73FBf75753d75AF. The address is the same for all chains.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**kind** | Option<**String**> |  |  |[default to spot]
**currency** | Option<**String**> |  |  |[default to 0x0000000000000000000000000000000000000000]
**twap_seconds** | Option<**f64**> |  |  |[default to 86400.0]
**eip3668_calldata** | Option<**String**> |  |  |
**collection** | Option<**String**> |  |  |
**token** | Option<**String**> |  |  |
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will use the collection non flagged floor ask events. |  |[default to false]
**signer** | Option<**String**> |  |  |[default to 0xaeb1d03929bf87f69888f381e73fbf75753d75af]

### Return type

[**models::GetCollectionFloorAskOracleV4Response**](getCollectionFloorAskOracleV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_oracle_collections_topbid_v2

> models::GetCollectionTopBidOracleV2Response get_oracle_collections_topbid_v2(kind, currency, twap_seconds, collection, token, signer)
Collection top bid oracle

Get a signed message of any collection's top bid price (spot or twap). The oracle's address is 0xAeB1D03929bF87F69888f381e73FBf75753d75AF. The address is the same for all chains.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**kind** | Option<**String**> |  |  |[default to spot]
**currency** | Option<**String**> |  |  |[default to 0x0000000000000000000000000000000000000000]
**twap_seconds** | Option<**f64**> |  |  |[default to 86400.0]
**collection** | Option<**String**> |  |  |
**token** | Option<**String**> |  |  |
**signer** | Option<**String**> |  |  |[default to 0xaeb1d03929bf87f69888f381e73fbf75753d75af]

### Return type

[**models::GetCollectionTopBidOracleV2Response**](getCollectionTopBidOracleV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_oracle_tokens_status_v2

> models::GetTokenStatusOracleV2Response get_oracle_tokens_status_v2(tokens, signer)
Token status oracle

Get a signed message of a token's details (flagged status and last transfer time). The oracle's address is 0xAeB1D03929bF87F69888f381e73FBf75753d75AF. The address is the same for all chains.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**tokens** | [**Vec<String>**](String.md) |  | [required] |
**signer** | Option<**String**> |  |  |[default to 0xaeb1d03929bf87f69888f381e73fbf75753d75af]

### Return type

[**models::GetTokenStatusOracleV2Response**](getTokenStatusOracleV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_all_v1

> models::GetOrdersAllV1Response get_orders_all_v1(id, source, native, side, include_metadata, include_raw_data, continuation, limit)
Bulk historical orders

This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> |  |  |
**source** | Option<**String**> | Filter to a source by domain. Example: `opensea.io` |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**side** | Option<**String**> | Sell or buy side. |  |[default to sell]
**include_metadata** | Option<**bool**> | If true, metadata will be included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data will be included in the response. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetOrdersAllV1Response**](getOrdersAllV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_all_v2

> models::GetOrdersAllV2Response get_orders_all_v2(id, source, native, side, include_metadata, include_raw_data, continuation, limit, display_currency)
Bulk historical orders

This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> |  |  |
**source** | Option<**String**> | Filter to a source by domain. Example: `opensea.io` |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**side** | Option<**String**> | Sell or buy side. |  |[default to sell]
**include_metadata** | Option<**bool**> | If true, metadata will be included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data will be included in the response. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetOrdersAllV2Response**](getOrdersAllV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_asks_v1

> models::GetOrdersAsksV1Response get_orders_asks_v1(token, maker, contract, status, sort_by, continuation, limit)
Get a list of asks (listings), filtered by token, collection or maker

This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | Option<**String**> | Filter to a token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**maker** | Option<**String**> | Filter to a particular user, e.g. `0x4d04eb67a2d1e01c71fad0366e0c200207a75487` |  |
**contract** | Option<**String**> | Filter to a particular user, e.g. `0x4d04eb67a2d1e01c71fad0366e0c200207a75487` |  |
**status** | Option<**String**> | `active` = currently valid, `inactive` = temporarily invalid  Available when filtering by maker, otherwise only valid orders will be returned |  |
**sort_by** | Option<**String**> |  |  |
**continuation** | Option<**String**> |  |  |
**limit** | Option<**i32**> |  |  |[default to 50]

### Return type

[**models::GetOrdersAsksV1Response**](getOrdersAsksV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_asks_v2

> models::GetOrdersAsksV1Response get_orders_asks_v2(token, maker, contracts, status, include_private, sort_by, continuation, limit)
Asks (listings)

Get a list of asks (listings), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**maker** | Option<**String**> | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**contracts** | Option<[**Vec<String>**](String.md)> |  |  |
**status** | Option<**String**> | active = currently valid, inactive = temporarily invalid  Available when filtering by maker, otherwise only valid orders will be returned |  |
**include_private** | Option<**bool**> | When true, private orders are included in the response. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to createdAt]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetOrdersAsksV1Response**](getOrdersAsksV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_asks_v3

> models::GetOrdersAsksV3Response get_orders_asks_v3(ids, token, maker, community, contracts, status, source, native, include_private, include_metadata, include_raw_data, start_timestamp, end_timestamp, normalize_royalties, sort_by, continuation, limit, display_currency)
Asks (listings)

Get a list of asks (listings), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<**String**> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**maker** | Option<**String**> | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contracts** | Option<[**Vec<String>**](String.md)> |  |  |
**status** | Option<**String**> | active = currently valid inactive = temporarily invalid expired, cancelled, filled = permanently invalid  Available when filtering by maker, otherwise only valid orders will be returned |  |
**source** | Option<[**Vec<String>**](String.md)> |  |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**include_private** | Option<**bool**> | If true, private orders are included in the response. |  |[default to false]
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response, Sorting by price allowed only when filtering by token |  |[default to createdAt]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetOrdersAsksV3Response**](getOrdersAsksV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_asks_v4

> models::GetOrdersAsksV4Response get_orders_asks_v4(ids, token, token_set_id, maker, community, collections_set_id, contracts_set_id, contracts, status, source, native, include_private, include_criteria_metadata, include_raw_data, include_dynamic_pricing, exclude_eoa, start_timestamp, end_timestamp, normalize_royalties, sort_by, sort_direction, continuation, limit, display_currency)
Asks (listings)

Get a list of asks (listings), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing.   Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<[**Vec<String>**](String.md)> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**maker** | Option<**String**> | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**contracts** | Option<[**Vec<String>**](String.md)> |  |  |
**status** | Option<**String**> | activeª^º = currently valid inactiveª^ = temporarily invalid expiredª^, canceledª^, filledª^ = permanently invalid anyªº = any status ª when an `id` is passed ^ when a `maker` is passed º when a `contract` is passed |  |
**source** | Option<**String**> | Filter to a source by domain. Only active listed will be returned. Example: `opensea.io` |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**include_private** | Option<**bool**> | If true, private orders are included in the response. |  |[default to false]
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**include_dynamic_pricing** | Option<**bool**> | If true, dynamic pricing data will be returned in the response. |  |[default to false]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. |  |[default to false]
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response, Sorting by price allowed only when filtering by token |  |[default to createdAt]
**sort_direction** | Option<**String**> |  |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetOrdersAsksV4Response**](getOrdersAsksV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_bids_v1

> models::GetOrdersBidsV1Response get_orders_bids_v1(token, token_set_id, maker, status, sort_by, continuation, limit)
Get a list of bids (offers), filtered by token, collection or maker

This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | Option<**String**> | Filter to a token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**maker** | Option<**String**> | Filter to a particular user, e.g. `0x4d04eb67a2d1e01c71fad0366e0c200207a75487` |  |
**status** | Option<**String**> | `active` = currently valid, `inactive` = temporarily invalid, `expired` = permanently invalid  Available when filtering by maker, otherwise only valid orders will be returned |  |
**sort_by** | Option<**String**> |  |  |
**continuation** | Option<**String**> |  |  |
**limit** | Option<**i32**> |  |  |[default to 50]

### Return type

[**models::GetOrdersBidsV1Response**](getOrdersBidsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_bids_v2

> models::GetOrdersBidsV1Response get_orders_bids_v2(token, token_set_id, maker, contracts, status, sort_by, continuation, limit)
Bids (offers)

Get a list of bids (offers), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**maker** | Option<**String**> | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**contracts** | Option<[**Vec<String>**](String.md)> | Filter to an array of contracts. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**status** | Option<**String**> | active = currently valid, inactive = temporarily invalid, expired = permanently invalid  Available when filtering by maker, otherwise only valid orders will be returned |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to createdAt]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]

### Return type

[**models::GetOrdersBidsV1Response**](getOrdersBidsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_bids_v3

> models::GetOrdersBidsV3Response get_orders_bids_v3(ids, token, token_set_id, maker, contracts, status, source, native, include_metadata, include_raw_data, sort_by, continuation, limit, display_currency)
Bids (offers)

Get a list of bids (offers), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<**String**> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**maker** | Option<**String**> | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**contracts** | Option<[**Vec<String>**](String.md)> | Filter to an array of contracts. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**status** | Option<**String**> | active = currently valid, inactive = temporarily invalid, expired = permanently invalid  Available when filtering by maker, otherwise only valid orders will be returned |  |
**source** | Option<**String**> | Filter to a source by domain. Example: `opensea.io` |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response, Sorting by price allowed only when filtering by token |  |[default to createdAt]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetOrdersBidsV3Response**](getOrdersBidsV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_bids_v4

> models::GetOrdersBidsV4Response get_orders_bids_v4(ids, token, token_set_id, maker, community, collection, attribute, contracts, status, source, native, include_metadata, include_raw_data, normalize_royalties, sort_by, continuation, limit, display_currency)
Bids (offers)

Get a list of bids (offers), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<**String**> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set. Example: `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` or `token:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:1` |  |
**maker** | Option<**String**> | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collection** | Option<**String**> | Filter to a particular collection bids with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attribute** | Option<**String**> | Filter to a particular attribute within a collection. Example: `attribute[Mouth]=Bored` (Collection must be passed as well when filtering by attribute) |  |
**contracts** | Option<[**Vec<String>**](String.md)> | Filter to an array of contracts. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**status** | Option<**String**> | active = currently valid inactive = temporarily invalid expired, cancelled, filled = permanently invalid  Available when filtering by maker, otherwise only valid orders will be returned |  |
**source** | Option<**String**> | Filter to a source by domain. Example: `opensea.io` |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response, Sorting by price allowed only when filtering by token |  |[default to createdAt]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetOrdersBidsV4Response**](getOrdersBidsV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_bids_v5

> models::GetOrdersBidsV5Response get_orders_bids_v5(ids, token, token_set_id, maker, community, collections_set_id, contracts_set_id, collection, attribute, contracts, status, source, native, include_criteria_metadata, include_raw_data, include_depth, start_timestamp, end_timestamp, exclude_eoa, normalize_royalties, sort_by, sort_direction, continuation, limit, display_currency)
Bids (offers)

Get a list of bids (offers), filtered by token, collection or maker. This API is designed for efficiently ingesting large volumes of orders, for external processing.   There are a different kind of bids than can be returned:  - Inputting a 'contract' will return token and attribute bids.  - Inputting a 'collection-id' will return collection wide bids./n/n Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | Option<[**Vec<String>**](String.md)> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set. Example: `token:CONTRACT:TOKEN_ID` representing a single token within contract, `contract:CONTRACT` representing a whole contract, `range:CONTRACT:START_TOKEN_ID:END_TOKEN_ID` representing a continuous token id range within a contract and `list:CONTRACT:TOKEN_IDS_HASH` representing a list of token ids within a contract. |  |
**maker** | Option<**String**> | Filter to a particular user. Must set `source=blur.io` to reveal maker's blur bids. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**collection** | Option<**String**> | Filter to a particular collection bids with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attribute** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/orders/bids/v5?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original` or `https://api.reservoir.tools/orders/bids/v5?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original&attribute[Type]=Sibling`(Collection must be passed as well when filtering by attribute) |  |
**contracts** | Option<[**Vec<String>**](String.md)> | Filter to an array of contracts. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**status** | Option<**String**> | activeª^º = currently valid inactiveª^ = temporarily invalid expiredª^, canceledª^, filledª^ = permanently invalid anyªº = any status ª when an `id` is passed ^ when a `maker` is passed º when a `contract` is passed |  |
**source** | Option<**String**> | Filter to a source by domain. Only active listed will be returned. Must set `rawData=true` to reveal individual bids when `source=blur.io`. Example: `opensea.io` |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. Set `source=blur.io` and make this `true` to reveal individual blur bids. |  |[default to false]
**include_depth** | Option<**bool**> | If true, the depth of each order is included in the response. |  |[default to false]
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to createdAt]
**sort_direction** | Option<**String**> |  |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetOrdersBidsV5Response**](getOrdersBidsV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_executed_v1

> String get_orders_executed_v1(ids)
Order status

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ids** | [**Vec<String>**](String.md) |  | [required] |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_users_user_topbids_v1

> models::GetUserTopBidsV1Response get_orders_users_user_topbids_v1(user, collection, community, optimize_checkout_url, normalize_royalties, continuation, sort_by, sort_direction, limit)
User Top Bids

Return the top bids for the given user tokens

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**collection** | Option<**String**> |  |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**optimize_checkout_url** | Option<**bool**> | If true, urls will only be returned for optimized sources that support royalties. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**sort_by** | Option<**String**> | Order of the items are returned in the response. |  |[default to topBidValue]
**sort_direction** | Option<**String**> |  |  |[default to desc]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetUserTopBidsV1Response**](getUserTopBidsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_users_user_topbids_v2

> models::GetUserTopBidsV2Response get_orders_users_user_topbids_v2(user, collection, community, optimize_checkout_url, include_criteria_metadata, normalize_royalties, use_non_flagged_floor_ask, continuation, sort_by, sort_direction, limit)
User Top Bids

Return the top bids for the given user tokens

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**collection** | Option<**String**> |  |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**optimize_checkout_url** | Option<**bool**> | If true, urls will only be returned for optimized sources that support royalties. |  |[default to false]
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to true]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will return the collection non flagged floor ask events. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**sort_by** | Option<**String**> | Order of the items are returned in the response. |  |[default to topBidValue]
**sort_direction** | Option<**String**> |  |  |[default to desc]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetUserTopBidsV2Response**](getUserTopBidsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_users_user_topbids_v3

> models::GetUserTopBidsV3Response get_orders_users_user_topbids_v3(user, collection, contracts_set_id, community, collections_set_id, optimize_checkout_url, include_criteria_metadata, normalize_royalties, use_non_flagged_floor_ask, continuation, sort_by, sort_direction, limit, sample_size, display_currency)
User Top Bids

Return the top bids for the given user tokens

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**collection** | Option<**String**> |  |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**optimize_checkout_url** | Option<**bool**> | If true, urls will only be returned for optimized sources that support royalties. |  |[default to false]
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to true]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will return the collection non flagged floor ask events. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**sort_by** | Option<**String**> | Order of the items are returned in the response. |  |[default to topBidValue]
**sort_direction** | Option<**String**> |  |  |[default to desc]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**sample_size** | Option<**i32**> | Amount of tokens considered. |  |[default to 10000]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetUserTopBidsV3Response**](getUserTopBidsV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_v1

> models::GetOrdersV1Response get_orders_v1(id, token, token_set_id, offset, limit)
List of valid orders.

Access orders with various filters applied. If you need orders created by a single user, use the positions API instead.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> |  |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetOrdersV1Response**](getOrdersV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_orders_v2

> models::GetOrdersV2Response get_orders_v2(id, token, token_set_id, offset, limit)
Submit order batch

Access orders with various filters applied. If you need orders created by a single user, use the positions API instead.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> |  |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set. Example: `token:CONTRACT:TOKEN_ID` representing a single token within contract, `contract:CONTRACT` representing a whole contract, `range:CONTRACT:START_TOKEN_ID:END_TOKEN_ID` representing a continuous token id range within a contract and `list:CONTRACT:TOKEN_IDS_HASH` representing a list of token ids within a contract. |  |
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetOrdersV2Response**](getOrdersV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_owners_v1

> models::GetOwnersV1Response get_owners_v1(collections_set_id, collection, contract, token, attributes, offset, limit)
Owners

Get owners with various filters applied, and a summary of their ownership. Useful for exploring top owners in a collection or attribute.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/owners/v1?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/owners/v1?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetOwnersV1Response**](getOwnersV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_pendingtxs_tokens_v1

> models::GetPendingTokensV1Response get_pendingtxs_tokens_v1(contract)
Pending tokens

Get tokens which have a pending sale transaction

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |

### Return type

[**models::GetPendingTokensV1Response**](getPendingTokensV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_redirect_logo_v1

> String get_redirect_logo_v1(source)
Redirect response to the given source logo

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**source** | **String** |  | [required] |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_redirect_token_v1

> String get_redirect_token_v1(source, token)
Redirect response to the given source token page

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**source** | **String** |  | [required] |
**token** | **String** | Redirect to the given token page, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [required] |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_sales_bulk_v1

> models::GetSalesBulkV1Response get_sales_bulk_v1(contract, token, start_timestamp, end_timestamp, limit, continuation)
Bulk historical sales

Note: this API is optimized for bulk access, and offers minimal filters/metadata. If you need more flexibility, try the `NFT API > Sales` endpoint

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 100]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetSalesBulkV1Response**](getSalesBulkV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_sales_v1

> models::GetSalesV1Response get_sales_v1(collection, contract, token, offset, limit)
Historical sales

Get recent sales for a contract or token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetSalesV1Response**](getSalesV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_sales_v2

> models::GetSalesV2Response get_sales_v2(collection, contract, token, offset, limit)
Historical sales

Get recent sales for a contract or token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetSalesV2Response**](getSalesV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_sales_v3

> models::GetSalesV3Response get_sales_v3(contract, token, collection, attributes, tx_hash, start_timestamp, end_timestamp, limit, continuation)
Historical sales

Get recent sales for a contract or token. Note: this API is returns rich metadata, and has advanced filters, so is only designed for small amounts of recent sales. If you want access to sales in bulk, use the `Aggregator > Bulk Sales` API.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Example: `attributes[Type]=Original` |  |
**tx_hash** | Option<**String**> | Filter to a particular transaction. Example: `0x04654cc4c81882ed4d20b958e0eeb107915d75730110cce65333221439de6afc` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetSalesV3Response**](getSalesV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_sales_v4

> models::GetSalesV4Response get_sales_v4(contract, token, include_token_metadata, include_deleted, collection, attributes, order_by, sort_direction, tx_hash, start_timestamp, end_timestamp, limit, continuation)
Sales

Get recent sales for a contract or token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**include_token_metadata** | Option<**bool**> | If enabled, also include token metadata in the response. |  |
**include_deleted** | Option<**bool**> | If enabled, include sales that have been deleted. In some cases the backfilling process deletes sales that are no longer relevant or have been reverted. |  |[default to false]
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/sales/v4?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/sales/v4?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**order_by** | Option<**String**> | Order the items are returned in the response. |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**tx_hash** | Option<**String**> | Filter to a particular transaction. Example: `0x04654cc4c81882ed4d20b958e0eeb107915d75730110cce65333221439de6afc` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive). Relative to the orderBy time filters. |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive). Relative to the orderBy time filters. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 100]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetSalesV4Response**](getSalesV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_sales_v5

> models::GetSalesV4Response get_sales_v5(contract, tokens, include_token_metadata, include_deleted, collection, attributes, order_by, sort_direction, tx_hash, start_timestamp, end_timestamp, limit, continuation)
Sales

Get recent sales for a contract or token. Paid mints are returned in this `sales` endpoint, free mints can be found in the `/activities/` endpoints. Array of contracts max limit is 20.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 20. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**include_token_metadata** | Option<**bool**> | If enabled, also include token metadata in the response. Default is false. |  |
**include_deleted** | Option<**bool**> | If enabled, include sales that have been deleted. In some cases the backfilling process deletes sales that are no longer relevant or have been reverted. |  |[default to false]
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Attributes are case sensitive. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/sales/v4?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/sales/v4?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**order_by** | Option<**String**> | Order the items are returned in the response. Options are `price`, `time`, and `updated_at`. Default is `time`. |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**tx_hash** | Option<**String**> | Filter to a particular transaction. Example: `0x04654cc4c81882ed4d20b958e0eeb107915d75730110cce65333221439de6afc` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive). Relative to the orderBy time filters. |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive). Relative to the orderBy time filters. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 100]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetSalesV4Response**](getSalesV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_search_collections_v1

> models::GetSearchCollectionsV1Response get_search_collections_v1(name, community, display_currency, collections_set_id, offset, limit)
Search collections

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**name** | Option<**String**> | Lightweight search for collections that match a string. Example: `bored` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**display_currency** | Option<**String**> | Return result in given currency |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set |  |
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetSearchCollectionsV1Response**](getSearchCollectionsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_search_collections_v3

> models::GetSearchCollectionsV3Response get_search_collections_v3(prefix, display_currency, exclude_spam, boost_verified, fuzzy, limit)
Search Collections

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**prefix** | Option<**String**> | Lightweight search for collections that match a string. Can also search using contract address. Example: `bored` or `0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d` |  |
**display_currency** | Option<**String**> | Return result in given currency. |  |
**exclude_spam** | Option<**bool**> | If true, will filter any collections marked as spam. |  |[default to false]
**boost_verified** | Option<**bool**> | If true, will promote verified collections. |  |[default to true]
**fuzzy** | Option<**bool**> | If true, fuzzy search to help with misspellings. |  |[default to false]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetSearchCollectionsV3Response**](getSearchCollectionsV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_stats_v1

> models::GetStatsV1Response get_stats_v1(collection, token, attributes)
Stats

Get aggregate stats for a particular set (collection, attribute or single token)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Example: `attributes[Type]=Original` |  |

### Return type

[**models::GetStatsV1Response**](getStatsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_sync_asks_v1

> models::SyncOrdersAsksV1Response get_sync_asks_v1(continuation)
Sync Asks (listings)

This API is optimized for bulk access to asks (listings) for syncing a remote database. Thus it offers minimal filters/metadata.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::SyncOrdersAsksV1Response**](syncOrdersAsksV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_details_v2

> models::GetTokensDetailsV2Response get_tokens_details_v2(collection, contract, token, token_set_id, attributes, source, sort_by, limit, continuation)
Get one or more tokens with full details

Get a list of tokens with full metadata. This is useful for showing a single token page, or scenarios that require more metadata. If you don't need this metadata, you should use the <a href='#/tokens/getTokensV1'>tokens</a> API, which is much faster.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute, e.g. `attributes[Type]=Original` |  |
**source** | Option<**String**> | Filter to a particular source, e.g. `0x5b3256965e7c3cf26e11fcaf296dfc8807c01073` |  |
**sort_by** | Option<**String**> |  |  |[default to floorAskPrice]
**limit** | Option<**i32**> |  |  |[default to 20]
**continuation** | Option<**String**> |  |  |

### Return type

[**models::GetTokensDetailsV2Response**](getTokensDetailsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_details_v3

> models::GetTokensDetailsV3Response get_tokens_details_v3(collection, contract, tokens, token_set_id, attributes, source, sort_by, limit, continuation)
Get one or more tokens with full details

Get a list of tokens with full metadata. This is useful for showing a single token page, or scenarios that require more metadata. If you don't need this metadata, you should use the <a href='#/tokens/getTokensV1'>tokens</a> API, which is much faster.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Filter to one or more tokens, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute, e.g. `attributes[Type]=Original` |  |
**source** | Option<**String**> | Filter to a particular source, e.g. `0x5b3256965e7c3cf26e11fcaf296dfc8807c01073` |  |
**sort_by** | Option<**String**> |  |  |[default to floorAskPrice]
**limit** | Option<**i32**> |  |  |[default to 20]
**continuation** | Option<**String**> |  |  |

### Return type

[**models::GetTokensDetailsV3Response**](getTokensDetailsV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_details_v4

> models::GetTokensDetailsV4Response get_tokens_details_v4(collection, contract, tokens, token_set_id, attributes, source, sort_by, sort_direction, limit, include_top_bid, continuation)
Tokens (detailed response)

Get a list of tokens with full metadata. This is useful for showing a single token page, or scenarios that require more metadata. If you don't need this metadata, you should use the <a href='#/tokens/getTokensV1'>tokens</a> API, which is much faster.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**token_set_id** | Option<**String**> | Filter to a particular token set. `Example: token:0xa7d8d9ef8d8ce8992df33d8b8cf4aebabd5bd270:129000685` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Example: `attributes[Type]=Original` |  |
**source** | Option<**String**> | Domain of the order source. Example `opensea.io` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to floorAskPrice]
**sort_direction** | Option<**String**> |  |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetTokensDetailsV4Response**](getTokensDetailsV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_token_activity_v1

> models::GetUserActivityV2Response get_tokens_token_activity_v1(token, limit, continuation, types)
Token activity

This API can be used to build a feed for a token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | **String** | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**continuation** | Option<**f64**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetUserActivityV2Response**](getUserActivityV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_token_activity_v2

> models::GetCollectionActivityV2Response get_tokens_token_activity_v2(token, limit, sort_by, continuation, types)
Token activity

This API can be used to build a feed for a token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | **String** | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetCollectionActivityV2Response**](getCollectionActivityV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_token_activity_v3

> models::GetCollectionActivityV4Response get_tokens_token_activity_v3(token, limit, sort_by, include_metadata, continuation, types)
Token activity

This API can be used to build a feed for a token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | **String** | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetCollectionActivityV4Response**](getCollectionActivityV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_token_activity_v4

> models::GetTokenActivityV4Response get_tokens_token_activity_v4(token, limit, sort_by, include_metadata, continuation, types)
Token activity

This API can be used to build a feed for a token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | **String** | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetTokenActivityV4Response**](getTokenActivityV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_v1

> models::GetTokensV1Response get_tokens_v1(collection, contract, token, token_set_id, on_sale, sort_by, sort_direction, offset, limit)
List of tokens

This API is optimized for quickly fetching a list of tokens in a collection, sorted by price, with only the most important information returned. If you need more metadata, use the `tokens/details` API

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**on_sale** | Option<**bool**> | Limit to tokens that are listed for sale |  |
**sort_by** | Option<**String**> |  |  |[default to floorAskPrice]
**sort_direction** | Option<**String**> |  |  |
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetTokensV1Response**](getTokensV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_v2

> models::GetTokensV2Response get_tokens_v2(collection, contract, token, token_set_id, attributes, sort_by, limit, continuation)
List of tokens, with basic details, optimized for speed

This API is optimized for quickly fetching a list of tokens in a collection, sorted by price, with only the most important information returned. If you need more metadata, use the `tokens/details` API

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute, e.g. `attributes[Type]=Original` |  |
**sort_by** | Option<**String**> |  |  |[default to floorAskPrice]
**limit** | Option<**i32**> |  |  |[default to 20]
**continuation** | Option<**String**> |  |  |

### Return type

[**models::GetTokensV2Response**](getTokensV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_v3

> models::GetTokensV3Response get_tokens_v3(collection, contract, tokens, token_set_id, attributes, sort_by, limit, continuation)
List of tokens, with basic details, optimized for speed

This API is optimized for quickly fetching a list of tokens in a collection, sorted by price, with only the most important information returned. If you need more metadata, use the `tokens/details` API

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Filter to one or more tokens, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**token_set_id** | Option<**String**> | Filter to a particular set, e.g. `contract:0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute, e.g. `attributes[Type]=Original` |  |
**sort_by** | Option<**String**> |  |  |[default to floorAskPrice]
**limit** | Option<**i32**> |  |  |[default to 20]
**continuation** | Option<**String**> |  |  |

### Return type

[**models::GetTokensV3Response**](getTokensV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_v4

> models::GetTokensV4Response get_tokens_v4(collection, contract, tokens, token_set_id, attributes, source, native, sort_by, sort_direction, limit, include_top_bid, continuation)
Tokens

This API is optimized for quickly fetching a list of tokens in a collection, sorted by price, with only the most important information returned. If you need more metadata, use the tokens/details API

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**token_set_id** | Option<**String**> | Filter to a particular token set. Example: token:0xa7d8d9ef8d8ce8992df33d8b8cf4aebabd5bd270:129000685 |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Example: `attributes[Type]=Original` |  |
**source** | Option<**String**> | Domain of the order source. Example `opensea.io` |  |
**native** | Option<**bool**> | If true, results will filter only Reservoir orders. |  |
**sort_by** | Option<**String**> | Order the items are returned in the response, by default sorted by `floorAskPrice`. Not supported when filtering by `contract`. When filtering by `contract` the results are sorted by `tokenId` by default. |  |[default to floorAskPrice]
**sort_direction** | Option<**String**> |  |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetTokensV4Response**](getTokensV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_v5

> models::GetTokensV5Response get_tokens_v5(collection, collections_set_id, community, contract, token_name, tokens, token_set_id, attributes, source, native_source, min_rarity_rank, max_rarity_rank, min_floor_ask_price, max_floor_ask_price, flag_status, sort_by, sort_direction, currencies, limit, include_top_bid, include_attributes, include_quantity, include_dynamic_pricing, include_royalties_paid, normalize_royalties, continuation, display_currency)
Tokens

Get a list of tokens with full metadata. This is useful for showing a single token page, or scenarios that require more metadata.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token_name** | Option<**String**> | Filter to a particular token by name. Example: `token #1` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**token_set_id** | Option<**String**> | Filter to a particular token set. Example: `token:CONTRACT:TOKEN_ID` representing a single token within contract, `contract:CONTRACT` representing a whole contract, `range:CONTRACT:START_TOKEN_ID:END_TOKEN_ID` representing a continuous token id range within a contract and `list:CONTRACT:TOKEN_IDS_HASH` representing a list of token ids within a contract. |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/tokens/v5?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/tokens/v5?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**source** | Option<**String**> | Domain of the order source. Example `opensea.io` (Only listed tokens are returned when filtering by source) |  |
**native_source** | Option<**String**> | Domain of the order source. Example `www.apecoinmarketplace.com`. For a native marketplace, return all tokens listed on this marketplace, even if better prices are available on other marketplaces. |  |
**min_rarity_rank** | Option<**i32**> | Get tokens with a min rarity rank (inclusive) |  |
**max_rarity_rank** | Option<**i32**> | Get tokens with a max rarity rank (inclusive) |  |
**min_floor_ask_price** | Option<**f64**> | Get tokens with a min floor ask price (inclusive) |  |
**max_floor_ask_price** | Option<**f64**> | Get tokens with a max floor ask price (inclusive) |  |
**flag_status** | Option<**f64**> | Allowed only with collection and tokens filtering! -1 = All tokens (default) 0 = Non flagged tokens 1 = Flagged tokens |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to floorAskPrice]
**sort_direction** | Option<**String**> |  |  |
**currencies** | Option<[**Vec<String>**](String.md)> | Filter to tokens with a listing in a particular currency. `Example: currencies[0]: 0x0000000000000000000000000000000000000000` |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be returned in the response. |  |[default to false]
**include_quantity** | Option<**bool**> | If true, quantity filled and quantity remaining will be returned in the response. |  |[default to false]
**include_dynamic_pricing** | Option<**bool**> | If true, dynamic pricing data will be returned in the response. |  |[default to false]
**include_royalties_paid** | Option<**bool**> | If true, a boolean indicating whether royalties were paid on a token's last sale will be returned in the response. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetTokensV5Response**](getTokensV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_v6

> models::GetTokensV6Response get_tokens_v6(collection, token_name, tokens, attributes, source, native_source, min_rarity_rank, max_rarity_rank, min_floor_ask_price, max_floor_ask_price, flag_status, collections_set_id, community, contract, token_set_id, sort_by, sort_direction, currencies, limit, start_timestamp, end_timestamp, include_top_bid, include_mint_stages, exclude_eoa, exclude_spam, exclude_nsfw, include_attributes, include_quantity, include_dynamic_pricing, include_last_sale, normalize_royalties, continuation, display_currency)
Tokens

Get a list of tokens with full metadata. This is useful for showing a single token page, or scenarios that require more metadata.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token_name** | Option<**String**> | Filter to a particular token by name. This is case sensitive. Example: `token #1` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Attributes are case sensitive. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/tokens/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/tokens/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**source** | Option<**String**> | Domain of the order source. Example `opensea.io` (Only listed tokens are returned when filtering by source) |  |
**native_source** | Option<**String**> | Domain of the order source. Example `www.apecoinmarketplace.com`. For a native marketplace, return all tokens listed on this marketplace, even if better prices are available on other marketplaces. |  |
**min_rarity_rank** | Option<**i32**> | Get tokens with a min rarity rank (inclusive), no rarity rank for collections over 100k |  |
**max_rarity_rank** | Option<**i32**> | Get tokens with a max rarity rank (inclusive), no rarity rank for collections over 100k |  |
**min_floor_ask_price** | Option<**f64**> | Get tokens with a min floor ask price (inclusive); use native currency |  |
**max_floor_ask_price** | Option<**f64**> | Get tokens with a max floor ask price (inclusive); use native currency |  |
**flag_status** | Option<**f64**> | Allowed only with collection and tokens filtering! -1 = All tokens (default) 0 = Non flagged tokens 1 = Flagged tokens |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**token_set_id** | Option<**String**> | Filter to a particular token set. `Example: token:0xa7d8d9ef8d8ce8992df33d8b8cf4aebabd5bd270:129000685` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `floorAskPrice`, `tokenId`, `rarity`, and `updatedAt`. No rarity rank for collections over 100k. |  |[default to floorAskPrice]
**sort_direction** | Option<**String**> |  |  |
**currencies** | Option<[**Vec<String>**](String.md)> | Filter to tokens with a listing in a particular currency. Max limit is 50. `Example: currencies[0]: 0x0000000000000000000000000000000000000000` |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 100, except when sorting by `updatedAt` which has a limit of 1000. |  |[default to 20]
**start_timestamp** | Option<**f64**> | When sorting by `updatedAt`, the start timestamp you want to filter on (UTC). |  |
**end_timestamp** | Option<**f64**> | When sorting by `updatedAt`, the end timestamp you want to filter on (UTC). |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_mint_stages** | Option<**bool**> | If true, mint data for the tokens will be included in the response. |  |[default to false]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. defaults to false |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any tokens marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any tokens marked as nsfw. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be returned in the response. |  |[default to false]
**include_quantity** | Option<**bool**> | If true, quantity filled and quantity remaining will be returned in the response. |  |[default to false]
**include_dynamic_pricing** | Option<**bool**> | If true, dynamic pricing data will be returned in the response. |  |[default to false]
**include_last_sale** | Option<**bool**> | If true, last sale data including royalties paid will be returned in the response. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |

### Return type

[**models::GetTokensV6Response**](getTokensV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_v8

> models::GetTokensV8Response get_tokens_v8(collection, token_name, tokens, attributes, source, native_source, min_rarity_rank, max_rarity_rank, min_floor_ask_price, max_floor_ask_price, flag_status, collections_set_id, community, contract, token_set_id, sort_by, sort_direction, currencies, limit, start_timestamp, end_timestamp, include_top_bid, include_mint_stages, exclude_eoa, exclude_spam, exclude_nsfw, include_attributes, include_quantity, include_dynamic_pricing, include_last_sale, normalize_royalties, continuation, display_currency)
Tokens

Get a list of tokens with full metadata. This is useful for showing a single token page, or scenarios that require more metadata.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token_name** | Option<**String**> | Filter to a particular token by name. This is case insensitive. Example: `token #1` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Attributes are case sensitive. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/tokens/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/tokens/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**source** | Option<**String**> | Domain of the order source. Example `opensea.io` (Only listed tokens are returned when filtering by source) |  |
**native_source** | Option<**String**> | Domain of the order source. Example `www.apecoinmarketplace.com`. For a native marketplace, return all tokens listed on this marketplace, even if better prices are available on other marketplaces. |  |
**min_rarity_rank** | Option<**i32**> | Get tokens with a min rarity rank (inclusive), no rarity rank for collections over 100k |  |
**max_rarity_rank** | Option<**i32**> | Get tokens with a max rarity rank (inclusive), no rarity rank for collections over 100k |  |
**min_floor_ask_price** | Option<**f64**> | Get tokens with a min floor ask price (inclusive); use native currency |  |
**max_floor_ask_price** | Option<**f64**> | Get tokens with a max floor ask price (inclusive); use native currency |  |
**flag_status** | Option<**f64**> | Allowed only with collection and tokens filtering! -1 = All tokens (default) 0 = Non flagged tokens 1 = Flagged tokens |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**token_set_id** | Option<**String**> | Filter to a particular token set. `Example: token:0xa7d8d9ef8d8ce8992df33d8b8cf4aebabd5bd270:129000685` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `floorAskPrice`, `tokenId`, `rarity`, and `updatedAt`. No rarity rank for collections over 100k. |  |[default to floorAskPrice]
**sort_direction** | Option<**String**> |  |  |
**currencies** | Option<[**Vec<String>**](String.md)> | Filter to tokens with a listing in a particular currency. Max limit is 50. `Example: currencies[0]: 0x0000000000000000000000000000000000000000` |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 100, except when sorting by `updatedAt` which has a limit of 1000. |  |[default to 20]
**start_timestamp** | Option<**f64**> | When sorting by `updatedAt`, the start timestamp you want to filter on (UTC). |  |
**end_timestamp** | Option<**f64**> | When sorting by `updatedAt`, the end timestamp you want to filter on (UTC). |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_mint_stages** | Option<**bool**> | If true, mint data for the tokens will be included in the response. |  |[default to false]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. defaults to false |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any tokens marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any tokens marked as nsfw. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be returned in the response. |  |[default to false]
**include_quantity** | Option<**bool**> | If true, quantity filled and quantity remaining will be returned in the response. |  |[default to false]
**include_dynamic_pricing** | Option<**bool**> | If true, dynamic pricing data will be returned in the response. |  |[default to false]
**include_last_sale** | Option<**bool**> | If true, last sale data including royalties paid will be returned in the response. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |

### Return type

[**models::GetTokensV8Response**](getTokensV8Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_transfers_bulk_v1

> models::GetTransfersBulkV1Response get_transfers_bulk_v1(contract, token, start_timestamp, end_timestamp, tx_hash, limit, order_by, continuation)
Bulk historical transfers

Note: this API is optimized for bulk access, and offers minimal filters/metadata. If you need more flexibility, try the `NFT API > Transfers` endpoint

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**tx_hash** | Option<**String**> | Filter to a particular transaction. Example: `0x04654cc4c81882ed4d20b958e0eeb107915d75730110cce65333221439de6afc` |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 100]
**order_by** | Option<**String**> | Order the items are returned in the response. Options are `timestamp`, and `updated_at`. Default is `timestamp`. |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetTransfersBulkV1Response**](getTransfersBulkV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_transfers_v2

> models::GetTransfersV2Response get_transfers_v2(contract, token, collection, attributes, tx_hash, limit, continuation)
Historical token transfers

Get recent transfers for a contract or token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/transfers/v2?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/transfers/v2?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**tx_hash** | Option<**String**> | Filter to a particular transaction. Example: `0x04654cc4c81882ed4d20b958e0eeb107915d75730110cce65333221439de6afc` |  |
**limit** | Option<**i32**> |  |  |[default to 20]
**continuation** | Option<**String**> |  |  |

### Return type

[**models::GetTransfersV2Response**](getTransfersV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_transfers_v3

> models::GetTransfersV3Response get_transfers_v3(contract, token, collection, attributes, tx_hash, order_by, limit, continuation, display_currency)
Historical token transfers

Get recent transfers for a contract or token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/transfers/v2?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/transfers/v2?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**tx_hash** | Option<**String**> | Filter to a particular transaction. Example: `0x04654cc4c81882ed4d20b958e0eeb107915d75730110cce65333221439de6afc` |  |
**order_by** | Option<**String**> | Order the items are returned in the response. Options are `timestamp`, and `updated_at`. Default is `timestamp`. |  |
**limit** | Option<**i32**> | Max limit is 100. |  |[default to 20]
**continuation** | Option<**String**> |  |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetTransfersV3Response**](getTransfersV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_activity_v2

> models::GetUserActivityV2Response get_users_activity_v2(users, limit, continuation, types)
Users activity

This API can be used to build a feed for a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**users** | [**Vec<String>**](String.md) | Array of users addresses. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**continuation** | Option<**f64**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetUserActivityV2Response**](getUserActivityV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_activity_v3

> models::GetUserActivityV3Response get_users_activity_v3(users, limit, sort_by, continuation, types)
Users activity

This API can be used to build a feed for a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**users** | [**Vec<String>**](String.md) | Array of users addresses. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetUserActivityV3Response**](getUserActivityV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_activity_v4

> models::GetUserActivityV4Response get_users_activity_v4(users, collection, collections_set_id, community, limit, sort_by, include_metadata, continuation, types)
Users activity

This API can be used to build a feed for a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**users** | [**Vec<String>**](String.md) | Array of users addresses. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**collection** | Option<**String**> |  |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**limit** | Option<**i32**> | Amount of items returned in response. If `includeMetadata=true` max limit is 20, otherwise max limit is 1,000. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetUserActivityV4Response**](getUserActivityV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_activity_v5

> models::GetUserActivityV5Response get_users_activity_v5(users, collection, collections_set_id, contracts_set_id, community, limit, sort_by, include_metadata, continuation, types)
Users activity

This API can be used to build a feed for a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**users** | [**Vec<String>**](String.md) | Array of users addresses. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**collection** | Option<[**Vec<String>**](String.md)> |  |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**limit** | Option<**i32**> | Amount of items returned in response. If `includeMetadata=true` max limit is 20, otherwise max limit is 1,000. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response, eventTimestamp = The blockchain event time, createdAt - The time in which event was recorded |  |[default to eventTimestamp]
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetUserActivityV5Response**](getUserActivityV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_activity_v1

> models::GetUserActivityV1Response get_users_user_activity_v1(user, limit, continuation, types)
User activity

This API can be used to build a feed for a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**continuation** | Option<**f64**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**models::GetUserActivityV1Response**](getUserActivityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_collections_v1

> models::GetUserCollectionsV1Response get_users_user_collections_v1(user, community, collection, offset, limit)
Get aggregate stats for a user, grouped by collection

Get aggregate stats for a user, grouped by collection. Useful for showing total portfolio information.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Wallet to see results for e.g. `0xf296178d553c8ec21a2fbd2c5dda8ca9ac905a00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetUserCollectionsV1Response**](getUserCollectionsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_collections_v2

> models::GetUserCollectionsV2Response get_users_user_collections_v2(user, community, collections_set_id, collection, include_top_bid, include_liquid_count, offset, limit, sort_by, sort_direction)
User collections

Get aggregate stats for a user, grouped by collection. Useful for showing total portfolio information.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_liquid_count** | Option<**bool**> | If true, number of tokens with bids will be returned in the response. |  |[default to false]
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response. Defaults to allTimeVolume |  |[default to allTimeVolume]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]

### Return type

[**models::GetUserCollectionsV2Response**](getUserCollectionsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_collections_v3

> models::GetUserCollectionsV3Response get_users_user_collections_v3(user, community, collections_set_id, collection, include_top_bid, include_liquid_count, exclude_spam, offset, limit, display_currency)
User collections

Get aggregate stats for a user, grouped by collection. Useful for showing total portfolio information.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_liquid_count** | Option<**bool**> | If true, number of tokens with bids will be returned in the response. |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any collections marked as spam. |  |[default to false]
**offset** | Option<**i32**> | Use offset to request the next batch of items. Max is 10,000. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. max limit is 100. |  |[default to 20]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |

### Return type

[**models::GetUserCollectionsV3Response**](getUserCollectionsV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_positions_v1

> models::GetUserPositionsV1Response get_users_user_positions_v1(user, side, status, offset, limit)
Get a summary of a users bids and asks

Get aggregate user liquidity, grouped by collection. Useful for showing a summary of liquidity being provided (orders made).

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Wallet to see results for e.g. `0xf296178d553c8ec21a2fbd2c5dda8ca9ac905a00` | [required] |
**side** | **String** |  | [required] |
**status** | **String** |  | [required] |
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetUserPositionsV1Response**](getUserPositionsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v1

> models::GetUserTokensV1Response get_users_user_tokens_v1(user, community, collection, contract, has_offer, sort_by, sort_direction, offset, limit)
User tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** |  | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**has_offer** | Option<**bool**> |  |  |
**sort_by** | Option<**String**> |  |  |
**sort_direction** | Option<**String**> |  |  |
**offset** | Option<**i32**> |  |  |[default to 0]
**limit** | Option<**i32**> |  |  |[default to 20]

### Return type

[**models::GetUserTokensV1Response**](getUserTokensV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v2

> models::GetUserTokensV2Response get_users_user_tokens_v2(user, community, collections_set_id, collection, contract, sort_by, sort_direction, offset, limit)
User tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetUserTokensV2Response**](getUserTokensV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v3

> models::GetUserTokensV3Response get_users_user_tokens_v3(user, community, collections_set_id, collection, contract, sort_by, sort_direction, offset, limit, include_top_bid)
User Tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]

### Return type

[**models::GetUserTokensV3Response**](getUserTokensV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v4

> models::GetUserTokensV4Response get_users_user_tokens_v4(user, community, collections_set_id, collection, contract, sort_by, sort_direction, offset, limit, include_top_bid, display_currency)
User Tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetUserTokensV4Response**](getUserTokensV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v5

> models::GetUserTokensV5Response get_users_user_tokens_v5(user, community, collections_set_id, collection, contract, tokens, normalize_royalties, sort_by, sort_direction, offset, limit, include_top_bid, display_currency)
User Tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetUserTokensV5Response**](getUserTokensV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v6

> models::GetUserTokensV6Response get_users_user_tokens_v6(user, community, collections_set_id, collection, contract, tokens, normalize_royalties, sort_by, sort_direction, continuation, limit, include_top_bid, include_dynamic_pricing, use_non_flagged_floor_ask, display_currency)
User Tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. |  |[default to acquiredAt]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_dynamic_pricing** | Option<**bool**> | If true, dynamic pricing data will be returned in the response. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will return the collection non flagged floor ask. |  |[default to false]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetUserTokensV6Response**](getUserTokensV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v7

> models::GetUserTokensV7Response get_users_user_tokens_v7(user, community, collections_set_id, collection, contract, tokens, normalize_royalties, sort_by, sort_direction, continuation, limit, include_top_bid, include_attributes, include_last_sale, include_raw_data, exclude_spam, use_non_flagged_floor_ask, display_currency)
User Tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `acquiredAt` and `lastAppraisalValue`. `lastAppraisalValue` is the value of the last sale. |  |[default to acquiredAt]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 200. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be returned in the response. |  |[default to false]
**include_last_sale** | Option<**bool**> | If true, last sale data including royalties paid will be returned in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any tokens marked as spam. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will return the collection non flagged floor ask. |  |[default to false]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |

### Return type

[**models::GetUserTokensV7Response**](getUserTokensV7Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v8

> models::GetUserTokensV8Response get_users_user_tokens_v8(user, community, collections_set_id, collection, contract, tokens, normalize_royalties, sort_by, sort_direction, continuation, limit, include_top_bid, include_attributes, include_last_sale, include_raw_data, exclude_spam, exclude_nsfw, use_non_flagged_floor_ask, display_currency)
User Tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `acquiredAt` and `lastAppraisalValue`. `lastAppraisalValue` is the value of the last sale. |  |[default to acquiredAt]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 200. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be returned in the response. |  |[default to false]
**include_last_sale** | Option<**bool**> | If true, last sale data including royalties paid will be returned in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any tokens marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any tokens marked as nsfw. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will return the collection non flagged floor ask. |  |[default to false]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |

### Return type

[**models::GetUserTokensV8Response**](getUserTokensV8Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_tokens_v9

> models::GetUserTokensV9Response get_users_user_tokens_v9(user, community, collections_set_id, collection, exclude_collections, contract, tokens, normalize_royalties, sort_by, sort_direction, continuation, limit, include_top_bid, include_attributes, include_last_sale, include_raw_data, exclude_spam, exclude_nsfw, only_listed, use_non_flagged_floor_ask, display_currency, token_name)
User Tokens

Get tokens held by a user, along with ownership information such as associated orders and date acquired.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community, e.g. `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**collection** | Option<[**Vec<String>**](String.md)> | Array of collections. Max limit is 100. Example: `collections[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**exclude_collections** | Option<[**Vec<String>**](String.md)> |  |  |
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `acquiredAt`, `lastAppraisalValue` and `floorAskPrice`. `lastAppraisalValue` is the value of the last sale. `floorAskPrice` is the collection floor ask |  |[default to acquiredAt]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 200. |  |[default to 20]
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_attributes** | Option<**bool**> | If true, attributes will be returned in the response. |  |[default to false]
**include_last_sale** | Option<**bool**> | If true, last sale data including royalties paid will be returned in the response. |  |[default to false]
**include_raw_data** | Option<**bool**> | If true, raw data is included in the response. |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any tokens marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any tokens marked as nsfw. |  |[default to false]
**only_listed** | Option<**bool**> | If true, will filter any tokens that are not listed |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will return the collection non flagged floor ask. |  |[default to false]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |
**token_name** | Option<**String**> | Filter to a particular token by name. This is case sensitive. Example: `token #1` |  |

### Return type

[**models::GetUserTokensV9Response**](getUserTokensV9Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_collections_refresh_v1

> models::PutSetCollectionCommunityV1Response post_collections_refresh_v1(x_api_key, body)
Refresh Collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | Option<**String**> |  |  |
**body** | Option<[**Model492**](Model492.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_bid_v4

> models::GetExecuteBidV4Response post_execute_bid_v4(body)
Create bid (offer)

Generate a bid and submit it to multiple marketplaces

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model497**](Model497.md)> |  |  |

### Return type

[**models::GetExecuteBidV4Response**](getExecuteBidV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_buy_v5

> models::GetExecuteBuyV5Response post_execute_buy_v5(body)
Buy tokens

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model511**](Model511.md)> |  |  |

### Return type

[**models::GetExecuteBuyV5Response**](getExecuteBuyV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_buy_v6

> models::GetExecuteBuyV6Response post_execute_buy_v6(body)
Buy tokens

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model520**](Model520.md)> |  |  |

### Return type

[**models::GetExecuteBuyV6Response**](getExecuteBuyV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_list_v4

> models::GetExecuteListV4Response post_execute_list_v4(body)
Create ask (listing)

Generate a listing and submit it to multiple marketplaces

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model560**](Model560.md)> |  |  |

### Return type

[**models::GetExecuteListV4Response**](getExecuteListV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_sell_v6

> models::GetExecuteBuyV6Response post_execute_sell_v6(body)
Sell tokens (accept bids)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model583**](Model583.md)> |  |  |

### Return type

[**models::GetExecuteBuyV6Response**](getExecuteBuyV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_order_v2

> models::PostOrderV2Response post_order_v2(signature, body)
Submit single order

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | Option<**String**> |  |  |
**body** | Option<[**Model470**](Model470.md)> |  |  |

### Return type

[**models::PostOrderV2Response**](postOrderV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_order_v3

> models::PostOrderV2Response post_order_v3(signature, body)
Submit signed order

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | Option<**String**> |  |  |
**body** | Option<[**Model472**](Model472.md)> |  |  |

### Return type

[**models::PostOrderV2Response**](postOrderV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_orders_v1

> String post_orders_v1(x_admin_api_key, body)
Submit order batch

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model481**](Model481.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_seaport_offers

> String post_seaport_offers(body)
Submit multiple Seaport offers (compatible with OpenSea's API response)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model483**](Model483.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokens_refresh_v1

> models::PutSetCollectionCommunityV1Response post_tokens_refresh_v1(body)
Refresh Token

Token metadata is never automatically refreshed, but may be manually refreshed with this API.  Caution: This API should be used in moderation, like only when missing data is discovered. Calling it in bulk or programmatically will result in your API key getting rate limited.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model612**](Model612.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokens_simulatefloor_v1

> models::PutSetCollectionCommunityV1Response post_tokens_simulatefloor_v1(body)
Simulate the floor ask of any token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model617**](Model617.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokens_simulatetopbid_v1

> models::PutSetCollectionCommunityV1Response post_tokens_simulatetopbid_v1(body)
Simulate the top bid of any token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model618**](Model618.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_tokensets_v1

> models::Model285 post_tokensets_v1(body)
Create Token Set

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model484**](Model484.md)> |  |  |

### Return type

[**models::Model285**](Model285.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

