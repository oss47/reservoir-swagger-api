# GetSalesV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales** | Option<[**Vec<models::Model52>**](Model52.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


