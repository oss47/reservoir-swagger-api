/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model261Exchanges {
    #[serde(rename = "string", skip_serializing_if = "Option::is_none")]
    pub string: Option<Box<models::ReservoirString>>,
}

impl Model261Exchanges {
    pub fn new() -> Model261Exchanges {
        Model261Exchanges { string: None }
    }
}
