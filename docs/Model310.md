# Model310

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  | 
**is_flagged** | **bool** |  | 
**last_transfer_time** | **f64** |  | 
**message** | Option<[**models::Model309**](Model309.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


