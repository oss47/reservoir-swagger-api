# Model79

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**floor_ask** | Option<[**models::Model77**](Model77.md)> |  | [optional]
**top_bid** | Option<[**models::Model78**](Model78.md)> |  | [optional]
**royalties_paid** | Option<**bool**> |  | [optional][default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


