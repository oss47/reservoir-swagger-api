# Model583

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | Option<**String**> |  | [optional]
**raw_order** | Option<[**models::Model581**](Model581.md)> |  | [optional]
**token** | **String** | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | 
**taker** | **String** | Address of wallet filling the order. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | 
**quantity** | Option<**i32**> | Quantity of tokens user is selling. Only compatible when selling a single ERC1155 token. Example: `5` | [optional]
**source** | Option<**String**> | Filling source used for attribution. Example: `reservoir.market` | [optional]
**fees_on_top** | Option<**Vec<String>**> | List of fees (formatted as `feeRecipient:feeAmount`) to be taken when filling. The currency used for any fees on top matches the accepted bid's currency. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:1000000000000000` | [optional]
**only_path** | Option<**bool**> | If true, only the path will be returned. | [optional][default to false]
**normalize_royalties** | Option<**bool**> |  | [optional][default to false]
**allow_inactive_order_ids** | Option<**bool**> | If true, do not filter out inactive orders (only relevant for order id filtering). | [optional][default to false]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. | [optional][default to false]
**max_fee_per_gas** | Option<**String**> | Optional. Set custom gas price. | [optional]
**max_priority_fee_per_gas** | Option<**String**> | Optional. Set custom gas price. | [optional]
**x2y2_api_key** | Option<**String**> | Override the X2Y2 API key used for filling. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


