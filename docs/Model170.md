# Model170

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**kind** | **String** | This is the `orderKind`. | 
**side** | **String** | Either `buy` or `sell` | 
**status** | Option<**String**> | Can be `active`, `inactive`, `expired`, `canceled`, or `filled` | [optional]
**token_set_id** | **String** |  | 
**token_set_schema_hash** | **String** |  | 
**contract** | Option<**String**> |  | [optional]
**contract_kind** | Option<**String**> |  | [optional]
**maker** | **String** |  | 
**taker** | **String** |  | 
**price** | Option<[**crate::models::Model110**](Model110.md)> |  | [optional]
**valid_from** | **f32** |  | 
**valid_until** | **f32** |  | 
**quantity_filled** | Option<**f32**> | With ERC1155s, quantity can be higher than 1 | [optional]
**quantity_remaining** | Option<**f32**> | With ERC1155s, quantity can be higher than 1 | [optional]
**dynamic_pricing** | Option<[**crate::models::Model166**](Model166.md)> |  | [optional]
**criteria** | Option<[**crate::models::Model106**](Model106.md)> |  | [optional]
**source** | Option<[**serde_json::Value**](.md)> |  | [optional]
**fee_bps** | Option<**f32**> |  | [optional]
**fee_breakdown** | Option<[**Vec<crate::models::Model167>**](Model167.md)> |  | [optional]
**expiration** | **f32** |  | 
**is_reservoir** | Option<**bool**> |  | [optional]
**is_dynamic** | Option<**bool**> |  | [optional]
**created_at** | **String** | Time when added to indexer | 
**updated_at** | **String** | Time when updated in indexer | 
**originated_at** | Option<**String**> | Time when created by maker | [optional]
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]
**is_native_off_chain_cancellable** | Option<**bool**> |  | [optional]
**depth** | Option<[**Vec<crate::models::Model169>**](Model169.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


