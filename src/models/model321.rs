/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model321 {
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    /// Open Sea slug
    #[serde(rename = "slug", skip_serializing_if = "Option::is_none")]
    pub slug: Option<String>,
    #[serde(rename = "symbol", skip_serializing_if = "Option::is_none")]
    pub symbol: Option<String>,
    /// Time when contract was deployed
    #[serde(rename = "contractDeployedAt", skip_serializing_if = "Option::is_none")]
    pub contract_deployed_at: Option<String>,
    #[serde(rename = "imageUrl", skip_serializing_if = "Option::is_none")]
    pub image_url: Option<String>,
    #[serde(rename = "isSpam", skip_serializing_if = "Option::is_none")]
    pub is_spam: Option<bool>,
    #[serde(rename = "isNsfw", skip_serializing_if = "Option::is_none")]
    pub is_nsfw: Option<bool>,
    #[serde(rename = "metadataDisabled", skip_serializing_if = "Option::is_none")]
    pub metadata_disabled: Option<bool>,
    #[serde(
        rename = "openseaVerificationStatus",
        skip_serializing_if = "Option::is_none"
    )]
    pub opensea_verification_status: Option<String>,
    /// Total tokens within the collection.
    #[serde(rename = "tokenCount", skip_serializing_if = "Option::is_none")]
    pub token_count: Option<String>,
    #[serde(rename = "floorAsk", skip_serializing_if = "Option::is_none")]
    pub floor_ask: Option<Box<models::Model318>>,
    #[serde(rename = "royaltiesBps", skip_serializing_if = "Option::is_none")]
    pub royalties_bps: Option<f64>,
    #[serde(rename = "royalties", skip_serializing_if = "Option::is_none")]
    pub royalties: Option<Vec<models::Model319>>,
}

impl Model321 {
    pub fn new() -> Model321 {
        Model321 {
            id: None,
            name: None,
            slug: None,
            symbol: None,
            contract_deployed_at: None,
            image_url: None,
            is_spam: None,
            is_nsfw: None,
            metadata_disabled: None,
            opensea_verification_status: None,
            token_count: None,
            floor_ask: None,
            royalties_bps: None,
            royalties: None,
        }
    }
}
