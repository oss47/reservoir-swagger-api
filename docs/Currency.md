# Currency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**symbol** | Option<**String**> |  | [optional]
**decimals** | Option<**f64**> |  | [optional]
**chain_id** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


