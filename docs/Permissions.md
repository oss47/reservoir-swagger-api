# Permissions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**override_collection_refresh_cool_down** | Option<**bool**> |  | [optional]
**assign_collection_to_community** | Option<**bool**> |  | [optional]
**update_metadata_disabled** | Option<**bool**> |  | [optional]
**update_spam_status** | Option<**bool**> |  | [optional]
**update_nsfw_status** | Option<**bool**> |  | [optional]
**entity_data_override** | Option<**bool**> |  | [optional]
**invalidate_orders** | Option<**bool**> |  | [optional]
**set_collection_magiceden_verification_status** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


