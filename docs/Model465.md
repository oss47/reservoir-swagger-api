# Model465

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | Option<**String**> | The source domain to sync. Example: `reservoir.market` | [optional]
**icon** | Option<**String**> |  | [optional]
**title** | Option<**String**> |  | [optional]
**allowed_api_keys** | Option<**Vec<String>**> |  | [optional]
**optimized** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


