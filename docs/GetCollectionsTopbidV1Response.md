# GetCollectionsTopbidV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | Option<[**Vec<models::Model295>**](Model295.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


