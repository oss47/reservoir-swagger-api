# Model154

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**kind** | **String** |  | 
**side** | **String** |  | 
**token_set_id** | **String** |  | 
**token_set_schema_hash** | **String** |  | 
**contract** | Option<**String**> |  | [optional]
**maker** | **String** |  | 
**taker** | **String** |  | 
**price** | Option<[**crate::models::Price**](price.md)> |  | [optional]
**valid_from** | **f32** |  | 
**valid_until** | **f32** |  | 
**source** | Option<**String**> |  | [optional]
**fee_bps** | Option<**f32**> |  | [optional]
**fee_breakdown** | Option<[**Vec<crate::models::Model150>**](Model150.md)> |  | [optional]
**status** | Option<**String**> |  | [optional]
**expiration** | **f32** |  | 
**created_at** | **String** |  | 
**updated_at** | **String** |  | 
**metadata** | Option<[**serde_json::Value**](.md)> |  | [optional]
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


