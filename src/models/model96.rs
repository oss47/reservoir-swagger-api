/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model96 {
    #[serde(rename = "route", skip_serializing_if = "Option::is_none")]
    pub route: Option<String>,
    #[serde(rename = "method", skip_serializing_if = "Option::is_none")]
    pub method: Option<String>,
    #[serde(rename = "allowedRequests", skip_serializing_if = "Option::is_none")]
    pub allowed_requests: Option<f32>,
    #[serde(rename = "perSeconds", skip_serializing_if = "Option::is_none")]
    pub per_seconds: Option<f32>,
    #[serde(rename = "payload", skip_serializing_if = "Option::is_none")]
    pub payload: Option<Vec<serde_json::Value>>,
}

impl Model96 {
    pub fn new() -> Model96 {
        Model96 {
            route: None,
            method: None,
            allowed_requests: None,
            per_seconds: None,
            payload: None,
        }
    }
}


