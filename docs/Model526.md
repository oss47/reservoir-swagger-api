# Model526

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | Option<**String**> | Collection to buy. | [optional]
**token** | Option<**String**> | Token to buy. | [optional]
**quantity** | Option<**i32**> | Quantity of tokens to buy. | [optional]
**order_id** | Option<**String**> | Optional order id to fill. | [optional]
**raw_order** | Option<[**models::RawOrder**](rawOrder.md)> |  | [optional]
**fill_type** | Option<**String**> | Optionally specify a particular fill method. Only relevant when filling via `collection`. | [optional][default to PreferMint]
**preferred_mint_stage** | Option<**String**> | Optionally specify a stage to mint | [optional]
**preferred_order_source** | Option<**String**> | If there are multiple listings with equal best price, prefer this source over others. NOTE: if you want to fill a listing that is not the best priced, you need to pass a specific order id or use `exactOrderSource`. | [optional]
**exact_order_source** | Option<**String**> | Only consider orders from this source. | [optional]
**exclusions** | Option<[**Vec<models::Model525>**](Model525.md)> | Items to exclude | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


