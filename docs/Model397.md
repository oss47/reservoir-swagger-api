# Model397

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_block** | **f32** |  | 
**to_block** | **f32** |  | 
**block_range** | Option<**f32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


