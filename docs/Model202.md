# Model202

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


