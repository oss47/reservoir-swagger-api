# Model384

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain_id** | **f64** |  | 
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**kind** | Option<**String**> | Can be erc721, erc115, etc. | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**image_small** | Option<**String**> |  | [optional]
**image_large** | Option<**String**> |  | [optional]
**metadata** | Option<[**serde_json::Value**](.md)> |  | [optional]
**description** | Option<**String**> |  | [optional]
**supply** | Option<**f64**> | Can be higher than one if erc1155. | [optional]
**remaining_supply** | Option<**f64**> |  | [optional]
**rarity_score** | Option<**f64**> | No rarity for collections over 100k | [optional]
**rarity_rank** | Option<**f64**> | No rarity rank for collections over 100k | [optional]
**media** | Option<**String**> |  | [optional]
**is_flagged** | Option<**bool**> |  | [optional][default to false]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**metadata_disabled** | Option<**bool**> |  | [optional][default to false]
**last_flag_update** | Option<**String**> |  | [optional]
**last_flag_change** | Option<**String**> |  | [optional]
**collection** | Option<[**models::Model381**](Model381.md)> |  | [optional]
**last_sale** | Option<[**models::Model58**](Model58.md)> |  | [optional]
**top_bid** | Option<[**models::Model322**](Model322.md)> |  | [optional]
**last_appraisal_value** | Option<**f64**> | The value of the last sale.Can be null. | [optional]
**attributes** | Option<[**Vec<models::Model382>**](Model382.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


