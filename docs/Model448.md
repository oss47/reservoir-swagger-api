# Model448

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | Option<**String**> | The source domain to sync. Example: `reservoir.market` | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


