# Model66

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**floor_ask** | Option<[**models::Model64**](Model64.md)> |  | [optional]
**top_bid** | Option<[**models::Model65**](Model65.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


