# Collection

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**slug** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**metadata** | Option<[**serde_json::Value**](.md)> |  | [optional]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**token_count** | Option<**String**> |  | [optional]
**on_sale_count** | Option<**String**> |  | [optional]
**primary_contract** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**royalties** | Option<[**models::Royalties**](royalties.md)> |  | [optional]
**last_buy** | Option<[**models::LastBuy**](lastBuy.md)> |  | [optional]
**last_sell** | Option<[**models::LastBuy**](lastBuy.md)> |  | [optional]
**floor_ask** | Option<[**models::FloorAsk**](floorAsk.md)> |  | [optional]
**top_bid** | Option<[**models::TopBid**](topBid.md)> |  | [optional]
**rank** | Option<[**models::Rank**](rank.md)> |  | [optional]
**volume** | Option<[**models::Rank**](rank.md)> |  | [optional]
**volume_change** | Option<[**models::VolumeChange**](volumeChange.md)> |  | [optional]
**floor_sale** | Option<[**models::VolumeChange**](volumeChange.md)> |  | [optional]
**floor_sale_change** | Option<[**models::VolumeChange**](volumeChange.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


