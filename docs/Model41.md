# Model41

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**kind** | **String** |  | 
**side** | **String** |  | 
**fillability_status** | **String** |  | 
**approval_status** | **String** |  | 
**token_set_id** | **String** |  | 
**token_set_schema_hash** | **String** |  | 
**maker** | **String** |  | 
**taker** | **String** |  | 
**price** | **f64** |  | 
**value** | **f64** |  | 
**valid_from** | **f64** |  | 
**valid_until** | **f64** |  | 
**source_id** | Option<**String**> |  | [optional]
**fee_bps** | Option<**f64**> |  | [optional]
**fee_breakdown** | Option<[**Vec<models::Model40>**](Model40.md)> |  | [optional]
**expiration** | **f64** |  | 
**created_at** | **String** |  | 
**updated_at** | **String** |  | 
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


