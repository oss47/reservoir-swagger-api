# Model529

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Response is `complete` or `incomplete`. | 
**tip** | Option<**String**> |  | [optional]
**order_ids** | Option<**Vec<String>**> |  | [optional]
**data** | Option<[**serde_json::Value**](.md)> |  | [optional]
**check** | Option<[**models::Check**](check.md)> |  | [optional]
**gas_estimate** | Option<**f64**> | Approximation of gas used (only applies to `transaction` items) | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


