# Model435

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_block** | **f64** |  | 
**to_block** | **f64** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


