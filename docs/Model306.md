# Model306

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | Option<**String**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**nonce** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**valid_from** | Option<**f64**> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]
**source** | Option<[**models::Source**](source.md)> |  | [optional]
**dynamic_pricing** | Option<[**models::DynamicPricing**](dynamicPricing.md)> |  | [optional]
**is_dynamic** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


