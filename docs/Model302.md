# Model302

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**slug** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**banner** | Option<**String**> |  | [optional]
**discord_url** | Option<**String**> |  | [optional]
**external_url** | Option<**String**> |  | [optional]
**twitter_username** | Option<**String**> |  | [optional]
**opensea_verification_status** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**token_count** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**primary_contract** | Option<**String**> |  | [optional]
**floor_ask_price** | Option<**f32**> |  | [optional]
**top_bid_value** | Option<**f32**> |  | [optional]
**top_bid_maker** | Option<**String**> |  | [optional]
**top_bid_source_domain** | Option<**String**> |  | [optional]
**rank** | Option<[**crate::models::Rank**](rank.md)> |  | [optional]
**volume** | Option<[**crate::models::Rank**](rank.md)> |  | [optional]
**volume_change** | Option<[**crate::models::VolumeChange**](volumeChange.md)> |  | [optional]
**floor_sale** | Option<[**crate::models::VolumeChange**](volumeChange.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


