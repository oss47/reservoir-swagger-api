/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

/// Options : Additional options.
#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Options {
    #[serde(rename = "seaport-v1.4", skip_serializing_if = "Option::is_none")]
    pub seaport_v1_period_4: Option<Box<models::SeaportV1Period4>>,
    #[serde(rename = "seaport-v1.5", skip_serializing_if = "Option::is_none")]
    pub seaport_v1_period_5: Option<Box<models::SeaportV1Period4>>,
    #[serde(rename = "seaport-v1.6", skip_serializing_if = "Option::is_none")]
    pub seaport_v1_period_6: Option<Box<models::SeaportV1Period4>>,
    #[serde(
        rename = "payment-processor-v2",
        skip_serializing_if = "Option::is_none"
    )]
    pub payment_processor_v2: Option<Box<models::PaymentProcessorV2>>,
}

impl Options {
    /// Additional options.
    pub fn new() -> Options {
        Options {
            seaport_v1_period_4: None,
            seaport_v1_period_5: None,
            seaport_v1_period_6: None,
            payment_processor_v2: None,
        }
    }
}
