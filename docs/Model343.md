# Model343

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<[**models::Model341**](Model341.md)> |  | [optional]
**ownership** | Option<[**models::Model342**](Model342.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


