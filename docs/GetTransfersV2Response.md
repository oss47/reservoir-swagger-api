# GetTransfersV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transfers** | Option<[**Vec<models::Model105>**](Model105.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


