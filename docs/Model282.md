# Model282

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplaces** | Option<[**Vec<models::Model280>**](Model280.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


