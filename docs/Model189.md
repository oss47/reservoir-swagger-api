# Model189

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**kind** | **String** | This is the `orderKind`. | 
**side** | **String** | Either `buy` or `sell` | 
**status** | Option<**String**> | Can be `active`, `inactive`, `expired`, `canceled`, or `filled` | [optional]
**token_set_id** | **String** |  | 
**token_set_schema_hash** | **String** |  | 
**contract** | Option<**String**> |  | [optional]
**contract_kind** | Option<**String**> |  | [optional]
**maker** | **String** |  | 
**taker** | **String** |  | 
**price** | Option<[**models::Model124**](Model124.md)> |  | [optional]
**valid_from** | **f64** |  | 
**valid_until** | **f64** |  | 
**quantity_filled** | Option<**f64**> | With ERC1155s, quantity can be higher than 1 | [optional]
**quantity_remaining** | Option<**f64**> | With ERC1155s, quantity can be higher than 1 | [optional]
**dynamic_pricing** | Option<[**models::Model185**](Model185.md)> |  | [optional]
**criteria** | Option<[**models::Model120**](Model120.md)> |  | [optional]
**source** | Option<[**serde_json::Value**](.md)> |  | [optional]
**fee_bps** | Option<**f64**> |  | [optional]
**fee_breakdown** | Option<[**Vec<models::Model186>**](Model186.md)> |  | [optional]
**expiration** | **f64** |  | 
**is_reservoir** | Option<**bool**> |  | [optional]
**is_dynamic** | Option<**bool**> |  | [optional]
**created_at** | **String** | Time when added to indexer | 
**updated_at** | **String** | Time when updated in indexer | 
**originated_at** | Option<**String**> | Time when created by maker | [optional]
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]
**is_native_off_chain_cancellable** | Option<**bool**> |  | [optional]
**depth** | Option<[**Vec<models::Model188>**](Model188.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


