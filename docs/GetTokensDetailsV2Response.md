# GetTokensDetailsV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokens** | Option<[**Vec<models::Model230>**](Model230.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


