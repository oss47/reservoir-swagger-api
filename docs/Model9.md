# Model9

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**slug** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**metadata** | Option<[**serde_json::Value**](.md)> |  | [optional]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**token_count** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**royalties** | Option<[**models::Royalties**](royalties.md)> |  | [optional]
**floor_ask_price** | Option<**f64**> |  | [optional]
**top_bid_value** | Option<**f64**> |  | [optional]
**top_bid_maker** | Option<**String**> |  | [optional]
**rank** | Option<[**models::Rank**](rank.md)> |  | [optional]
**volume** | Option<[**models::Rank**](rank.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


