# Model121

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**side** | Option<**String**> |  | [optional]
**source** | Option<[**models::Source**](source.md)> |  | [optional]
**criteria** | Option<[**models::Model120**](Model120.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


