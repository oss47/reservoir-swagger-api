# GetStatsV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stats** | Option<[**models::Model67**](Model67.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


