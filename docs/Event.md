# Event

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**f64**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**tx_timestamp** | Option<**f64**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


