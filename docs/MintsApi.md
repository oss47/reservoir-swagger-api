# \MintsApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_collections_trendingmints_v1**](MintsApi.md#get_collections_trendingmints_v1) | **GET** /collections/trending-mints/v1 | Top Trending Mints



## get_collections_trendingmints_v1

> crate::models::GetTrendingMintsV1Response get_collections_trendingmints_v1(period, r#type, limit, normalize_royalties, use_non_flagged_floor_ask)
Top Trending Mints

Get top trending mints

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**period** | Option<**String**> | Time window to aggregate. |  |[default to 24h]
**r#type** | Option<**String**> | The type of the mint (free/paid). |  |[default to any]
**limit** | Option<**i32**> | Amount of items returned in response. Default is 50 and max is 50. Expected to be sorted and filtered on client side. |  |[default to 50]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, return the non flagged floor ask. Supported only when `normalizeRoyalties` is false. |  |[default to false]

### Return type

[**crate::models::GetTrendingMintsV1Response**](get-trending-mintsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

