/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model39 {
    #[serde(rename = "id")]
    pub id: f64,
    #[serde(rename = "orderId")]
    pub order_id: String,
    #[serde(rename = "orderbook")]
    pub orderbook: String,
    /// Possible values: pending - Waiting to be submitted. posted - Successfully submitted to the marketplace. posted - Failed to be submitted to the marketplace (see statusReason for detail).
    #[serde(rename = "status")]
    pub status: String,
    #[serde(rename = "statusReason")]
    pub status_reason: String,
    /// Time when added to indexer
    #[serde(rename = "createdAt")]
    pub created_at: String,
    /// Time when updated in indexer
    #[serde(rename = "updatedAt")]
    pub updated_at: String,
}

impl Model39 {
    pub fn new(
        id: f64,
        order_id: String,
        orderbook: String,
        status: String,
        status_reason: String,
        created_at: String,
        updated_at: String,
    ) -> Model39 {
        Model39 {
            id,
            order_id,
            orderbook,
            status,
            status_reason,
            created_at,
            updated_at,
        }
    }
}
