# Model493

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | **String** | Refresh the given collection. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | 
**override_cool_down** | Option<**bool**> | If true, will force a refresh regardless of cool down. Requires an authorized api key to be passed. | [optional][default to false]
**refresh_tokens** | Option<**bool**> | If true, will refresh the metadata for the tokens in the collection. | [optional][default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


