# GetCollectionsSetOwnersDistributionV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owners_distribution** | Option<[**Vec<models::Model283>**](Model283.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


