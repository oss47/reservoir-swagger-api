# Model437

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**by** | **String** |  | 
**id** | **String** |  | 
**token** | **String** |  | 
**maker** | **String** |  | 
**contract** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


