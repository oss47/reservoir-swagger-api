/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model306 {
    #[serde(rename = "orderId", skip_serializing_if = "Option::is_none")]
    pub order_id: Option<String>,
    #[serde(rename = "maker", skip_serializing_if = "Option::is_none")]
    pub maker: Option<String>,
    #[serde(rename = "nonce", skip_serializing_if = "Option::is_none")]
    pub nonce: Option<String>,
    #[serde(rename = "price", skip_serializing_if = "Option::is_none")]
    pub price: Option<Box<models::Price>>,
    #[serde(rename = "validFrom", skip_serializing_if = "Option::is_none")]
    pub valid_from: Option<f64>,
    #[serde(rename = "validUntil", skip_serializing_if = "Option::is_none")]
    pub valid_until: Option<f64>,
    #[serde(rename = "source", skip_serializing_if = "Option::is_none")]
    pub source: Option<Box<models::Source>>,
    #[serde(rename = "dynamicPricing", skip_serializing_if = "Option::is_none")]
    pub dynamic_pricing: Option<Box<models::DynamicPricing>>,
    #[serde(rename = "isDynamic", skip_serializing_if = "Option::is_none")]
    pub is_dynamic: Option<bool>,
}

impl Model306 {
    pub fn new() -> Model306 {
        Model306 {
            order_id: None,
            maker: None,
            nonce: None,
            price: None,
            valid_from: None,
            valid_until: None,
            source: None,
            dynamic_pricing: None,
            is_dynamic: None,
        }
    }
}
