# Model470

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | Option<[**models::Model469**](Model469.md)> |  | [optional]
**orderbook** | Option<**String**> |  | [optional][default to Reservoir]
**orderbook_api_key** | Option<**String**> |  | [optional]
**source** | Option<**String**> | The name of the source | [optional]
**attribute** | Option<[**models::Attribute**](attribute.md)> |  | [optional]
**collection** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**is_non_flagged** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


