/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct GetExecuteBidV4Response {
    #[serde(rename = "steps", skip_serializing_if = "Option::is_none")]
    pub steps: Option<Vec<models::Model498>>,
    #[serde(rename = "query", skip_serializing_if = "Option::is_none")]
    pub query: Option<serde_json::Value>,
}

impl GetExecuteBidV4Response {
    pub fn new() -> GetExecuteBidV4Response {
        GetExecuteBidV4Response {
            steps: None,
            query: None,
        }
    }
}
