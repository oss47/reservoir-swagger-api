# Model82

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | Option<[**models::Currency**](currency.md)> |  | [optional]
**amount** | Option<[**models::Amount**](amount.md)> |  | [optional]
**net_amount** | Option<[**models::NetAmount**](netAmount.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


