# Model534

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **String** | Execution kind | 
**id** | **String** | The id of the execution (eg. transaction / order / intent hash) | 
**chain_id** | Option<**f32**> | Chain id where the action is happening (only relevant for 'cross-chain-transaction' actions) | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


