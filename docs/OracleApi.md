# \OracleApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_oracle_collections_bidaskmidpoint_v1**](OracleApi.md#get_oracle_collections_bidaskmidpoint_v1) | **GET** /oracle/collections/bid-ask-midpoint/v1 | Collection bid-ask midpoint
[**get_oracle_collections_floorask_v6**](OracleApi.md#get_oracle_collections_floorask_v6) | **GET** /oracle/collections/floor-ask/v6 | Collection floor
[**get_oracle_collections_topbid_v3**](OracleApi.md#get_oracle_collections_topbid_v3) | **GET** /oracle/collections/top-bid/v3 | Collection top bid oracle
[**get_oracle_tokens_status_v3**](OracleApi.md#get_oracle_tokens_status_v3) | **GET** /oracle/tokens/status/v3 | Token status oracle



## get_oracle_collections_bidaskmidpoint_v1

> models::GetCollectionBidAskMidpointOracleV1Response get_oracle_collections_bidaskmidpoint_v1(kind, currency, twap_seconds, collection, token, signer)
Collection bid-ask midpoint

Get a signed message of any collection's bid-ask midpoint (spot or twap). This is approximation of the colletion price. The oracle's address is 0xAeB1D03929bF87F69888f381e73FBf75753d75AF. The address is the same for all chains.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**kind** | Option<**String**> |  |  |[default to spot]
**currency** | Option<**String**> |  |  |[default to 0x0000000000000000000000000000000000000000]
**twap_seconds** | Option<**f64**> |  |  |[default to 86400.0]
**collection** | Option<**String**> |  |  |
**token** | Option<**String**> |  |  |
**signer** | Option<**String**> |  |  |[default to 0xaeb1d03929bf87f69888f381e73fbf75753d75af]

### Return type

[**models::GetCollectionBidAskMidpointOracleV1Response**](getCollectionBidAskMidpointOracleV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_oracle_collections_floorask_v6

> models::GetCollectionFloorAskOracleV6Response get_oracle_collections_floorask_v6(kind, currency, twap_seconds, eip3668_calldata, collection, token, use_non_flagged_floor_ask, signer)
Collection floor

Get a signed message of any collection's floor price (spot or twap). The oracle's address is 0xAeB1D03929bF87F69888f381e73FBf75753d75AF. The address is the same for all chains.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**kind** | Option<**String**> |  |  |[default to spot]
**currency** | Option<**String**> |  |  |[default to 0x0000000000000000000000000000000000000000]
**twap_seconds** | Option<**f64**> |  |  |[default to 86400.0]
**eip3668_calldata** | Option<**String**> |  |  |
**collection** | Option<**String**> |  |  |
**token** | Option<**String**> |  |  |
**use_non_flagged_floor_ask** | Option<**bool**> | If true, will use the collection non flagged floor ask events. |  |[default to false]
**signer** | Option<**String**> |  |  |[default to 0xaeb1d03929bf87f69888f381e73fbf75753d75af]

### Return type

[**models::GetCollectionFloorAskOracleV6Response**](getCollectionFloorAskOracleV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_oracle_collections_topbid_v3

> models::GetCollectionBidAskMidpointOracleV1Response get_oracle_collections_topbid_v3(kind, currency, twap_seconds, collection, token, signer)
Collection top bid oracle

Get a signed message of any collection's top bid price (spot or twap). The oracle's address is 0xAeB1D03929bF87F69888f381e73FBf75753d75AF. The address is the same for all chains.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**kind** | Option<**String**> |  |  |[default to spot]
**currency** | Option<**String**> |  |  |[default to 0x0000000000000000000000000000000000000000]
**twap_seconds** | Option<**f64**> |  |  |[default to 86400.0]
**collection** | Option<**String**> |  |  |
**token** | Option<**String**> |  |  |
**signer** | Option<**String**> |  |  |[default to 0xaeb1d03929bf87f69888f381e73fbf75753d75af]

### Return type

[**models::GetCollectionBidAskMidpointOracleV1Response**](getCollectionBidAskMidpointOracleV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_oracle_tokens_status_v3

> models::GetTokenStatusOracleV3Response get_oracle_tokens_status_v3(tokens, signer)
Token status oracle

Get a signed message of a token's details (flagged status and last transfer time). The oracle's address is 0xAeB1D03929bF87F69888f381e73FBf75753d75AF. The address is the same for all chains.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**tokens** | [**Vec<String>**](String.md) |  | [required] |
**signer** | Option<**String**> |  |  |[default to 0xaeb1d03929bf87f69888f381e73fbf75753d75af]

### Return type

[**models::GetTokenStatusOracleV3Response**](getTokenStatusOracleV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

