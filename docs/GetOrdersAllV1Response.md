# GetOrdersAllV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orders** | Option<[**Vec<models::Model169>**](Model169.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


