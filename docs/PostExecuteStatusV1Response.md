# PostExecuteStatusV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**details** | Option<**String**> |  | [optional]
**tx_hashes** | Option<**Vec<String>**> |  | [optional]
**time** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


