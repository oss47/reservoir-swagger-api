# Model107

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**side** | Option<**String**> |  | [optional]
**source** | Option<[**crate::models::Source**](source.md)> |  | [optional]
**criteria** | Option<[**crate::models::Model106**](Model106.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


