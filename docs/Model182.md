# Model182

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> |  | [optional]
**from_address** | Option<**String**> |  | [optional]
**to_address** | Option<**String**> |  | [optional]
**price** | Option<[**crate::models::Price**](price.md)> |  | [optional]
**amount** | Option<**f32**> |  | [optional]
**timestamp** | Option<**f32**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**token** | Option<[**crate::models::Model181**](Model181.md)> |  | [optional]
**collection** | Option<[**crate::models::Model104**](Model104.md)> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**log_index** | Option<**f32**> |  | [optional]
**batch_index** | Option<**f32**> |  | [optional]
**order** | Option<[**crate::models::Model111**](Model111.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


