# Model506

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Returns `currency-wrapping`, `currency-approval`, or `order-signature`. | 
**kind** | **String** | Returns `request`, `signature`, or `transaction`. | 
**action** | **String** |  | 
**description** | **String** |  | 
**items** | [**Vec<models::Model504>**](Model504.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


