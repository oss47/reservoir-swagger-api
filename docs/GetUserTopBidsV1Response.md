# GetUserTopBidsV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_tokens_with_bids** | Option<**f64**> |  | [optional]
**top_bids** | Option<[**Vec<models::Model420>**](Model420.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


