# Model243

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**token** | Option<[**models::Model204**](Model204.md)> |  | [optional]
**from** | Option<**String**> |  | [optional]
**to** | Option<**String**> |  | [optional]
**amount** | Option<**String**> | Can be more than 1 if erc1155. | [optional]
**block** | Option<**f64**> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**log_index** | Option<**f64**> |  | [optional]
**batch_index** | Option<**f64**> |  | [optional]
**timestamp** | Option<**f64**> |  | [optional]
**is_deleted** | Option<**bool**> |  | [optional]
**updated_at** | Option<**String**> | Time when updated in indexer | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


