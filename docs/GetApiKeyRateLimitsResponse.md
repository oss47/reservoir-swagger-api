# GetApiKeyRateLimitsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | Option<**String**> |  | [optional]
**app_name** | Option<**String**> |  | [optional]
**website** | Option<**String**> |  | [optional]
**email** | Option<**String**> |  | [optional]
**active** | Option<**bool**> |  | [optional]
**tier** | Option<**f64**> |  | [optional]
**ips** | Option<**Vec<String>**> |  | [optional]
**origins** | Option<**Vec<String>**> |  | [optional]
**permissions** | Option<[**serde_json::Value**](.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**rev_share_bps** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


