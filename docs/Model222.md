# Model222

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> |  | [optional]
**from_address** | Option<**String**> |  | [optional]
**to_address** | Option<**String**> |  | [optional]
**price** | Option<**f32**> |  | [optional]
**amount** | Option<**f32**> |  | [optional]
**timestamp** | Option<**f32**> |  | [optional]
**token** | Option<[**crate::models::Model99**](Model99.md)> |  | [optional]
**collection** | Option<[**crate::models::Model100**](Model100.md)> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**log_index** | Option<**f32**> |  | [optional]
**batch_index** | Option<**f32**> |  | [optional]
**source** | Option<[**crate::models::Source**](source.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


