# Model142

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid** | Option<[**crate::models::Model141**](Model141.md)> |  | [optional]
**event** | Option<[**crate::models::Model133**](Model133.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


