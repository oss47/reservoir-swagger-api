# Model481

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orders** | Option<[**Vec<models::Model479>**](Model479.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


