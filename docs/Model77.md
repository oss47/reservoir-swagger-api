# Model77

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**valid_from** | Option<**f64**> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]
**quantity_filled** | Option<**f64**> |  | [optional]
**quantity_remaining** | Option<**f64**> |  | [optional]
**dynamic_pricing** | Option<[**models::DynamicPricing**](dynamicPricing.md)> |  | [optional]
**source** | Option<[**models::Source**](source.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


