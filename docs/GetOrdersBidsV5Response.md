# GetOrdersBidsV5Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orders** | Option<[**Vec<models::Model189>**](Model189.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


