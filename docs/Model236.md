# Model236

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | Option<**String**> |  | [optional]
**domain** | Option<**String**> |  | [optional]
**image_url** | Option<**String**> |  | [optional]
**fee** | Option<[**crate::models::Model233**](Model233.md)> |  | [optional]
**royalties** | Option<[**crate::models::Model234**](Model234.md)> |  | [optional]
**orderbook** | Option<**String**> |  | [optional]
**order_kind** | Option<**String**> |  | [optional]
**listing_enabled** | Option<**bool**> |  | [optional]
**custom_fees_supported** | Option<**bool**> |  | [optional]
**minimum_bid_expiry** | Option<**f32**> |  | [optional]
**minimum_precision** | Option<**String**> |  | [optional]
**collection_bid_supported** | Option<**bool**> |  | [optional]
**trait_bid_supported** | Option<**bool**> |  | [optional]
**partial_bid_supported** | Option<**bool**> | This indicates whether or not multi quantity bidding is supported | [optional]
**supported_bid_currencies** | Option<**Vec<String>**> | erc20 contract addresses | [optional]
**payment_tokens** | Option<[**Vec<crate::models::Model235>**](Model235.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


