# Model560

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maker** | **String** | Address of wallet making the order. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | 
**source** | Option<**String**> | Domain of your app that is creating the order, e.g. `myapp.xyz`. This is used for filtering, and to attribute the \"order source\" of sales in on-chain analytics, to help your app get discovered. Lean more <a href='https://docs.reservoir.tools/docs/calldata-attribution'>here</a> | [optional]
**params** | Option<[**Vec<models::Model558>**](Model558.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


