# Model232

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | **String** |  | 
**token_id** | **String** |  | 
**name** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**collection** | Option<[**models::Model69**](Model69.md)> |  | [optional]
**last_buy** | Option<[**models::LastBuy**](lastBuy.md)> |  | [optional]
**last_sell** | Option<[**models::LastBuy**](lastBuy.md)> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**attributes** | Option<[**Vec<models::Model225>**](Model225.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


