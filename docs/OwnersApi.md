# \OwnersApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_collections_collection_ownersdistribution_v1**](OwnersApi.md#get_collections_collection_ownersdistribution_v1) | **GET** /collections/{collection}/owners-distribution/v1 | Owners Collection Distribution
[**get_collectionssets_collectionssetid_ownersdistribution_v1**](OwnersApi.md#get_collectionssets_collectionssetid_ownersdistribution_v1) | **GET** /collections-sets/{collectionsSetId}/owners-distribution/v1 | Owners Collection Set Distribution
[**get_owners_commoncollections_v1**](OwnersApi.md#get_owners_commoncollections_v1) | **GET** /owners/common-collections/v1 | Common Collections
[**get_owners_crosscollections_v1**](OwnersApi.md#get_owners_crosscollections_v1) | **GET** /owners/cross-collections/v1 | Owners intersection
[**get_owners_v2**](OwnersApi.md#get_owners_v2) | **GET** /owners/v2 | Owners



## get_collections_collection_ownersdistribution_v1

> models::GetCollectionOwnersDistributionV1Response get_collections_collection_ownersdistribution_v1(collection)
Owners Collection Distribution

This API can be used to show what the distribution of owners in a collection looks like.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |

### Return type

[**models::GetCollectionOwnersDistributionV1Response**](getCollectionOwnersDistributionV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collectionssets_collectionssetid_ownersdistribution_v1

> models::GetCollectionsSetOwnersDistributionV1Response get_collectionssets_collectionssetid_ownersdistribution_v1(collections_set_id)
Owners Collection Set Distribution

This API can be used to show what the distribution of owners in a collections set id looks like.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collections_set_id** | **String** | Filter to a particular collections set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` | [required] |

### Return type

[**models::GetCollectionsSetOwnersDistributionV1Response**](getCollectionsSetOwnersDistributionV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_owners_commoncollections_v1

> models::GetCommonCollectionsOwnersV1Response get_owners_commoncollections_v1(owners, limit)
Common Collections

This API can be used to find top common collections from an array of owners.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owners** | [**Vec<String>**](String.md) | Array of owner addresses. Max limit is 50. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**limit** | Option<**i32**> | Amount of collections returned in response. Max limit is 100. |  |[default to 20]

### Return type

[**models::GetCommonCollectionsOwnersV1Response**](getCommonCollectionsOwnersV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_owners_crosscollections_v1

> models::GetCrossCollectionsOwnersV1Response get_owners_crosscollections_v1(collections, limit)
Owners intersection

Find which addresses own the most of a group of collections.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collections** | [**Vec<String>**](String.md) | Filter to one or more collections. Max limit is 5. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**limit** | Option<**i32**> | Amount of owners returned in response. Max limit is 50. |  |[default to 20]

### Return type

[**models::GetCrossCollectionsOwnersV1Response**](getCrossCollectionsOwnersV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_owners_v2

> models::GetOwnersV2Response get_owners_v2(collections_set_id, collection, contract, token, attributes, offset, limit, display_currency)
Owners

Get owners with various filters applied, and a summary of their ownership. Useful for exploring top owners in a collection or attribute.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collections_set_id** | Option<**String**> | Filter to a particular collection set id. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Attributes are case sensitive. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/owners/v1?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original` or `https://api.reservoir.tools/owners/v1?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original&attribute[Type]=Sibling` |  |
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 500. |  |[default to 20]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetOwnersV2Response**](getOwnersV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

