# Model297

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]
**source** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


