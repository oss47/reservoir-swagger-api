# Model110

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**route** | Option<**String**> |  | [optional]
**method** | Option<**String**> |  | [optional]
**allowed_requests** | Option<**f64**> |  | [optional]
**per_seconds** | Option<**f64**> |  | [optional]
**payload** | Option<[**Vec<serde_json::Value>**](serde_json::Value.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


