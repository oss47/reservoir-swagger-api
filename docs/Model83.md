# Model83

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**slug** | Option<**String**> |  | [optional]
**symbol** | Option<**String**> |  | [optional]
**creator** | Option<**String**> |  | [optional]
**token_count** | Option<**f64**> |  | [optional]
**metadata_disabled** | Option<**bool**> |  | [optional][default to false]
**floor_ask_price** | Option<[**models::Model82**](Model82.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


