/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model125 {
    #[serde(rename = "tokenId", skip_serializing_if = "Option::is_none")]
    pub token_id: Option<String>,
    #[serde(rename = "tokenName", skip_serializing_if = "Option::is_none")]
    pub token_name: Option<String>,
    #[serde(rename = "tokenImage", skip_serializing_if = "Option::is_none")]
    pub token_image: Option<String>,
    #[serde(rename = "isSpam", skip_serializing_if = "Option::is_none")]
    pub is_spam: Option<bool>,
    #[serde(rename = "isNsfw", skip_serializing_if = "Option::is_none")]
    pub is_nsfw: Option<bool>,
    #[serde(rename = "rarityScore", skip_serializing_if = "Option::is_none")]
    pub rarity_score: Option<f64>,
    #[serde(rename = "rarityRank", skip_serializing_if = "Option::is_none")]
    pub rarity_rank: Option<f64>,
}

impl Model125 {
    pub fn new() -> Model125 {
        Model125 {
            token_id: None,
            token_name: None,
            token_image: None,
            is_spam: None,
            is_nsfw: None,
            rarity_score: None,
            rarity_rank: None,
        }
    }
}
