# Model501

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<**String**> | Bid on a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [optional]
**token_set_id** | Option<**String**> | Bid on a particular token set. Cannot be used with cross-posting to OpenSea. Example: `token:CONTRACT:TOKEN_ID` representing a single token within contract, `contract:CONTRACT` representing a whole contract, `range:CONTRACT:START_TOKEN_ID:END_TOKEN_ID` representing a continuous token id range within a contract and `list:CONTRACT:TOKEN_IDS_HASH` representing a list of token ids within a contract. | [optional]
**collection** | Option<**String**> | Bid on a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [optional]
**attribute_key** | Option<**String**> | Bid on a particular attribute key. This is case sensitive. Example: `Composition` | [optional]
**attribute_value** | Option<**String**> | Bid on a particular attribute value. This is case sensitive. Example: `Teddy (#33)` | [optional]
**quantity** | Option<**f64**> | Quantity of tokens to bid on. | [optional]
**wei_price** | **String** | Amount bidder is willing to offer in the smallest denomination for the specific currency. Example: `1000000000000000000` | 
**order_kind** | Option<**String**> | Exchange protocol used to create order. Example: `seaport-v1.5` | [optional][default to SeaportV1Period5]
**options** | Option<[**models::Options**](options.md)> |  | [optional]
**orderbook** | Option<**String**> | Orderbook where order is placed. Example: `Reservoir` | [optional][default to Reservoir]
**orderbook_api_key** | Option<**String**> | Optional API key for the target orderbook | [optional]
**automated_royalties** | Option<**bool**> | If true, royalty amounts and recipients will be set automatically. | [optional][default to true]
**royalty_bps** | Option<**f64**> | Set a maximum amount of royalties to pay, rather than the full amount. Only relevant when using automated royalties. 1 BPS = 0.01% Note: OpenSea does not support values below 50 bps. | [optional]
**fees** | Option<**Vec<String>**> | Deprecated, use `marketplaceFees` and/or `customRoyalties` | [optional]
**marketplace_fees** | Option<**Vec<String>**> | List of marketplace fees (formatted as `feeRecipient:feeBps`) to be bundled within the order. 1 BPS = 0.01% Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:100` | [optional]
**marketplace_flat_fees** | Option<**Vec<String>**> | List of marketplace flat fees (formatted as `feeRecipient:weiAmount`) to be bundled within the order. | [optional]
**custom_royalties** | Option<**Vec<String>**> | List of custom royalties (formatted as `feeRecipient:feeBps`) to be bundled within the order. 1 BPS = 0.01% Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:100` | [optional]
**exclude_flagged_tokens** | Option<**bool**> | If true flagged tokens will be excluded | [optional][default to false]
**listing_time** | Option<**String**> | Unix timestamp (seconds) indicating when listing will be listed. Example: `1656080318` | [optional]
**expiration_time** | Option<**String**> | Unix timestamp (seconds) indicating when listing will expire. Example: `1656080318` | [optional]
**salt** | Option<**String**> | Optional. Random string to make the order unique | [optional]
**nonce** | Option<**String**> | Optional. Set a custom nonce | [optional]
**currency** | Option<**String**> |  | [optional][default to 0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2]
**use_permit** | Option<**bool**> | When true, will use permit to avoid approvals. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


