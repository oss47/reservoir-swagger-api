# GetUserTopBidsV4Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_tokens_with_bids** | Option<**f64**> | Amount of token with bids. | [optional]
**total_amount** | Option<**f64**> | Amount of currency from all token bids; native currency unless `displayCurrency` passed | [optional]
**top_bids** | Option<[**Vec<models::Model427>**](Model427.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


