/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model295 {
    #[serde(rename = "collection", skip_serializing_if = "Option::is_none")]
    pub collection: Option<Box<models::Model285>>,
    #[serde(rename = "topBid", skip_serializing_if = "Option::is_none")]
    pub top_bid: Option<Box<models::Model294>>,
    #[serde(rename = "event", skip_serializing_if = "Option::is_none")]
    pub event: Option<Box<models::Model287>>,
}

impl Model295 {
    pub fn new() -> Model295 {
        Model295 {
            collection: None,
            top_bid: None,
            event: None,
        }
    }
}
