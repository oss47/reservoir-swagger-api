# Model94

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<[**crate::models::Model45**](Model45.md)> |  | [optional]
**from** | Option<**String**> |  | [optional]
**to** | Option<**String**> |  | [optional]
**amount** | Option<**String**> | Can be higher than 1 if erc1155. | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**block** | Option<**f32**> |  | [optional]
**log_index** | Option<**f32**> |  | [optional]
**batch_index** | Option<**f32**> |  | [optional]
**timestamp** | Option<**f32**> |  | [optional]
**is_deleted** | Option<**bool**> |  | [optional]
**updated_at** | Option<**String**> | Time when last updated in indexer | [optional]
**price** | Option<[**crate::models::Price**](price.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


