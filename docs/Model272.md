# Model272

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | Option<**String**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**nonce** | Option<**String**> |  | [optional]
**price** | Option<[**crate::models::Price**](price.md)> |  | [optional]
**valid_from** | Option<**f32**> |  | [optional]
**valid_until** | Option<**f32**> |  | [optional]
**source** | Option<**String**> |  | [optional]
**is_dynamic** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


