# GetExecuteBuyV6Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**steps** | Option<[**Vec<models::Model521>**](Model521.md)> |  | [optional]
**errors** | Option<[**Vec<models::Model523>**](Model523.md)> |  | [optional]
**path** | Option<[**Vec<models::Model516>**](Model516.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


