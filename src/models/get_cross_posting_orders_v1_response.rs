/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct GetCrossPostingOrdersV1Response {
    #[serde(rename = "orders", skip_serializing_if = "Option::is_none")]
    pub orders: Option<Vec<models::Model39>>,
    #[serde(rename = "continuation", skip_serializing_if = "Option::is_none")]
    pub continuation: Option<String>,
}

impl GetCrossPostingOrdersV1Response {
    pub fn new() -> GetCrossPostingOrdersV1Response {
        GetCrossPostingOrdersV1Response {
            orders: None,
            continuation: None,
        }
    }
}
