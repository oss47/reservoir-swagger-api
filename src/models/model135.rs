/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model135 {
    #[serde(rename = "contract", skip_serializing_if = "Option::is_none")]
    pub contract: Option<String>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(rename = "timestamp", skip_serializing_if = "Option::is_none")]
    pub timestamp: Option<f64>,
    #[serde(rename = "toAddress", skip_serializing_if = "Option::is_none")]
    pub to_address: Option<String>,
    #[serde(rename = "price", skip_serializing_if = "Option::is_none")]
    pub price: Option<Box<models::Price>>,
    #[serde(rename = "collection", skip_serializing_if = "Option::is_none")]
    pub collection: Option<Box<models::Model134>>,
    #[serde(rename = "token", skip_serializing_if = "Option::is_none")]
    pub token: Option<Box<models::Model134>>,
}

impl Model135 {
    pub fn new() -> Model135 {
        Model135 {
            contract: None,
            r#type: None,
            timestamp: None,
            to_address: None,
            price: None,
            collection: None,
            token: None,
        }
    }
}
