# Model433

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**route** | **String** | The route for which the rule is created | 
**points** | Option<**f64**> |  | [optional]
**duration** | Option<**f64**> |  | [optional]
**points_to_consume** | Option<**f64**> |  | [optional]
**tier** | Option<**f64**> |  | [optional]
**api_key** | Option<**String**> |  | [optional][default to ]
**method** | Option<**String**> |  | [optional][default to ]
**payload** | Option<[**Vec<models::Model431>**](Model431.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


