# GetCrossCollectionsOwnersV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owners** | Option<[**Vec<models::Model200>**](Model200.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


