/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use async_graphql::SimpleObject;

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize, SimpleObject)]
pub struct PostOrderV2Response {
    #[serde(rename = "message", skip_serializing_if = "Option::is_none")]
    pub message: Option<String>,
    #[serde(rename = "orderId", skip_serializing_if = "Option::is_none")]
    pub order_id: Option<String>,
    /// Only available when posting to external orderbook. Can be used to retrieve the status of a cross-post order.
    #[serde(
        rename = "crossPostingOrderId",
        skip_serializing_if = "Option::is_none"
    )]
    pub cross_posting_order_id: Option<String>,
    #[serde(
        rename = "crossPostingOrderStatus",
        skip_serializing_if = "Option::is_none"
    )]
    pub cross_posting_order_status: Option<String>,
}

impl PostOrderV2Response {
    pub fn new() -> PostOrderV2Response {
        PostOrderV2Response {
            message: None,
            order_id: None,
            cross_posting_order_id: None,
            cross_posting_order_status: None,
        }
    }
}
