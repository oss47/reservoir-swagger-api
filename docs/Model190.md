# Model190

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection_id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**token_count** | Option<**String**> |  | [optional]
**all_time_volume** | Option<**f32**> |  | [optional]
**floor_ask_price** | Option<**f32**> |  | [optional]
**opensea_verification_status** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


