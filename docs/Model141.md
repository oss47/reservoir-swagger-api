# Model141

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Collection id | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**banner** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**description** | Option<**String**> |  | [optional]
**primary_contract** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**count** | Option<**i32**> |  | [optional]
**volume** | Option<**f64**> |  | [optional]
**volume_percent_change** | Option<**f64**> |  | [optional]
**count_percent_change** | Option<**f64**> |  | [optional]
**creator** | Option<**String**> |  | [optional]
**opensea_verification_status** | Option<**String**> |  | [optional]
**magiceden_verification_status** | Option<**String**> |  | [optional]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**on_sale_count** | Option<**i32**> |  | [optional]
**floor_ask** | Option<[**models::Model19**](Model19.md)> |  | [optional]
**top_bid** | Option<[**models::Model140**](Model140.md)> |  | [optional]
**floor_ask_percent_change** | Option<**f64**> |  | [optional]
**token_count** | Option<**f64**> | Total tokens within the collection. | [optional]
**owner_count** | Option<**f64**> | Unique number of owners. | [optional]
**collection_volume** | Option<[**models::Volume**](volume.md)> |  | [optional]
**volume_change** | Option<[**models::Model28**](Model28.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


