# Model408

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sync_details** | Option<[**crate::models::Model407**](Model407.md)> |  | [optional]
**from_block** | **i32** |  | 
**to_block** | **i32** |  | 
**blocks_per_batch** | Option<**i32**> |  | [optional]
**skip_non_fill_writes** | Option<**bool**> |  | [optional][default to false]
**backfill** | Option<**bool**> |  | [optional][default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


