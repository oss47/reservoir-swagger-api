# GetBidEventsV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | Option<[**Vec<models::Model153>**](Model153.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


