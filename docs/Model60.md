# Model60

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**social_image** | Option<**String**> |  | [optional]
**twitter_username** | Option<**String**> |  | [optional]
**icon** | Option<**String**> |  | [optional]
**token_url** | Option<**String**> |  | [optional]
**domain** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


