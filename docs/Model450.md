# Model450

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**queue_name** | **String** | The queue name to retry | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


