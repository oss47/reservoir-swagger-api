# Model313

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_id** | **String** |  | 
**last_flag_change** | Option<**String**> |  | [optional]
**is_flagged** | Option<**bool**> |  | [optional][default to false]
**contract** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


