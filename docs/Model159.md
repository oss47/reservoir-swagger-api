# Model159

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid** | Option<[**models::Model158**](Model158.md)> |  | [optional]
**event** | Option<[**models::Model150**](Model150.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


