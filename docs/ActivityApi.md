# \ActivityApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_collections_activity_v6**](ActivityApi.md#get_collections_activity_v6) | **GET** /collections/activity/v6 | Collection activity
[**get_tokens_token_activity_v5**](ActivityApi.md#get_tokens_token_activity_v5) | **GET** /tokens/{token}/activity/v5 | Token activity
[**get_users_activity_v6**](ActivityApi.md#get_users_activity_v6) | **GET** /users/activity/v6 | Users activity



## get_collections_activity_v6

> models::GetCollectionActivityV6Response get_collections_activity_v6(collection, collections_set_id, community, attributes, exclude_spam, exclude_nsfw, limit, sort_by, continuation, include_metadata, types, display_currency)
Collection activity

This API can be used to build a feed for a collection including sales, asks, transfers, mints, bids, cancelled bids, and cancelled asks types.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/collections/activity/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original` or `https://api.reservoir.tools/collections/activity/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original&attribute[Type]=Sibling` |  |
**exclude_spam** | Option<**bool**> | If true, will filter any activities marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any activities marked as nsfw. |  |[default to false]
**limit** | Option<**i32**> | Amount of items returned. Max limit is 50. |  |[default to 50]
**sort_by** | Option<**String**> | Order the items are returned in the response. The blockchain event time is `eventTimestamp`. The event time recorded is `createdAt`. |  |[default to eventTimestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. If true, max limit is 50. |  |[default to true]
**types** | Option<[**Vec<String>**](String.md)> |  |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetCollectionActivityV6Response**](getCollectionActivityV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens_token_activity_v5

> models::GetTokenActivityV5Response get_tokens_token_activity_v5(token, limit, sort_by, include_metadata, continuation, exclude_spam, exclude_nsfw, types, display_currency)
Token activity

This API can be used to build a feed for a token activity including sales, asks, transfers, mints, bids, cancelled bids, and cancelled asks types.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | **String** | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [required] |
**limit** | Option<**i32**> | Amount of items returned. Default and max is 20. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response. The blockchain event time is `eventTimestamp`. The event time recorded is `createdAt`. |  |[default to eventTimestamp]
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**exclude_spam** | Option<**bool**> | If true, will filter any activities marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any activities marked as nsfw. |  |[default to false]
**types** | Option<[**Vec<String>**](String.md)> |  |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetTokenActivityV5Response**](getTokenActivityV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_activity_v6

> models::GetUserActivityV6Response get_users_activity_v6(users, collection, exclude_spam, exclude_nsfw, collections_set_id, contracts_set_id, community, limit, sort_by, include_metadata, continuation, types, display_currency)
Users activity

This API can be used to build a feed for a user including sales, asks, transfers, mints, bids, cancelled bids, and cancelled asks types.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**users** | [**Vec<String>**](String.md) | Array of users addresses. Max is 50. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**collection** | Option<[**Vec<String>**](String.md)> |  |  |
**exclude_spam** | Option<**bool**> | If true, will filter any activities marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any activities marked as nsfw. |  |[default to false]
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**limit** | Option<**i32**> | Amount of items returned in response. If `includeMetadata=true` max limit is 20, otherwise max limit is 1,000. |  |[default to 20]
**sort_by** | Option<**String**> | Order the items are returned in the response. The blockchain event time is `eventTimestamp`. The event time recorded is `createdAt`. |  |[default to eventTimestamp]
**include_metadata** | Option<**bool**> | If true, metadata is included in the response. |  |[default to true]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. |  |

### Return type

[**models::GetUserActivityV6Response**](getUserActivityV6Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

