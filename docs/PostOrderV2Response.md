# PostOrderV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | Option<**String**> |  | [optional]
**order_id** | Option<**String**> |  | [optional]
**cross_posting_order_id** | Option<**String**> | Only available when posting to external orderbook. Can be used to retrieve the status of a cross-post order. | [optional]
**cross_posting_order_status** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


