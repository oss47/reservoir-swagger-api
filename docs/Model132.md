# Model132

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain_id** | Option<**f64**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**token_count** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**slug** | Option<**String**> |  | [optional]
**all_time_volume** | Option<**f64**> |  | [optional]
**floor_ask_price** | Option<**f64**> |  | [optional]
**opensea_verification_status** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


