# Model39

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **f64** |  | 
**order_id** | **String** |  | 
**orderbook** | **String** |  | 
**status** | **String** | Possible values: pending - Waiting to be submitted. posted - Successfully submitted to the marketplace. posted - Failed to be submitted to the marketplace (see statusReason for detail). | 
**status_reason** | **String** |  | 
**created_at** | **String** | Time when added to indexer | 
**updated_at** | **String** | Time when updated in indexer | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


