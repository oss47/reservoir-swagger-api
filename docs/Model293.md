# Model293

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | Option<[**crate::models::Model291**](Model291.md)> |  | [optional]
**ownership** | Option<[**crate::models::Model292**](Model292.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


