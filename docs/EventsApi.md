# \EventsApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_events_asks_v3**](EventsApi.md#get_events_asks_v3) | **GET** /events/asks/v3 | Asks status changes
[**get_events_bids_v3**](EventsApi.md#get_events_bids_v3) | **GET** /events/bids/v3 | Bid status changes
[**get_events_collections_floorask_v2**](EventsApi.md#get_events_collections_floorask_v2) | **GET** /events/collections/floor-ask/v2 | Collection floor changes
[**get_events_collections_topbid_v2**](EventsApi.md#get_events_collections_topbid_v2) | **GET** /events/collections/top-bid/v2 | Collection top bid changes
[**get_events_tokens_floorask_v4**](EventsApi.md#get_events_tokens_floorask_v4) | **GET** /events/tokens/floor-ask/v4 | Token price changes



## get_events_asks_v3

> models::GetAsksEventsV3Response get_events_asks_v3(contract, start_timestamp, end_timestamp, include_criteria_metadata, sort_direction, continuation, limit, normalize_royalties, display_currency)
Asks status changes

Every time an ask status changes, an event is generated. This API is designed to be polled at high frequency, in order to keep an external system in sync with accurate prices for any token.  There are multiple event types, which describe what caused the change in price:  - `new-order` > new listing at a lower price  - `expiry` > the previous best listing expired  - `sale` > the previous best listing was filled  - `cancel` > the previous best listing was canceled  - `balance-change` > the best listing was invalidated due to no longer owning the NFT  - `approval-change` > the best listing was invalidated due to revoked approval  - `revalidation` > manual revalidation of orders (e.g. after a bug fixed)  - `reprice` > price update for dynamic orders (e.g. dutch auctions)  - `bootstrap` > initial loading of data, so that all tokens have a price associated  Note: Private listings (asks) will not appear in the results.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to false]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max is 1000 |  |[default to 50]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetAsksEventsV3Response**](getAsksEventsV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_bids_v3

> models::GetBidEventsV3Response get_events_bids_v3(contract, start_timestamp, end_timestamp, include_criteria_metadata, sort_direction, continuation, limit, normalize_royalties, display_currency)
Bid status changes

Every time a bid status changes, an event is generated. This API is designed to be polled at high frequency, in order to keep an external system in sync with accurate prices for any token.  There are multiple event types, which describe what caused the change in price:  - `new-order` > new offer at a lower price  - `expiry` > the previous best offer expired  - `sale` > the previous best offer was filled  - `cancel` > the previous best offer was canceled  - `balance-change` > the best offer was invalidated due to no longer owning the NFT  - `approval-change` > the best offer was invalidated due to revoked approval  - `revalidation` > manual revalidation of orders (e.g. after a bug fixed)  - `reprice` > price update for dynamic orders (e.g. dutch auctions)  - `bootstrap` > initial loading of data, so that all tokens have a price associated  Some considerations to keep in mind  - Selling a partial quantity of available 1155 tokens in a listing will generate a `sale` and will have a new quantity.  - Due to the complex nature of monitoring off-chain liquidity across multiple marketplaces, including dealing with block re-orgs, events should be considered 'relative' to the perspective of the indexer, ie _when they were discovered_, rather than _when they happened_. A more deterministic historical record of price changes is in development, but in the meantime, this method is sufficent for keeping an external system in sync with the best available prices.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**include_criteria_metadata** | Option<**bool**> | If true, criteria metadata is included in the response. |  |[default to false]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 50]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetBidEventsV3Response**](getBidEventsV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_collections_floorask_v2

> models::GetCollectionsFloorAskV2Response get_events_collections_floorask_v2(collection, start_timestamp, end_timestamp, normalize_royalties, exclude_flagged_tokens, sort_direction, continuation, limit, display_currency)
Collection floor changes

Every time the floor price of a collection changes (i.e. the 'floor ask'), an event is generated. This API is designed to be polled at high frequency, in order to keep an external system in sync with accurate prices for any token.  There are multiple event types, which describe what caused the change in price:  - `new-order` > new listing at a lower price  - `expiry` > the previous best listing expired  - `sale` > the previous best listing was filled  - `cancel` > the previous best listing was cancelled  - `balance-change` > the best listing was invalidated due to no longer owning the NFT  - `approval-change` > the best listing was invalidated due to revoked approval  - `revalidation` > manual revalidation of orders (e.g. after a bug fixed)  - `reprice` > price update for dynamic orders (e.g. dutch auctions)  - `bootstrap` > initial loading of data, so that all tokens have a price associated  Some considerations to keep in mind  - Selling a partial quantity of available 1155 tokens in a listing will generate a `sale` and will have a new quantity.  - Due to the complex nature of monitoring off-chain liquidity across multiple marketplaces, including dealing with block re-orgs, events should be considered 'relative' to the perspective of the indexer, ie _when they were discovered_, rather than _when they happened_. A more deterministic historical record of price changes is in development, but in the meantime, this method is sufficent for keeping an external system in sync with the best available prices.  - Events are only generated if the best price changes. So if a new order or sale happens without changing the best price, no event is generated. This is more common with 1155 tokens, which have multiple owners and more depth. For this reason, if you need sales data, use the Sales API.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**exclude_flagged_tokens** | Option<**bool**> | If true, will exclude floor asks on flagged tokens. (only supported when `normalizeRoyalties` is false) |  |[default to false]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetCollectionsFloorAskV2Response**](getCollectionsFloorAskV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_collections_topbid_v2

> models::GetCollectionsTopbidV2Response get_events_collections_topbid_v2(collection, start_timestamp, end_timestamp, sort_direction, continuation, limit, display_currency)
Collection top bid changes

Every time the top offer of a collection changes (i.e. the 'top bid'), an event is generated. This API is designed to be polled at high frequency.  There are multiple event types, which describe what caused the change in price:  - `new-order` > new bid at a higher price  - `expiry` > the previous top bid expired  - `sale` > the previous top bid was accepted  - `cancel` > the previous top bid was cancelled  - `balance-change` > the top bid was invalidated due NFT no longer available  - `approval-change` > the top bid was invalidated due to revoked approval  - `revalidation` > manual revalidation of orders (e.g. after a bug fixed)  - `reprice` > price update for dynamic orders (e.g. dutch auctions)  - `bootstrap` > initial loading of data, so that all tokens have a price associated  Some considerations to keep in mind  - Selling a partial quantity of available 1155 tokens in a listing will generate a `sale` and will have a new quantity.  - Due to the complex nature of monitoring off-chain liquidity across multiple marketplaces, including dealing with block re-orgs, events should be considered 'relative' to the perspective of the indexer, ie _when they were discovered_, rather than _when they happened_. A more deterministic historical record of price changes is in development, but in the meantime, this method is sufficent for keeping an external system in sync with the best available prices.  - Events are only generated if the top bid changes. So if a new order or sale happens without changing the top bid, no event is generated. This is more common with 1155 tokens, which have multiple owners and more depth. For this reason, if you need sales data, use the Sales API.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetCollectionsTopbidV2Response**](getCollectionsTopbidV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_events_tokens_floorask_v4

> models::GetTokensFloorAskV4Response get_events_tokens_floorask_v4(contract, token, start_timestamp, end_timestamp, sort_direction, normalize_royalties, continuation, limit, display_currency)
Token price changes

Every time the best price of a token changes (i.e. the 'floor ask'), an event is generated. This API is designed to be polled at high frequency, in order to keep an external system in sync with accurate prices for any token.  There are multiple event types, which describe what caused the change in price:  - `new-order` > new listing at a lower price  - `expiry` > the previous best listing expired  - `sale` > the previous best listing was filled  - `cancel` > the previous best listing was cancelled  - `balance-change` > the best listing was invalidated due to no longer owning the NFT  - `approval-change` > the best listing was invalidated due to revoked approval  - `revalidation` > manual revalidation of orders (e.g. after a bug fixed)  - `reprice` > price update for dynamic orders (e.g. dutch auctions)  - `bootstrap` > initial loading of data, so that all tokens have a price associated  Some considerations to keep in mind  - Selling a partial quantity of available 1155 tokens in a listing will generate a `sale` and will have a new quantity.  - Due to the complex nature of monitoring off-chain liquidity across multiple marketplaces, including dealing with block re-orgs, events should be considered 'relative' to the perspective of the indexer, ie _when they were discovered_, rather than _when they happened_. A more deterministic historical record of price changes is in development, but in the meantime, this method is sufficent for keeping an external system in sync with the best available prices.  - Events are only generated if the best price changes. So if a new order or sale happens without changing the best price, no event is generated. This is more common with 1155 tokens, which have multiple owners and more depth. For this reason, if you need sales data, use the Sales API.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> |  |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**sort_direction** | Option<**String**> |  |  |[default to desc]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**continuation** | Option<**String**> |  |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 50]
**display_currency** | Option<**String**> | Return result in given currency |  |

### Return type

[**models::GetTokensFloorAskV4Response**](getTokensFloorAskV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

