# GetUserPositionsV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**positions** | Option<[**Vec<models::Model368>**](Model368.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


