# Model513

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**Vec<crate::models::Model510>**](Model510.md) | List of items to mint. | 
**taker** | **String** | Address of wallet minting (receiver of the NFT). | 
**relayer** | Option<**String**> | Address of wallet relaying the mint transaction(s) (paying for the NFT). | [optional]
**only_path** | Option<**bool**> | If true, only the path will be returned. | [optional][default to false]
**alternative_currencies** | Option<**Vec<String>**> | Alternative currencies to return the quote in. | [optional]
**currency_chain_id** | Option<**f32**> | The chain id of the purchase currency. | [optional]
**source** | Option<**String**> | Filling source used for attribution. Example: `reservoir.market` | [optional]
**fees_on_top** | Option<**Vec<String>**> | List of fees (formatted as `feeRecipient:feeAmount`) to be taken when minting. Unless overridden via the `currency` param, the currency used for any fees on top matches the buy-in currency detected by the backend. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:1000000000000000` | [optional]
**partial** | Option<**bool**> | If true, any off-chain or on-chain errors will be skipped. | [optional][default to false]
**skip_balance_check** | Option<**bool**> | If true, balance checks will be skipped. | [optional][default to false]
**referrer** | Option<**String**> | Referrer address (where supported). | [optional]
**comment** | Option<**String**> | Mint comment (where suported). | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


