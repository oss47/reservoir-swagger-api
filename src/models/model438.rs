/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model438 {
    #[serde(rename = "kind")]
    pub kind: Kind,
    #[serde(rename = "token")]
    pub token: String,
}

impl Model438 {
    pub fn new(kind: Kind, token: String) -> Model438 {
        Model438 { kind, token }
    }
}
///
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum Kind {
    #[serde(rename = "tokens-floor-sell")]
    FloorSell,
    #[serde(rename = "tokens-top-buy")]
    TopBuy,
}

impl Default for Kind {
    fn default() -> Kind {
        Self::FloorSell
    }
}
