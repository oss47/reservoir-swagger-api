/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct GetCollectionV2Response {
    #[serde(rename = "collection", skip_serializing_if = "Option::is_none")]
    pub collection: Option<Box<models::Model6>>,
}

impl GetCollectionV2Response {
    pub fn new() -> GetCollectionV2Response {
        GetCollectionV2Response { collection: None }
    }
}
