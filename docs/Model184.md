# Model184

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | Option<[**models::Model183**](Model183.md)> |  | [optional]
**time** | Option<[**models::Time**](time.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


