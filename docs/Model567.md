# Model567

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Returns `nft-approval` or `order-signature` | 
**kind** | **String** | Returns `request`, `signature`, or `transaction`. | 
**action** | **String** |  | 
**description** | **String** |  | 
**items** | [**Vec<models::Model565>**](Model565.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


