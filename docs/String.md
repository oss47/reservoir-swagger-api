# String

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_kind** | Option<**String**> |  | [optional]
**enabled** | Option<**bool**> |  | [optional]
**custom_fees_supported** | Option<**bool**> |  | [optional]
**num_fees_supported** | Option<**f64**> |  | [optional]
**minimum_bid_expiry** | Option<**f64**> |  | [optional]
**minimum_precision** | Option<**String**> |  | [optional]
**collection_bid_supported** | Option<**bool**> |  | [optional]
**trait_bid_supported** | Option<**bool**> |  | [optional]
**partial_order_supported** | Option<**bool**> | This indicates whether or not multi quantity bidding is supported | [optional]
**supported_bid_currencies** | Option<[**Vec<models::Model260>**](Model260.md)> | erc20 contract addresses | [optional]
**payment_tokens** | Option<[**Vec<models::Model260>**](Model260.md)> |  | [optional]
**max_price_raw** | Option<**String**> |  | [optional]
**min_price_raw** | Option<**String**> |  | [optional]
**oracle_enabled** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


