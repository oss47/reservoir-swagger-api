# Model576

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**quantity** | Option<**f64**> |  | [optional]
**source** | Option<**String**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**currency_symbol** | Option<**String**> |  | [optional]
**currency_decimals** | Option<**f64**> |  | [optional]
**quote** | Option<**f64**> |  | [optional]
**raw_quote** | Option<**String**> |  | [optional]
**buy_in_currency** | Option<**String**> |  | [optional]
**buy_in_currency_symbol** | Option<**String**> |  | [optional]
**buy_in_currency_decimals** | Option<**f64**> |  | [optional]
**buy_in_quote** | Option<**f64**> |  | [optional]
**buy_in_raw_quote** | Option<**String**> |  | [optional]
**total_price** | Option<**f64**> |  | [optional]
**total_raw_price** | Option<**String**> |  | [optional]
**fees_on_top** | Option<[**Vec<models::Model535>**](Model535.md)> | Can be referral fees. | [optional]
**gas_cost** | Option<**String**> |  | [optional]
**from_chain_id** | Option<**f64**> | Chain id buying from | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


