# GetSalesBulkV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales** | Option<[**Vec<models::Model205>**](Model205.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


