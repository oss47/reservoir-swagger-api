# Model462

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_key** | Option<**String**> | The api key to update | [optional]
**tier** | Option<**f64**> |  | [optional]
**active** | Option<**bool**> |  | [optional]
**ips** | Option<**Vec<String>**> |  | [optional]
**origins** | Option<**Vec<String>**> |  | [optional]
**permissions** | Option<[**models::Permissions**](permissions.md)> |  | [optional]
**rev_share_bps** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


