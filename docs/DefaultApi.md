# \DefaultApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**post_execute_bid_v5**](DefaultApi.md#post_execute_bid_v5) | **POST** /execute/bid/v5 | Create Bids
[**post_execute_buy_v7**](DefaultApi.md#post_execute_buy_v7) | **POST** /execute/buy/v7 | Buy Tokens
[**post_execute_cancel_v3**](DefaultApi.md#post_execute_cancel_v3) | **POST** /execute/cancel/v3 | Cancel Orders
[**post_execute_list_v5**](DefaultApi.md#post_execute_list_v5) | **POST** /execute/list/v5 | Create Listings
[**post_execute_mint_v1**](DefaultApi.md#post_execute_mint_v1) | **POST** /execute/mint/v1 | Mint Tokens
[**post_execute_sell_v7**](DefaultApi.md#post_execute_sell_v7) | **POST** /execute/sell/v7 | Sell Tokens
[**post_execute_transfer_v1**](DefaultApi.md#post_execute_transfer_v1) | **POST** /execute/transfer/v1 | Transfer Tokens



## post_execute_bid_v5

> models::GetExecuteBidV5Response post_execute_bid_v5(body)
Create Bids

Generate bids and submit them to multiple marketplaces.   Notes:  - Please use the `/cross-posting-orders/v1` to check the status on cross posted bids.  - We recommend using Reservoir SDK as it abstracts the process of iterating through steps, and returning callbacks that can be used to update your UI.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model503**](Model503.md)> |  |  |

### Return type

[**models::GetExecuteBidV5Response**](getExecuteBidV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_buy_v7

> models::GetExecuteBuyV7Response post_execute_buy_v7(body)
Buy Tokens

Use this API to fill listings. We recommend using the SDK over this API as the SDK will iterate through the steps and return callbacks. Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model528**](Model528.md)> |  |  |

### Return type

[**models::GetExecuteBuyV7Response**](getExecuteBuyV7Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_cancel_v3

> models::GetExecuteCancelV3Response post_execute_cancel_v3(body)
Cancel Orders

Cancel existing orders on any marketplace

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model548**](Model548.md)> |  |  |

### Return type

[**models::GetExecuteCancelV3Response**](getExecuteCancelV3Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_list_v5

> models::GetExecuteListV5Response post_execute_list_v5(body)
Create Listings

Generate listings and submit them to multiple marketplaces.   Notes:  - Please use the `/cross-posting-orders/v1` to check the status on cross posted bids.  - We recommend using Reservoir SDK as it abstracts the process of iterating through steps, and returning callbacks that can be used to update your UI.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model564**](Model564.md)> |  |  |

### Return type

[**models::GetExecuteListV5Response**](getExecuteListV5Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_mint_v1

> models::PostExecuteMintV1Response post_execute_mint_v1(body)
Mint Tokens

Use this API to mint tokens. We recommend using the SDK over this API as the SDK will iterate through the steps and return callbacks.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model575**](Model575.md)> |  |  |

### Return type

[**models::PostExecuteMintV1Response**](postExecuteMintV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_sell_v7

> models::GetExecuteSellV7Response post_execute_sell_v7(body)
Sell Tokens

Use this API to accept bids. We recommend using the SDK over this API as the SDK will iterate through the steps and return callbacks. Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model588**](Model588.md)> |  |  |

### Return type

[**models::GetExecuteSellV7Response**](getExecuteSellV7Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_transfer_v1

> models::PostExecuteTransferV1Response post_execute_transfer_v1(body)
Transfer Tokens

Use this endpoint to bulk transfer an array of NFTs.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model599**](Model599.md)> |  |  |

### Return type

[**models::PostExecuteTransferV1Response**](postExecuteTransferV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

