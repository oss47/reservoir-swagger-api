# Model426

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**floor_ask_price** | Option<[**models::Price**](price.md)> |  | [optional]
**last_sale_price** | Option<[**models::Price**](price.md)> |  | [optional]
**collection** | Option<[**models::Model425**](Model425.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


