# \RedirectsApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_redirect_collections_collection_image_v1**](RedirectsApi.md#get_redirect_collections_collection_image_v1) | **GET** /redirect/collections/{collection}/image/v1 | Redirect to the given collection image
[**get_redirect_currency_address_icon_v1**](RedirectsApi.md#get_redirect_currency_address_icon_v1) | **GET** /redirect/currency/{address}/icon/v1 | Redirect response to the given currency address icon
[**get_redirect_sources_source_logo_v2**](RedirectsApi.md#get_redirect_sources_source_logo_v2) | **GET** /redirect/sources/{source}/logo/v2 | Redirect response to the given source logo
[**get_redirect_sources_source_tokens_token_link_v2**](RedirectsApi.md#get_redirect_sources_source_tokens_token_link_v2) | **GET** /redirect/sources/{source}/tokens/{token}/link/v2 | Redirect response to the given source token page
[**get_redirect_tokens_token_image_v1**](RedirectsApi.md#get_redirect_tokens_token_image_v1) | **GET** /redirect/tokens/{token}/image/v1 | Redirect response to the given token image



## get_redirect_collections_collection_image_v1

> String get_redirect_collections_collection_image_v1(collection)
Redirect to the given collection image

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Redirect to the given collection image. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_redirect_currency_address_icon_v1

> String get_redirect_currency_address_icon_v1(address)
Redirect response to the given currency address icon

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**address** | **String** | Redirect to the given currency address icon. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_redirect_sources_source_logo_v2

> String get_redirect_sources_source_logo_v2(source)
Redirect response to the given source logo

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**source** | **String** | Domain of the source. Example `opensea.io` | [required] |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_redirect_sources_source_tokens_token_link_v2

> String get_redirect_sources_source_tokens_token_link_v2(source, token)
Redirect response to the given source token page

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**source** | **String** | Domain of the source. Example `opensea.io` | [required] |
**token** | **String** | Redirect to the given token page. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [required] |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_redirect_tokens_token_image_v1

> String get_redirect_tokens_token_image_v1(token, image_size)
Redirect response to the given token image

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | **String** | Redirect to the given token image. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | [required] |
**image_size** | Option<**String**> | Image size: 'small', 'medium', or 'large'.  |  |[default to medium]

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

