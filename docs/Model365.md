# Model365

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Collection Id | [optional]
**slug** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**banner** | Option<**String**> |  | [optional]
**discord_url** | Option<**String**> |  | [optional]
**external_url** | Option<**String**> |  | [optional]
**twitter_username** | Option<**String**> |  | [optional]
**twitter_url** | Option<**String**> |  | [optional]
**opensea_verification_status** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**metadata_disabled** | Option<**bool**> |  | [optional][default to false]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**token_count** | Option<**String**> | Total token count | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**primary_contract** | Option<**String**> |  | [optional]
**floor_ask_price** | Option<[**models::Model349**](Model349.md)> |  | [optional]
**top_bid_value** | Option<[**models::Model350**](Model350.md)> |  | [optional]
**top_bid_maker** | Option<**String**> |  | [optional]
**top_bid_source_domain** | Option<**String**> |  | [optional]
**rank** | Option<[**models::Model21**](Model21.md)> |  | [optional]
**volume** | Option<[**models::Volume**](volume.md)> |  | [optional]
**volume_change** | Option<[**models::Model28**](Model28.md)> |  | [optional]
**floor_sale** | Option<[**models::FloorSale**](floorSale.md)> |  | [optional]
**contract_kind** | Option<**String**> | Returns `erc721`, `erc1155`, etc. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


