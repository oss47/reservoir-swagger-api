/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model211 {
    #[serde(rename = "collectionId", skip_serializing_if = "Option::is_none")]
    pub collection_id: Option<String>,
    #[serde(rename = "contract", skip_serializing_if = "Option::is_none")]
    pub contract: Option<String>,
    #[serde(rename = "image", skip_serializing_if = "Option::is_none")]
    pub image: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "tokenCount", skip_serializing_if = "Option::is_none")]
    pub token_count: Option<String>,
    #[serde(rename = "allTimeVolume", skip_serializing_if = "Option::is_none")]
    pub all_time_volume: Option<f64>,
    #[serde(rename = "floorAskPrice", skip_serializing_if = "Option::is_none")]
    pub floor_ask_price: Option<f64>,
    #[serde(
        rename = "openseaVerificationStatus",
        skip_serializing_if = "Option::is_none"
    )]
    pub opensea_verification_status: Option<String>,
}

impl Model211 {
    pub fn new() -> Model211 {
        Model211 {
            collection_id: None,
            contract: None,
            image: None,
            name: None,
            token_count: None,
            all_time_volume: None,
            floor_ask_price: None,
            opensea_verification_status: None,
        }
    }
}
