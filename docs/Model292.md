# Model292

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | Option<[**models::Model285**](Model285.md)> |  | [optional]
**floor_ask** | Option<[**models::Model290**](Model290.md)> |  | [optional]
**event** | Option<[**models::Model291**](Model291.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


