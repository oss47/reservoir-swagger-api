# Model74

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | Option<**String**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**value** | **String** |  | 
**token_count** | Option<**f64**> |  | [optional]
**on_sale_count** | Option<**f64**> |  | [optional]
**floor_ask_price** | Option<**f64**> |  | [optional]
**top_bid_value** | Option<**f64**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


