/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model493 {
    /// Refresh the given collection. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63`
    #[serde(rename = "collection")]
    pub collection: String,
    /// If true, will force a refresh regardless of cool down. Requires an authorized api key to be passed.
    #[serde(rename = "overrideCoolDown", skip_serializing_if = "Option::is_none")]
    pub override_cool_down: Option<bool>,
    /// If true, will refresh the metadata for the tokens in the collection.
    #[serde(rename = "refreshTokens", skip_serializing_if = "Option::is_none")]
    pub refresh_tokens: Option<bool>,
}

impl Model493 {
    pub fn new(collection: String) -> Model493 {
        Model493 {
            collection,
            override_cool_down: None,
            refresh_tokens: None,
        }
    }
}
