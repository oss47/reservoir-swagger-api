# TopBid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**value** | Option<**f64**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**valid_from** | Option<**f64**> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


