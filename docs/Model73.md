# Model73

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**price** | Option<[**crate::models::Price**](price.md)> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**valid_from** | Option<**f32**> |  | [optional]
**valid_until** | Option<**f32**> |  | [optional]
**source** | Option<[**crate::models::Source**](source.md)> |  | [optional]
**fee_breakdown** | Option<[**Vec<crate::models::Model35>**](Model35.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


