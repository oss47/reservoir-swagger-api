# Model35

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage** | **String** |  | 
**token_id** | Option<**String**> |  | [optional]
**kind** | **String** |  | 
**standard** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**price_per_quantity** | Option<[**Vec<models::Model30>**](Model30.md)> |  | [optional]
**start_time** | Option<**f64**> |  | [optional]
**end_time** | Option<**f64**> |  | [optional]
**max_mints** | Option<**f64**> |  | [optional]
**max_mints_per_wallet** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


