# \TransfersApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_transfers_bulk_v2**](TransfersApi.md#get_transfers_bulk_v2) | **GET** /transfers/bulk/v2 | Bulk historical transfers
[**get_transfers_v4**](TransfersApi.md#get_transfers_v4) | **GET** /transfers/v4 | Historical token transfers



## get_transfers_bulk_v2

> models::GetTransfersBulkV1Response get_transfers_bulk_v2(contract, token, start_timestamp, end_timestamp, tx_hash, limit, sort_by, sort_direction, continuation)
Bulk historical transfers

Note: this API is optimized for bulk access, and offers minimal filters/metadata. If you need more flexibility, try the `NFT API > Transfers` endpoint

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**start_timestamp** | Option<**f64**> | Get events after a particular unix timestamp (inclusive) |  |
**end_timestamp** | Option<**f64**> | Get events before a particular unix timestamp (inclusive) |  |
**tx_hash** | Option<[**Vec<String>**](String.md)> |  |  |
**limit** | Option<**i32**> | Amount of items returned in response. Max limit is 1000. |  |[default to 100]
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `timestamp`, and `updatedAt`. Default is `timestamp`. |  |
**sort_direction** | Option<**String**> |  |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |

### Return type

[**models::GetTransfersBulkV1Response**](getTransfersBulkV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_transfers_v4

> models::GetTransfersV4Response get_transfers_v4(contract, token, collection, attributes, tx_hash, sort_by, sort_direction, limit, continuation, display_currency)
Historical token transfers

Get recent transfers for a contract or token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**contract** | Option<**String**> | Filter to a particular contract, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**collection** | Option<**String**> | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/transfers/v2?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/transfers/v2?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**tx_hash** | Option<**String**> | Filter to a particular transaction. Example: `0x04654cc4c81882ed4d20b958e0eeb107915d75730110cce65333221439de6afc` |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `timestamp`, and `updatedAt`. Default is `timestamp`. |  |
**sort_direction** | Option<**String**> |  |  |
**limit** | Option<**i32**> | Max limit is 100. |  |[default to 20]
**continuation** | Option<**String**> |  |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetTransfersV4Response**](getTransfersV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

