# Model301

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<[**models::Model204**](Model204.md)> |  | [optional]
**floor_ask** | Option<[**models::Model300**](Model300.md)> |  | [optional]
**event** | Option<[**models::Model287**](Model287.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


