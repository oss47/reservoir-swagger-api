# Model26

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Collection id | [optional]
**slug** | Option<**String**> | Open Sea slug | [optional]
**created_at** | Option<**String**> | Time when added to indexer | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**banner** | Option<**String**> |  | [optional]
**discord_url** | Option<**String**> |  | [optional]
**external_url** | Option<**String**> |  | [optional]
**twitter_username** | Option<**String**> |  | [optional]
**opensea_verification_status** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**token_count** | Option<**String**> | Total tokens within the collection. | [optional]
**on_sale_count** | Option<**String**> | Total tokens currently on sale. | [optional]
**primary_contract** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**royalties** | Option<[**models::Model17**](Model17.md)> |  | [optional]
**all_royalties** | Option<[**serde_json::Value**](.md)> |  | [optional]
**last_buy** | Option<[**models::LastBuy**](lastBuy.md)> |  | [optional]
**floor_ask** | Option<[**models::Model19**](Model19.md)> |  | [optional]
**top_bid** | Option<[**models::Model20**](Model20.md)> |  | [optional]
**rank** | Option<[**models::Model21**](Model21.md)> |  | [optional]
**volume** | Option<[**models::Volume**](volume.md)> |  | [optional]
**volume_change** | Option<[**models::Model22**](Model22.md)> |  | [optional]
**floor_sale** | Option<[**models::FloorSale**](floorSale.md)> |  | [optional]
**floor_sale_change** | Option<[**models::FloorSaleChange**](floorSaleChange.md)> |  | [optional]
**sales_count** | Option<[**models::SalesCount**](salesCount.md)> |  | [optional]
**collection_bid_supported** | Option<**bool**> | true or false | [optional]
**owner_count** | Option<**f64**> | Unique number of owners. | [optional]
**attributes** | Option<[**Vec<models::Model23>**](Model23.md)> |  | [optional]
**contract_kind** | Option<**String**> | Returns `erc721`, `erc1155`, etc. | [optional]
**minted_timestamp** | Option<**f64**> |  | [optional]
**mint_stages** | Option<[**Vec<models::Model25>**](Model25.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


