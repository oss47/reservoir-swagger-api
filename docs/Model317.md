# Model317

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**set** | Option<[**crate::models::Set**](set.md)> |  | [optional]
**primary_order** | Option<[**crate::models::PrimaryOrder**](primaryOrder.md)> |  | [optional]
**total_valid** | Option<**f32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


