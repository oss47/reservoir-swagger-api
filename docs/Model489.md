# Model489

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collections** | **Vec<String>** | Array of collection ids to disable metadata for. Max limit is 50. Example: `collections[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63 collections[1]: 0x521f9c7505005cfa19a8e5786a9c3c9c9f5e6f42` | 
**disable** | Option<**bool**> | Whether to disable or reenable the metadata. Defaults to true (disable) | [optional][default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


