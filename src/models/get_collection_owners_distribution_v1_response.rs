/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct GetCollectionOwnersDistributionV1Response {
    #[serde(rename = "ownersDistribution", skip_serializing_if = "Option::is_none")]
    pub owners_distribution: Option<Vec<models::Model268>>,
}

impl GetCollectionOwnersDistributionV1Response {
    pub fn new() -> GetCollectionOwnersDistributionV1Response {
        GetCollectionOwnersDistributionV1Response {
            owners_distribution: None,
        }
    }
}
