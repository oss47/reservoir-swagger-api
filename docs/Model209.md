# Model209

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> |  | [optional]
**from_address** | Option<**String**> |  | [optional]
**to_address** | Option<**String**> |  | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**amount** | Option<**f64**> |  | [optional]
**timestamp** | Option<**f64**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**token** | Option<[**models::Model207**](Model207.md)> |  | [optional]
**collection** | Option<[**models::Model208**](Model208.md)> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**log_index** | Option<**f64**> |  | [optional]
**batch_index** | Option<**f64**> |  | [optional]
**order** | Option<[**models::Model127**](Model127.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


