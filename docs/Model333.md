# Model333

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | Case sensitive | 
**value** | **String** | Case sensitive | 
**token_count** | **f32** | Total token count with this attribute. | 
**on_sale_count** | **f32** | Token count with this attribute on sale. | 
**sample_images** | Option<**Vec<String>**> |  | [optional]
**floor_ask_prices** | Option<**Vec<f32>**> | Current floor price ask. | [optional]
**last_buys** | Option<[**Vec<crate::models::Model331>**](Model331.md)> |  | [optional]
**last_sells** | Option<[**Vec<crate::models::Model331>**](Model331.md)> |  | [optional]
**top_bid** | Option<[**crate::models::TopBid**](topBid.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


