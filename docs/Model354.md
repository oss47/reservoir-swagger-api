# Model354

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | 
**value** | **String** |  | 
**token_count** | **f32** |  | 
**on_sale_count** | **f32** |  | 
**sample_images** | Option<**Vec<String>**> |  | [optional]
**floor_ask_prices** | Option<**Vec<f32>**> |  | [optional]
**last_buys** | Option<[**Vec<crate::models::Model331>**](Model331.md)> |  | [optional]
**last_sells** | Option<[**Vec<crate::models::Model331>**](Model331.md)> |  | [optional]
**top_bid** | Option<[**crate::models::TopBid**](topBid.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


