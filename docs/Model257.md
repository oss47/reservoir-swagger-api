# Model257

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | Option<[**crate::models::Model254**](Model254.md)> |  | [optional]
**floor_ask** | Option<[**crate::models::Model255**](Model255.md)> |  | [optional]
**event** | Option<[**crate::models::Model256**](Model256.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


