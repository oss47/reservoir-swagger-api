# \CollectionsApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_collections_collection_marketplaceconfigurations_v2**](CollectionsApi.md#get_collections_collection_marketplaceconfigurations_v2) | **GET** /collections/{collection}/marketplace-configurations/v2 | Marketplace configurations by collection
[**get_collections_collection_toptraders_v1**](CollectionsApi.md#get_collections_collection_toptraders_v1) | **GET** /collections/{collection}/top-traders/v1 | Top Traders
[**get_collections_search_v1**](CollectionsApi.md#get_collections_search_v1) | **GET** /collections/search/v1 | Search Collections (Cross Chain)
[**get_collections_trending_v1**](CollectionsApi.md#get_collections_trending_v1) | **GET** /collections/trending/v1 | Top Trending Collections
[**get_collections_trendingmints_v1**](CollectionsApi.md#get_collections_trendingmints_v1) | **GET** /collections/trending-mints/v1 | Top Trending Mints
[**get_collections_v7**](CollectionsApi.md#get_collections_v7) | **GET** /collections/v7 | Collections
[**get_search_collections_v2**](CollectionsApi.md#get_search_collections_v2) | **GET** /search/collections/v2 | Search Collections
[**get_users_user_collections_v4**](CollectionsApi.md#get_users_user_collections_v4) | **GET** /users/{user}/collections/v4 | User collections
[**post_collections_refresh_v2**](CollectionsApi.md#post_collections_refresh_v2) | **POST** /collections/refresh/v2 | Refresh Collection
[**post_collectionssets_v1**](CollectionsApi.md#post_collectionssets_v1) | **POST** /collections-sets/v1 | Create collection set
[**post_contractssets_v1**](CollectionsApi.md#post_contractssets_v1) | **POST** /contracts-sets/v1 | Create contracts set



## get_collections_collection_marketplaceconfigurations_v2

> models::Model263 get_collections_collection_marketplaceconfigurations_v2(collection, token_id)
Marketplace configurations by collection

This API returns recommended marketplace configurations given a collection id

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**token_id** | Option<**String**> | When set, token-level royalties will be returned in the response |  |

### Return type

[**models::Model263**](Model263.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_collection_toptraders_v1

> models::GetTopTradersV1Response get_collections_collection_toptraders_v1(collection, period, limit)
Top Traders

Get top traders for a particular collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | **String** | Filter to a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**period** | Option<**String**> | Time window to aggregate. |  |[default to 1d]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetTopTradersV1Response**](getTopTradersV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_search_v1

> models::GetCollectionSearchV1Response get_collections_search_v1(prefix, chains, community, exclude_spam, exclude_nsfw, limit)
Search Collections (Cross Chain)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**prefix** | **String** | Lightweight search for collections that match a string. Example: `bored` | [required] |
**chains** | Option<[**Vec<f64>**](f64.md)> | Array of chains. Max limit is 50. Example: `chains[0]: 1` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**exclude_spam** | Option<**bool**> | If true, will filter any collections marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any collections marked as nsfw. |  |[default to false]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetCollectionSearchV1Response**](getCollectionSearchV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_trending_v1

> models::GetTrendingCollectionsV1Response get_collections_trending_v1(period, limit, sort_by, normalize_royalties, use_non_flagged_floor_ask)
Top Trending Collections

Get trending selling/minting collections

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**period** | Option<**String**> | Time window to aggregate. |  |[default to 1d]
**limit** | Option<**i32**> | Amount of items returned in response. Default is 50 and max is 1000. Expected to be sorted and filtered on client side. |  |[default to 50]
**sort_by** | Option<**String**> |  |  |[default to sales]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, return the non flagged floor ask. Supported only when `normalizeRoyalties` is false. |  |[default to false]

### Return type

[**models::GetTrendingCollectionsV1Response**](getTrendingCollectionsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_trendingmints_v1

> models::GetTrendingMintsV1Response get_collections_trendingmints_v1(period, r#type, limit, normalize_royalties, use_non_flagged_floor_ask)
Top Trending Mints

Get top trending mints

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**period** | Option<**String**> | Time window to aggregate. |  |[default to 24h]
**r#type** | Option<**String**> | The type of the mint (free/paid). |  |[default to any]
**limit** | Option<**i32**> | Amount of items returned in response. Default is 50 and max is 50. Expected to be sorted and filtered on client side. |  |[default to 50]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, return the non flagged floor ask. Supported only when `normalizeRoyalties` is false. |  |[default to false]

### Return type

[**models::GetTrendingMintsV1Response**](get-trending-mintsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_collections_v7

> models::GetCollectionsV7Response get_collections_v7(id, slug, collections_set_id, community, contract, creator, name, max_floor_ask_price, min_floor_ask_price, include_attributes, include_sales_count, include_mint_stages, include_security_configs, normalize_royalties, use_non_flagged_floor_ask, sort_by, sort_direction, limit, exclude_spam, exclude_nsfw, start_timestamp, end_timestamp, continuation, display_currency)
Collections

Use this API to explore a collection's metadata and statistics (sales, volume, etc).

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> | Filter to a particular collection with collection id. The id is immutable.  For single contract collections, the id is the contract address e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63`  Artblocks will follow this format with a token range: `{artblocksContractAddress}:{startTokenId}:{endTokenId}`  Shared contracts will generally follow the format `{contract}:{namespace}-{id}`; The following shared contracts will follow this format:  OpenSea, Sound, SuperRare, Foundation, Ordinals, & Courtyard |  |
**slug** | Option<**String**> | Filter to a particular collection slug. We recommend to rely on collection id; slugs are dynamic and can change without warning. Example: `boredapeyachtclub` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**contract** | Option<[**Vec<String>**](String.md)> |  |  |
**creator** | Option<**String**> | Filter by creator |  |
**name** | Option<**String**> | Search for collections that match a string. Example: `bored` |  |
**max_floor_ask_price** | Option<**f64**> | Maximum floor price of the collection |  |
**min_floor_ask_price** | Option<**f64**> | Minumum floor price of the collection |  |
**include_attributes** | Option<**bool**> | If true, attributes will be included in the response. Must filter by `id` or `slug` to a particular collection. |  |
**include_sales_count** | Option<**bool**> | If true, sales count (1 day, 7 day, 30 day, all time) will be included in the response. Must filter by `id` or `slug` to a particular collection. |  |
**include_mint_stages** | Option<**bool**> | If true, mint data for the collection will be included in the response. |  |[default to false]
**include_security_configs** | Option<**bool**> | If true, security configuration data (e.g. ERC721C or Operator Filter Registry configuration) will be included in the response. |  |[default to false]
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**use_non_flagged_floor_ask** | Option<**bool**> | If true, return the non flagged floor ask. Supported only when `normalizeRoyalties` is false. |  |[default to false]
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `#DayVolume`, `createdAt`, `updatedAt`, or `floorAskPrice` |  |[default to allTimeVolume]
**sort_direction** | Option<**String**> |  |  |
**limit** | Option<**i32**> | Amount of items returned in response. Default and max limit is 20, unless sorting by `updatedAt` which has a max limit of 1000. |  |[default to 20]
**exclude_spam** | Option<**bool**> | If true, will filter any collections marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any collections marked as nsfw. |  |[default to false]
**start_timestamp** | Option<**f64**> | When sorting by `updatedAt`, the start timestamp you want to filter on (UTC). |  |
**end_timestamp** | Option<**f64**> | When sorting by `updatedAt`, the end timestamp you want to filter on (UTC). |  |
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |

### Return type

[**models::GetCollectionsV7Response**](getCollectionsV7Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_search_collections_v2

> models::GetSearchCollectionsV2Response get_search_collections_v2(name, community, display_currency, collections_set_id, exclude_spam, exclude_nsfw, offset, limit)
Search Collections

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**name** | Option<**String**> | Lightweight search for collections that match a string. Can also search using contract address. Example: `bored` or `0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d` |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**display_currency** | Option<**String**> | Return result in given currency |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set |  |
**exclude_spam** | Option<**bool**> | If true, will filter any collections marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any collections marked as nsfw. |  |[default to false]
**offset** | Option<**i32**> | Use offset to request the next batch of items. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. |  |[default to 20]

### Return type

[**models::GetSearchCollectionsV2Response**](getSearchCollectionsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users_user_collections_v4

> models::GetUserCollectionsV4Response get_users_user_collections_v4(user, community, collections_set_id, collection, exclude_collections, name, include_top_bid, include_liquid_count, include_on_sale_count, exclude_spam, exclude_nsfw, offset, limit, display_currency, sort_by, sort_direction)
User collections

Get aggregate stats for a user, grouped by collection. Useful for showing total portfolio information.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user** | **String** | Filter to a particular user. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [required] |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. Example: `8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65` |  |
**collection** | Option<[**Vec<String>**](String.md)> | Array of collections. Max limit is 100. Example: `collections[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**exclude_collections** | Option<[**Vec<String>**](String.md)> |  |  |
**name** | Option<**String**> | Filter to a particular collection with name. This is case insensitive. Example: `ape` |  |
**include_top_bid** | Option<**bool**> | If true, top bid will be returned in the response. |  |[default to false]
**include_liquid_count** | Option<**bool**> | If true, number of tokens with bids will be returned in the response. |  |[default to false]
**include_on_sale_count** | Option<**bool**> | If true, number of listed tokens will be returned in the response. |  |[default to false]
**exclude_spam** | Option<**bool**> | If true, will filter any collections marked as spam. |  |[default to false]
**exclude_nsfw** | Option<**bool**> | If true, will filter any collections marked as spam. |  |[default to false]
**offset** | Option<**i32**> | Use offset to request the next batch of items. Max is 10,000. |  |[default to 0]
**limit** | Option<**i32**> | Amount of items returned in response. max limit is 100. |  |[default to 20]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency. Applies to `topBid` and `floorAsk`. |  |
**sort_by** | Option<**String**> | Order the items are returned in the response. Options are `allTimeVolume`, `totalValue`, `floorAskPrice` |  |[default to allTimeVolume]
**sort_direction** | Option<**String**> | Order the items are returned in the response. |  |[default to desc]

### Return type

[**models::GetUserCollectionsV4Response**](getUserCollectionsV4Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_collections_refresh_v2

> models::PutSetCollectionCommunityV1Response post_collections_refresh_v2(x_api_key, body)
Refresh Collection

Use this API to refresh a collection metadata. Only use this endpoint when you notice multiple tokens with incorrect metadata. Otherwise, refresh single token metadata. Collections with over 30,000 tokens require admin key override, so please contact technical support for assistance.   Collection metadata is automatically updated at 23:30 UTC daily for:  - Top 500 Collection by 24hr Volume  - Collections Minted 1 Day Ago  - Collections Minted 7 Days Ago   Caution: This API should be used in moderation, like only when missing data is discovered. Calling it in bulk or programmatically will result in your API key getting rate limited.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | Option<**String**> |  |  |
**body** | Option<[**Model493**](Model493.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_collectionssets_v1

> models::PostCreateCollectionsSetV1Response post_collectionssets_v1(body)
Create collection set

Array of collections to gather in a set. Adding or removing a collection will change the response. You may use this set when `collectionSetId` is an available param. The max limit of collection in an array is 500. An example is below.  `\"collections\": \"0xba30E5F9Bb24caa003E9f2f0497Ad287FDF95623\", \"0xBC4CA0EdA7647A8aB7C2061c2E118A18a936f13D\"`  `\"collectionsSetId\": \"8daa732ebe5db23f267e58d52f1c9b1879279bcdf4f78b8fb563390e6946ea65\"`

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model467**](Model467.md)> |  |  |

### Return type

[**models::PostCreateCollectionsSetV1Response**](postCreateCollectionsSetV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_contractssets_v1

> models::PostCreateContractsSetV1Response post_contractssets_v1(body)
Create contracts set

Array of contracts to gather in a set. Adding or removing a contract will change the response. You may use this set when contractSetId is an available param. Max limit of contracts passed in an array is 500. An example is below.  `\"contracts\": \"0x60e4d786628fea6478f785a6d7e704777c86a7c6\", \"0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d\"`  `\"contractsSetId\": \"74cc9bdc0824e92de13c75213015916557fcf8187e43b34a8e77175cd03d1931\"

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model468**](Model468.md)> |  |  |

### Return type

[**models::PostCreateContractsSetV1Response**](postCreateContractsSetV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

