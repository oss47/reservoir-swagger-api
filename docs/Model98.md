# Model98

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<[**models::Model97**](Model97.md)> |  | [optional]
**market** | Option<[**models::Model92**](Model92.md)> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


