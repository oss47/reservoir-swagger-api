# Model144

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**status** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**price** | Option<**f32**> |  | [optional]
**quantity_remaining** | Option<**f32**> |  | [optional]
**nonce** | Option<**String**> |  | [optional]
**valid_from** | Option<**f32**> |  | [optional]
**valid_until** | Option<**f32**> |  | [optional]
**source** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


