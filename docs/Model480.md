# Model480

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_ids** | **Vec<String>** | Ids of the orders to cancel | 
**order_kind** | **String** | Exchange protocol used to bulk cancel order. Example: `seaport-v1.5` | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


