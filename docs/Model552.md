# Model552

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**image_url** | Option<**String**> |  | [optional]
**twitter_url** | Option<**String**> |  | [optional]
**discord_url** | Option<**String**> |  | [optional]
**external_url** | Option<**String**> |  | [optional]
**royalties** | Option<[**Vec<crate::models::Model550>**](Model550.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


