# Model68

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | **String** |  | 
**token_id** | **String** |  | 
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**collection** | Option<[**models::Model49**](Model49.md)> |  | [optional]
**top_bid_value** | Option<**f64**> |  | [optional]
**floor_ask_price** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


