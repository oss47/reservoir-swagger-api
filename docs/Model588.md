# Model588

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**Vec<models::Model585>**](Model585.md) | List of items to sell. | 
**taker** | **String** | Address of wallet filling. | 
**source** | Option<**String**> | Filling source used for attribution. | [optional]
**fees_on_top** | Option<**Vec<String>**> | List of fees (formatted as `feeRecipient:feeAmount`) to be taken when filling. The currency used for any fees on top is always the wrapped native currency of the chain. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:1000000000000000` | [optional]
**only_path** | Option<**bool**> | If true, only the filling path will be returned. | [optional][default to false]
**normalize_royalties** | Option<**bool**> | Charge any missing royalties. | [optional][default to false]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. | [optional][default to false]
**allow_inactive_order_ids** | Option<**bool**> | If true, inactive orders will not be skipped over (only relevant when filling via a specific order id). | [optional][default to false]
**partial** | Option<**bool**> | If true, any off-chain or on-chain errors will be skipped. | [optional][default to false]
**force_router** | Option<**bool**> | If true, filling will be forced to use the common 'approval + transfer' method instead of the approval-less 'on-received hook' method | [optional][default to false]
**forwarder_channel** | Option<**String**> | If passed, all fills will be executed through the trusted trusted forwarder (where possible) | [optional]
**currency** | Option<**String**> | Currency to be received when selling. | [optional]
**max_fee_per_gas** | Option<**String**> | Optional custom gas settings. Includes base fee & priority fee in this limit. | [optional]
**max_priority_fee_per_gas** | Option<**String**> | Optional custom gas settings. | [optional]
**x2y2_api_key** | Option<**String**> | Optional X2Y2 API key used for filling. | [optional]
**opensea_api_key** | Option<**String**> | Optional OpenSea API key used for filling. You don't need to pass your own key, but if you don't, you are more likely to be rate-limited. | [optional]
**blur_auth** | Option<**String**> | Optional Blur auth used for filling | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


