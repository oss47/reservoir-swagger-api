# Model455

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | **String** | Update community for a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | 
**community** | **String** |  | 
**do_retries** | Option<**bool**> |  | [optional][default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


