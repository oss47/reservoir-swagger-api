# \FillOrdersBuySellApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**post_execute_buy_v7**](FillOrdersBuySellApi.md#post_execute_buy_v7) | **POST** /execute/buy/v7 | Buy tokens (fill listings)
[**post_execute_sell_v7**](FillOrdersBuySellApi.md#post_execute_sell_v7) | **POST** /execute/sell/v7 | Sell tokens (accept bids)



## post_execute_buy_v7

> crate::models::GetExecuteBuyV7Response post_execute_buy_v7(body)
Buy tokens (fill listings)

Use this API to fill listings. We recommend using the SDK over this API as the SDK will iterate through the steps and return callbacks. Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model476**](Model476.md)> |  |  |

### Return type

[**crate::models::GetExecuteBuyV7Response**](getExecuteBuyV7Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_sell_v7

> crate::models::GetExecuteSellV7Response post_execute_sell_v7(body)
Sell tokens (accept bids)

Use this API to accept bids. We recommend using the SDK over this API as the SDK will iterate through the steps and return callbacks. Please mark `excludeEOA` as `true` to exclude Blur orders.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model526**](Model526.md)> |  |  |

### Return type

[**crate::models::GetExecuteSellV7Response**](getExecuteSellV7Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

