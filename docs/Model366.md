# Model366

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | Option<[**models::Model365**](Model365.md)> |  | [optional]
**ownership** | Option<[**models::Model356**](Model356.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


