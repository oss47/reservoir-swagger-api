# FloorSale

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**param_1day** | Option<**f64**> |  | [optional]
**param_7day** | Option<**f64**> |  | [optional]
**param_30day** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


