# GetUserActivityV6Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**continuation** | Option<**String**> |  | [optional]
**activities** | Option<[**Vec<models::Model256>**](Model256.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


