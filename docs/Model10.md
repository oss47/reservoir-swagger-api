# Model10

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**slug** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**banner** | Option<**String**> |  | [optional]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**token_count** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**primary_contract** | Option<**String**> |  | [optional]
**floor_ask_price** | Option<**f64**> |  | [optional]
**top_bid_value** | Option<**f64**> |  | [optional]
**top_bid_maker** | Option<**String**> |  | [optional]
**param_1day_volume** | Option<**f64**> |  | [optional]
**param_7day_volume** | Option<**f64**> |  | [optional]
**param_30day_volume** | Option<**f64**> |  | [optional]
**all_time_volume** | Option<**f64**> |  | [optional]
**all_time_rank** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


