# Model611

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokens** | **Vec<String>** | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` | 
**nsfw** | Option<**bool**> | The new status of the nsfw | [optional][default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


