# Model271

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | 
**value** | **String** |  | 
**token_count** | **f64** |  | 
**sample_images** | Option<**Vec<String>**> |  | [optional]
**last_buys** | Option<[**Vec<models::Model270>**](Model270.md)> |  | [optional]
**last_sells** | Option<[**Vec<models::Model270>**](Model270.md)> |  | [optional]
**floor_ask_prices** | Option<**Vec<f64>**> |  | [optional]
**top_bid** | Option<[**models::TopBid**](topBid.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


