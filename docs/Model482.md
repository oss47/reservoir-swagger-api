# Model482

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**protocol_data** | Option<[**models::ProtocolData**](protocol_data.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


