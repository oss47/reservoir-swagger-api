# Model280

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | Option<**String**> |  | [optional]
**domain** | Option<**String**> |  | [optional]
**image_url** | Option<**String**> |  | [optional]
**fee** | Option<[**models::Model258**](Model258.md)> |  | [optional]
**royalties** | Option<[**models::Model259**](Model259.md)> |  | [optional]
**orderbook** | Option<**String**> |  | [optional]
**exchanges** | Option<[**models::Model280Exchanges**](Model280_exchanges.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


