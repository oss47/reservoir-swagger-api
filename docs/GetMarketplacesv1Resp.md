# GetMarketplacesv1Resp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplaces** | Option<[**Vec<models::Model1>**](Model1.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


