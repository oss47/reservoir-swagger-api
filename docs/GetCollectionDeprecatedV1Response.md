# GetCollectionDeprecatedV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | Option<[**models::Model146**](Model146.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


