# Model456

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**method** | **String** |  | 
**collection** | **String** | Collection to update. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


