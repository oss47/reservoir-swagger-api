# Model174

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**kind** | **String** |  | 
**side** | **String** |  | 
**status** | Option<**String**> |  | [optional]
**token_set_id** | **String** |  | 
**token_set_schema_hash** | **String** |  | 
**contract** | Option<**String**> |  | [optional]
**maker** | **String** |  | 
**taker** | **String** |  | 
**price** | Option<[**crate::models::Price**](price.md)> |  | [optional]
**valid_from** | **f32** |  | 
**valid_until** | **f32** |  | 
**quantity_filled** | Option<**f32**> |  | [optional]
**quantity_remaining** | Option<**f32**> |  | [optional]
**metadata** | Option<[**crate::models::Model157**](Model157.md)> |  | [optional]
**source** | Option<[**serde_json::Value**](.md)> |  | [optional]
**fee_bps** | Option<**f32**> |  | [optional]
**fee_breakdown** | Option<[**Vec<crate::models::Model158>**](Model158.md)> |  | [optional]
**expiration** | **f32** |  | 
**is_reservoir** | Option<**bool**> |  | [optional]
**created_at** | **String** |  | 
**updated_at** | **String** |  | 
**raw_data** | Option<[**serde_json::Value**](.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


