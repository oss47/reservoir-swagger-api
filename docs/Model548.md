# Model548

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_ids** | Option<**Vec<String>**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**order_kind** | Option<**String**> |  | [optional]
**token** | Option<**String**> |  | [optional]
**blur_auth** | Option<**String**> |  | [optional]
**max_fee_per_gas** | Option<**String**> | Optional. Set custom gas price | [optional]
**max_priority_fee_per_gas** | Option<**String**> | Optional. Set custom gas price | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


