# Model143

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**kind** | **String** |  | 
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**start_time** | Option<**f64**> |  | [optional]
**end_time** | Option<**f64**> |  | [optional]
**max_mints_per_wallet** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


