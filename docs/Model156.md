# Model156

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid** | Option<[**models::Model155**](Model155.md)> |  | [optional]
**event** | Option<[**models::Event**](event.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


