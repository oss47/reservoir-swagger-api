# GetExecuteBuyV5Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**steps** | Option<[**Vec<models::Model514>**](Model514.md)> |  | [optional]
**path** | Option<[**Vec<models::Model516>**](Model516.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


