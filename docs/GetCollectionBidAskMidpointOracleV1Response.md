# GetCollectionBidAskMidpointOracleV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | **f64** |  | 
**message** | Option<[**models::Message**](message.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


