# Model427

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**price** | Option<[**models::Model124**](Model124.md)> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> | Time when added to indexer | [optional]
**valid_from** | Option<**f64**> |  | [optional]
**valid_until** | Option<**f64**> |  | [optional]
**floor_difference_percentage** | Option<**f64**> | Percentage difference between this bid and the current floor price. | [optional]
**source** | Option<[**models::Source**](source.md)> |  | [optional]
**fee_breakdown** | Option<[**Vec<models::Model186>**](Model186.md)> |  | [optional]
**criteria** | Option<[**models::Model120**](Model120.md)> |  | [optional]
**token** | Option<[**models::Model426**](Model426.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


