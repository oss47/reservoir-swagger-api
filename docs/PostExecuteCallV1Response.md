# PostExecuteCallV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**steps** | Option<[**Vec<models::Model545>**](Model545.md)> |  | [optional]
**fees** | Option<[**models::Model540**](Model540.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


