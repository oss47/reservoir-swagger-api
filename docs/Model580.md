# Model580

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **String** | Request id of the associate execute API request | 
**step_id** | **String** | Step id of the relevant execute item | 
**tx_hash** | Option<**String**> | Associated transaction hash | [optional]
**error_message** | Option<**String**> | Associated error message | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


