# Model368

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**set** | Option<[**models::Set**](set.md)> |  | [optional]
**primary_order** | Option<[**models::PrimaryOrder**](primaryOrder.md)> |  | [optional]
**total_valid** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


