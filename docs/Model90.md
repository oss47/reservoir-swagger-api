# Model90

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**floor_ask** | Option<[**crate::models::Model89**](Model89.md)> |  | [optional]
**top_bid** | Option<[**crate::models::Model85**](Model85.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


