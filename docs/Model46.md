# Model46

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_count** | Option<**String**> |  | [optional]
**on_sale_count** | Option<**String**> |  | [optional]
**floor_ask_price** | Option<[**models::FloorAskPrice**](floorAskPrice.md)> |  | [optional]
**top_bid_value** | Option<[**models::TopBidValue**](topBidValue.md)> |  | [optional]
**total_bid_value** | Option<[**models::TopBidValue**](topBidValue.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


