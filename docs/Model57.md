# Model57

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**price** | Option<**f32**> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**valid_from** | Option<**f32**> |  | [optional]
**valid_until** | Option<**f32**> |  | [optional]
**token** | Option<[**crate::models::Model56**](Model56.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


