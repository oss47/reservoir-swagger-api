# Model528

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**Vec<models::Model526>**](Model526.md) | List of items to buy. | 
**taker** | **String** | Address of wallet filling (receiver of the NFT). | 
**relayer** | Option<**String**> | Address of wallet relaying the fill transaction (paying for the NFT). | [optional]
**only_path** | Option<**bool**> | If true, only the path will be returned. | [optional][default to false]
**force_router** | Option<**bool**> | If true, all fills will be executed through the router (where possible) | [optional]
**forwarder_channel** | Option<**String**> | If passed, all fills will be executed through the trusted trusted forwarder (where possible) | [optional]
**currency** | Option<**String**> | Currency to be used for purchases. | [optional]
**currency_chain_id** | Option<**f64**> | The chain id of the purchase currency | [optional]
**normalize_royalties** | Option<**bool**> | Charge any missing royalties. | [optional][default to false]
**allow_inactive_order_ids** | Option<**bool**> | If true, inactive orders will not be skipped over (only relevant when filling via a specific order id). | [optional][default to false]
**source** | Option<**String**> | Filling source used for attribution. Example: `reservoir.market` | [optional]
**fees_on_top** | Option<**Vec<String>**> | List of fees (formatted as `feeRecipient:feeAmount`) to be taken when filling. Unless overridden via the `currency` param, the currency used for any fees on top matches the buy-in currency detected by the backend. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:1000000000000000` | [optional]
**partial** | Option<**bool**> | If true, any off-chain or on-chain errors will be skipped. | [optional][default to false]
**skip_balance_check** | Option<**bool**> | If true, balance check will be skipped. | [optional][default to false]
**exclude_eoa** | Option<**bool**> | Exclude orders that can only be filled by EOAs, to support filling with smart contracts. If marked `true`, blur will be excluded. | [optional][default to false]
**max_fee_per_gas** | Option<**String**> | Optional custom gas settings. Includes base fee & priority fee in this limit. | [optional]
**max_priority_fee_per_gas** | Option<**String**> | Optional custom gas settings. | [optional]
**use_permit** | Option<**bool**> | When true, will use permit to avoid approvals. | [optional]
**swap_provider** | Option<**String**> | Choose a specific swapping provider when buying in a different currency (defaults to `uniswap`) | [optional][default to Uniswap]
**execution_method** | Option<**String**> |  | [optional]
**referrer** | Option<**String**> | Referrer address (where supported) | [optional]
**comment** | Option<**String**> | Mint comment (where suported) | [optional]
**conduit_key** | Option<**String**> | Conduit key to use to fulfill the order | [optional]
**x2y2_api_key** | Option<**String**> | Optional X2Y2 API key used for filling. | [optional]
**opensea_api_key** | Option<**String**> | Optional OpenSea API key used for filling. You don't need to pass your own key, but if you don't, you are more likely to be rate-limited. | [optional]
**blur_auth** | Option<**String**> | Advanced use case to pass personal blurAuthToken; the API will generate one if left empty. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


