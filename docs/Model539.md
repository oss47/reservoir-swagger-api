# Model539

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_index** | **f64** |  | 
**max_quantity** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


