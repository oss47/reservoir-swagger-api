# Model487

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time** | Option<**String**> |  | [optional]
**api_calls_count** | Option<**f64**> |  | [optional]
**points_consumed** | Option<**f64**> |  | [optional]
**key** | Option<**String**> |  | [optional]
**route** | Option<**String**> |  | [optional]
**status_code** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


