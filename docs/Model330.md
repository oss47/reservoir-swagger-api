# Model330

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**slug** | Option<**String**> | Open Sea slug | [optional]
**symbol** | Option<**String**> |  | [optional]
**image_url** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**is_nsfw** | Option<**bool**> |  | [optional][default to false]
**metadata_disabled** | Option<**bool**> |  | [optional][default to false]
**opensea_verification_status** | Option<**String**> |  | [optional]
**floor_ask_price** | Option<[**models::Model82**](Model82.md)> |  | [optional]
**royalties_bps** | Option<**f64**> |  | [optional]
**royalties** | Option<[**Vec<models::Model319>**](Model319.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


