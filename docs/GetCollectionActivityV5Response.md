# GetCollectionActivityV5Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**es** | Option<**bool**> |  | [optional][default to false]
**continuation** | Option<**String**> |  | [optional]
**activities** | Option<[**Vec<models::Model122>**](Model122.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


