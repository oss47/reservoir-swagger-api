# PostExecuteSolveCapacityV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capacity_per_request** | **String** |  | 
**total_capacity** | **String** |  | 
**user_balance** | Option<[**models::UserBalance**](userBalance.md)> |  | [optional]
**max_items** | **f64** |  | 
**max_price_per_item** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


