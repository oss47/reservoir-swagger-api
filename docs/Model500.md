# Model500

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` | 
**quantity** | Option<**f32**> | Quantity of tokens user is listing. Only compatible with ERC1155 tokens. Example: `5` | [optional]
**wei_price** | **String** | Amount seller is willing to sell for in the smallest denomination for the specific currency. Example: `1000000000000000000` | 
**end_wei_price** | Option<**String**> | Amount seller is willing to sell for Dutch auction in the largest denomination for the specific currency. Example: `2000000000000000000` | [optional]
**order_kind** | Option<**String**> | Exchange protocol used to create order. Example: `seaport-v1.5` | [optional][default to SeaportV1Period5]
**options** | Option<[**crate::models::Model499**](Model499.md)> |  | [optional]
**orderbook** | Option<**String**> | Orderbook where order is placed. Example: `Reservoir` | [optional][default to Reservoir]
**orderbook_api_key** | Option<**String**> | Optional API key for the target orderbook | [optional]
**automated_royalties** | Option<**bool**> | If true, royalty amounts and recipients will be set automatically. | [optional][default to true]
**royalty_bps** | Option<**f32**> | Set a maximum amount of royalties to pay, rather than the full amount. Only relevant when using automated royalties. 1 BPS = 0.01% Note: OpenSea does not support values below 50 bps. | [optional]
**fees** | Option<**Vec<String>**> | Deprecated, use `marketplaceFees` and/or `customRoyalties` | [optional]
**marketplace_fees** | Option<**Vec<String>**> | List of marketplace fees (formatted as `feeRecipient:feeBps`) to be bundled within the order. 1 BPS = 0.01% Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:100` | [optional]
**custom_royalties** | Option<**Vec<String>**> | List of custom royalties (formatted as `feeRecipient:feeBps`) to be bundled within the order. 1 BPS = 0.01% Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00:100` | [optional]
**listing_time** | Option<**String**> | Unix timestamp (seconds) indicating when listing will be listed. Example: `1656080318` | [optional]
**expiration_time** | Option<**String**> | Unix timestamp (seconds) indicating when listing will expire. Example: `1656080318` | [optional]
**salt** | Option<**String**> | Optional. Random string to make the order unique | [optional]
**nonce** | Option<**String**> | Optional. Set a custom nonce | [optional]
**currency** | Option<**String**> |  | [optional][default to 0x0000000000000000000000000000000000000000]
**taker** | Option<**String**> | Address of wallet taking the private order. Example: `0xF296178d553C8Ec21A2fBD2c5dDa8CA9ac905A00` | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


