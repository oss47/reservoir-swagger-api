# GetCollectionV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | Option<[**models::Model6**](Model6.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


