# Model92

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**floor_ask** | Option<[**models::Model88**](Model88.md)> |  | [optional]
**top_bid** | Option<[**models::Model91**](Model91.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


