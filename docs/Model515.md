# Model515

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Returns `nft-approval` or `transfer` | 
**kind** | **String** | Returns `transaction` | 
**action** | **String** |  | 
**description** | **String** |  | 
**items** | [**Vec<crate::models::Model513>**](Model513.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


