# Model242

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_count** | Option<**f32**> |  | [optional]
**owner_count** | Option<**f32**> | The amount of owners with the same `tokenCount`. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


