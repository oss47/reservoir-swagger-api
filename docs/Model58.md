# Model58

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Deprecated. Use `saleId` instead. | [optional]
**sale_id** | Option<**String**> | Unique identifier made from txn hash, price, etc. | [optional]
**token** | Option<[**models::Model50**](Model50.md)> |  | [optional]
**order_source** | Option<**String**> |  | [optional]
**order_side** | Option<**String**> | Can be `ask` or `bid`. | [optional]
**order_kind** | Option<**String**> |  | [optional]
**order_id** | Option<**String**> |  | [optional]
**from** | Option<**String**> |  | [optional]
**to** | Option<**String**> |  | [optional]
**amount** | Option<**String**> |  | [optional]
**fill_source** | Option<**String**> |  | [optional]
**block** | Option<**f64**> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**log_index** | Option<**f64**> |  | [optional]
**batch_index** | Option<**f64**> |  | [optional]
**timestamp** | Option<**f64**> | Time added on the blockchain | [optional]
**price** | Option<[**models::Price**](price.md)> |  | [optional]
**wash_trading_score** | Option<**f64**> |  | [optional]
**royalty_fee_bps** | Option<**f64**> |  | [optional]
**marketplace_fee_bps** | Option<**f64**> |  | [optional]
**paid_full_royalty** | Option<**bool**> |  | [optional]
**fee_breakdown** | Option<[**Vec<models::Model56>**](Model56.md)> | `kind` can be `marketplace` or `royalty` | [optional]
**is_deleted** | Option<**bool**> |  | [optional]
**created_at** | Option<**String**> | Time when added to indexer | [optional]
**updated_at** | Option<**String**> | Time when updated in indexer | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


