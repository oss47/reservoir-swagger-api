# Model214

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection_id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**is_spam** | Option<**bool**> |  | [optional][default to false]
**metadata_disabled** | Option<**bool**> |  | [optional][default to false]
**slug** | Option<**String**> |  | [optional]
**all_time_volume** | Option<**f64**> |  | [optional]
**floor_ask_price** | Option<[**models::Model213**](Model213.md)> |  | [optional]
**opensea_verification_status** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


