# Model51

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<[**models::Model50**](Model50.md)> |  | [optional]
**maker** | Option<**String**> |  | [optional]
**taker** | Option<**String**> |  | [optional]
**amount** | Option<**String**> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**timestamp** | Option<**f64**> |  | [optional]
**price** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


