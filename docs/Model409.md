# Model409

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | 
**attribute_count** | Option<**f64**> |  | [optional]
**kind** | **String** |  | 
**min_range** | Option<**f64**> |  | [optional]
**max_range** | Option<**f64**> |  | [optional]
**values** | Option<[**Vec<models::Model407>**](Model407.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


