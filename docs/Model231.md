# Model231

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> | Possible types returned: `ask`, `ask_cancel`, `bid`, `bid_cancel`, `sale`, `mint, and `transfer`. | [optional]
**from_address** | Option<**String**> |  | [optional]
**to_address** | Option<**String**> |  | [optional]
**price** | Option<[**crate::models::Model110**](Model110.md)> |  | [optional]
**amount** | Option<**f32**> |  | [optional]
**timestamp** | Option<**f32**> | Time when added on the blockchain. | [optional]
**contract** | Option<**String**> |  | [optional]
**token** | Option<[**crate::models::Model229**](Model229.md)> |  | [optional]
**collection** | Option<[**crate::models::Model230**](Model230.md)> |  | [optional]
**tx_hash** | Option<**String**> | Txn hash from the blockchain. | [optional]
**log_index** | Option<**f32**> |  | [optional]
**batch_index** | Option<**f32**> |  | [optional]
**fill_source** | Option<[**crate::models::Source**](source.md)> |  | [optional]
**order** | Option<[**crate::models::Model113**](Model113.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


