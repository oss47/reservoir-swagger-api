# GetTransactionSyncedV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**synced** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


