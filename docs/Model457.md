# Model457

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days** | Option<**i32**> | If no days are passed, will automatically resync from beginning of time. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


