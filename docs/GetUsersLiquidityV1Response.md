# GetUsersLiquidityV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**liquidity** | Option<[**Vec<models::Model166>**](Model166.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


