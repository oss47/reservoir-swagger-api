# Model622

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**image_url** | Option<**String**> |  | [optional]
**twitter_url** | Option<**String**> |  | [optional]
**discord_url** | Option<**String**> |  | [optional]
**external_url** | Option<**String**> |  | [optional]
**magiceden_verification_status** | Option<**String**> |  | [optional]
**royalties** | Option<[**Vec<models::Model620>**](Model620.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


