# Model491

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collections** | **Vec<String>** | Update to one or more collections. Max limit is 50. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | 
**nsfw** | Option<**bool**> | API to update the nsfw status of a collection | [optional][default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


