# Model289

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**last_buy** | Option<[**crate::models::LastBuy**](lastBuy.md)> |  | [optional]
**last_sell** | Option<[**crate::models::LastBuy**](lastBuy.md)> |  | [optional]
**rarity_score** | Option<**f32**> |  | [optional]
**rarity_rank** | Option<**f32**> |  | [optional]
**media** | Option<**String**> |  | [optional]
**collection** | Option<[**crate::models::Model287**](Model287.md)> |  | [optional]
**top_bid** | Option<[**crate::models::Model288**](Model288.md)> |  | [optional]
**last_appraisal_value** | Option<**f32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


