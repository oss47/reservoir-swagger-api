# \XAdminApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_admin_getapikey_key**](XAdminApi.md#get_admin_getapikey_key) | **GET** /admin/get-api-key/{key} | Get the associated info for the given API key
[**get_admin_getmarketplaces**](XAdminApi.md#get_admin_getmarketplaces) | **GET** /admin/get-marketplaces | Get supported marketplaces
[**get_admin_openapi**](XAdminApi.md#get_admin_openapi) | **GET** /admin/open-api | Get swagger json in OpenApi V3
[**get_admin_providermetadata_type**](XAdminApi.md#get_admin_providermetadata_type) | **GET** /admin/provider-metadata/{type} | Get metadata for a token or collection
[**get_admin_ratelimitrules**](XAdminApi.md#get_admin_ratelimitrules) | **GET** /admin/rate-limit-rules | Get rate limit rules
[**get_assets_v1**](XAdminApi.md#get_assets_v1) | **GET** /assets/v1 | Return the asset based on the given param
[**get_search_activities_v1**](XAdminApi.md#get_search_activities_v1) | **GET** /search/activities/v1 | Search activity
[**post_admin_apikeys_metrics**](XAdminApi.md#post_admin_apikeys_metrics) | **POST** /admin/api-keys/metrics | Get API usage metrics for the given API key
[**post_admin_calcrarity**](XAdminApi.md#post_admin_calcrarity) | **POST** /admin/calc-rarity | Trigger calculation of the give collection tokens rarity
[**post_admin_createratelimitrule**](XAdminApi.md#post_admin_createratelimitrule) | **POST** /admin/create-rate-limit-rule | Create rate limit
[**post_admin_deleteratelimitrule**](XAdminApi.md#post_admin_deleteratelimitrule) | **POST** /admin/delete-rate-limit-rule | Delete the rate limit with the given ID
[**post_admin_fixblocks**](XAdminApi.md#post_admin_fixblocks) | **POST** /admin/fix-blocks | Trigger fixing any orphaned block.
[**post_admin_fixcache**](XAdminApi.md#post_admin_fixcache) | **POST** /admin/fix-cache | Trigger fixing any cache inconsistencies for array of contracts.
[**post_admin_fixorders**](XAdminApi.md#post_admin_fixorders) | **POST** /admin/fix-orders | Trigger fixing any order inconsistencies.
[**post_admin_fixtokencache**](XAdminApi.md#post_admin_fixtokencache) | **POST** /admin/fix-token-cache | Trigger fixing any cache inconsistencies for specific token.
[**post_admin_indexmetadata**](XAdminApi.md#post_admin_indexmetadata) | **POST** /admin/index-metadata | Trigger metadata indexing for a token's collection
[**post_admin_pauserabbitqueue**](XAdminApi.md#post_admin_pauserabbitqueue) | **POST** /admin/pause-rabbit-queue | Pause rabbit queue
[**post_admin_refreshcollection**](XAdminApi.md#post_admin_refreshcollection) | **POST** /admin/refresh-collection | Refresh a collection's orders and metadata
[**post_admin_refreshtoken**](XAdminApi.md#post_admin_refreshtoken) | **POST** /admin/refresh-token | Refresh a token's orders and metadata
[**post_admin_resumerabbitqueue**](XAdminApi.md#post_admin_resumerabbitqueue) | **POST** /admin/resume-rabbit-queue | Resume rabbit queue
[**post_admin_resyncapikey**](XAdminApi.md#post_admin_resyncapikey) | **POST** /admin/resync-api-key | Trigger a resync from mainnet to all other chains of the given api key.
[**post_admin_resyncfloorevents**](XAdminApi.md#post_admin_resyncfloorevents) | **POST** /admin/resync-floor-events | Trigger fixing any floor events inconsistencies for any particular collection.
[**post_admin_resyncnftbalances**](XAdminApi.md#post_admin_resyncnftbalances) | **POST** /admin/resync-nft-balances | Trigger the recalculation of nft balances for tokens transferred in any particular block range
[**post_admin_resyncsaleroyalties**](XAdminApi.md#post_admin_resyncsaleroyalties) | **POST** /admin/resync-sale-royalties | Trigger the recalculation of sale royalties for any particular block range.
[**post_admin_resyncsource**](XAdminApi.md#post_admin_resyncsource) | **POST** /admin/resync-source | Trigger re-syncing of specific source domain
[**post_admin_resyncuserbalance**](XAdminApi.md#post_admin_resyncuserbalance) | **POST** /admin/resync-user-balance | Trigger the recalculation of user in certain collection
[**post_admin_retryrabbitqueue**](XAdminApi.md#post_admin_retryrabbitqueue) | **POST** /admin/retry-rabbit-queue | Retry all the messages within the given dead letter rabbit queue
[**post_admin_revalidatemint**](XAdminApi.md#post_admin_revalidatemint) | **POST** /admin/revalidate-mint | Revalidate an existing mint
[**post_admin_revalidateorder**](XAdminApi.md#post_admin_revalidateorder) | **POST** /admin/revalidate-order | Revalidate an existing order
[**post_admin_routers**](XAdminApi.md#post_admin_routers) | **POST** /admin/routers | Add a new router contract
[**post_admin_setcommunity**](XAdminApi.md#post_admin_setcommunity) | **POST** /admin/set-community | Set a community for a specific collection
[**post_admin_setindexingmethod**](XAdminApi.md#post_admin_setindexingmethod) | **POST** /admin/set-indexing-method | Set the tokens indexing method for all tokens in certain collection
[**post_admin_syncdailyvolumes**](XAdminApi.md#post_admin_syncdailyvolumes) | **POST** /admin/sync-daily-volumes | Trigger a re-sync of daily volume calculations, volumes should only be calculated when fill_events have been fully synced
[**post_admin_syncevents**](XAdminApi.md#post_admin_syncevents) | **POST** /admin/sync-events | Trigger syncing of events.
[**post_admin_triggerjob**](XAdminApi.md#post_admin_triggerjob) | **POST** /admin/trigger-job | Trigger bullmq job
[**post_admin_triggerrabbitjob**](XAdminApi.md#post_admin_triggerrabbitjob) | **POST** /admin/trigger-rabbit-job | Trigger rabbit job
[**post_admin_updateapikey**](XAdminApi.md#post_admin_updateapikey) | **POST** /admin/update-api-key | Update the given api key
[**post_admin_updateimageversion**](XAdminApi.md#post_admin_updateimageversion) | **POST** /admin/update-image-version | Increment the metadata version for a collection to bust the cache
[**post_admin_updateratelimitrule**](XAdminApi.md#post_admin_updateratelimitrule) | **POST** /admin/update-rate-limit-rule | Update the rate limit for the given ID
[**post_admin_updatesource**](XAdminApi.md#post_admin_updatesource) | **POST** /admin/update-source | Trigger re-syncing of specific source domain
[**post_apikeys**](XAdminApi.md#post_apikeys) | **POST** /api-keys | Generate API Key
[**put_collections_collection_community_v1**](XAdminApi.md#put_collections_collection_community_v1) | **PUT** /collections/{collection}/community/v1 | Set a community for a specific collection



## get_admin_getapikey_key

> models::GetApiKeyRateLimitsResponse get_admin_getapikey_key(x_admin_api_key, key)
Get the associated info for the given API key

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**key** | **String** | The API key | [required] |

### Return type

[**models::GetApiKeyRateLimitsResponse**](getApiKeyRateLimitsResponse.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_admin_getmarketplaces

> models::GetMarketplacesv1Resp get_admin_getmarketplaces()
Get supported marketplaces

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::GetMarketplacesv1Resp**](getMarketplacesv1Resp.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_admin_openapi

> String get_admin_openapi()
Get swagger json in OpenApi V3

### Parameters

This endpoint does not need any parameter.

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_admin_providermetadata_type

> String get_admin_providermetadata_type(x_admin_api_key, r#type, tokens, method)
Get metadata for a token or collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**r#type** | **String** | Fetch metadata for a token or collection | [required] |
**tokens** | [**Vec<String>**](String.md) | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` | [required] |
**method** | Option<**String**> | The indexing method to use |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_admin_ratelimitrules

> String get_admin_ratelimitrules(x_admin_api_key, route)
Get rate limit rules

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**route** | Option<**String**> | The route to get rules for |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_assets_v1

> String get_assets_v1(asset)
Return the asset based on the given param

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**asset** | **String** |  | [required] |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_search_activities_v1

> models::GetSearchActivitiesV1Response get_search_activities_v1(tokens, collections, contracts_set_id, collections_set_id, community, attributes, sources, users, limit, sort_by, continuation, types, display_currency)
Search activity

This API can be used to build a feed for a collection including sales, asks, transfers, mints, bids, cancelled bids, and cancelled asks types.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**tokens** | Option<[**Vec<String>**](String.md)> | Array of tokens. Max limit is 50. Example: `tokens[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:704 tokens[1]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:979` |  |
**collections** | Option<[**Vec<String>**](String.md)> | Array of collections. Max limit is 50. Example: `collections[0]: 0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**contracts_set_id** | Option<**String**> | Filter to a particular contracts set. |  |
**collections_set_id** | Option<**String**> | Filter to a particular collection set. |  |
**community** | Option<**String**> | Filter to a particular community. Example: `artblocks` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/collections/activity/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original` or `https://api.reservoir.tools/collections/activity/v6?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attribute[Type]=Original&attribute[Type]=Sibling` |  |
**sources** | Option<[**Vec<String>**](String.md)> | Array of source domains. Max limit is 50. Example: `sources[0]: opensea.io` |  |
**users** | Option<[**Vec<String>**](String.md)> | Array of users addresses. Max is 50. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**limit** | Option<**i32**> | Amount of items returned. Max limit is 50 when `includedMetadata=true` otherwise max limit is 1000. |  |[default to 50]
**sort_by** | Option<**String**> | Order the items are returned in the response. The blockchain event time is `timestamp`. The event time recorded is `createdAt`. |  |[default to timestamp]
**continuation** | Option<**String**> | Use continuation token to request next offset of items. |  |
**types** | Option<[**Vec<String>**](String.md)> |  |  |
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetSearchActivitiesV1Response**](getSearchActivitiesV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_apikeys_metrics

> models::PostApiKeyMetricsResponse post_admin_apikeys_metrics(x_admin_api_key, keys, granularity, group_by, start_time, end_time)
Get API usage metrics for the given API key

Get API usage metrics for the given API key

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**keys** | [**Vec<String>**](String.md) | Array API keys | [required] |
**granularity** | Option<**String**> | Return results by either hourly/daily/monthly granularity.<br>Hourly will return time in format YYYY-MM-DDTHH:00:000Z<br>Daily will return time in format YYYY-MM-DDT00:00:000Z<br>Monthly will return time in format YYYY-MM-01T00:00:000Z<br> |  |[default to monthly]
**group_by** | Option<**f64**> | 1 - All calls per hour/day/month<br>2 - All calls per key per hour/day/month<br>3 - All calls per key per route per hour/day/month<br>4 - All calls per key per route per status code per hour/day/month<br> |  |[default to 1.0]
**start_time** | Option<**String**> | Get metrics after a particular time (allowed format YYYY-MM-DD HH:00)<br>Hourly default to last 24 hours<br>Daily default to last 7 days<br>Monthly default to last 12 months |  |
**end_time** | Option<**String**> | Get metrics before a particular time (allowed format YYYY-MM-DD HH:00) |  |

### Return type

[**models::PostApiKeyMetricsResponse**](postApiKeyMetricsResponse.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_calcrarity

> String post_admin_calcrarity(x_admin_api_key, body)
Trigger calculation of the give collection tokens rarity

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model430**](Model430.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_createratelimitrule

> String post_admin_createratelimitrule(x_admin_api_key, body)
Create rate limit

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model433**](Model433.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_deleteratelimitrule

> String post_admin_deleteratelimitrule(x_admin_api_key, body)
Delete the rate limit with the given ID

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model434**](Model434.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_fixblocks

> String post_admin_fixblocks(x_admin_api_key, body)
Trigger fixing any orphaned block.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model435**](Model435.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_fixcache

> String post_admin_fixcache(x_admin_api_key, body)
Trigger fixing any cache inconsistencies for array of contracts.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model436**](Model436.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_fixorders

> String post_admin_fixorders(x_admin_api_key, body)
Trigger fixing any order inconsistencies.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model437**](Model437.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_fixtokencache

> String post_admin_fixtokencache(x_admin_api_key, body)
Trigger fixing any cache inconsistencies for specific token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model438**](Model438.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_indexmetadata

> String post_admin_indexmetadata(x_admin_api_key, body)
Trigger metadata indexing for a token's collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model439**](Model439.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_pauserabbitqueue

> String post_admin_pauserabbitqueue(x_admin_api_key, body)
Pause rabbit queue

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model440**](Model440.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_refreshcollection

> String post_admin_refreshcollection(x_admin_api_key, body)
Refresh a collection's orders and metadata

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model441**](Model441.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_refreshtoken

> String post_admin_refreshtoken(x_admin_api_key, body)
Refresh a token's orders and metadata

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model442**](Model442.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_resumerabbitqueue

> String post_admin_resumerabbitqueue(x_admin_api_key, body)
Resume rabbit queue

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model443**](Model443.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_resyncapikey

> String post_admin_resyncapikey(x_admin_api_key, body)
Trigger a resync from mainnet to all other chains of the given api key.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model444**](Model444.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_resyncfloorevents

> String post_admin_resyncfloorevents(x_admin_api_key, body)
Trigger fixing any floor events inconsistencies for any particular collection.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model445**](Model445.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_resyncnftbalances

> String post_admin_resyncnftbalances(x_admin_api_key, body)
Trigger the recalculation of nft balances for tokens transferred in any particular block range

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model435**](Model435.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_resyncsaleroyalties

> String post_admin_resyncsaleroyalties(x_admin_api_key, body)
Trigger the recalculation of sale royalties for any particular block range.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model447**](Model447.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_resyncsource

> String post_admin_resyncsource(x_admin_api_key, body)
Trigger re-syncing of specific source domain

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model448**](Model448.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_resyncuserbalance

> String post_admin_resyncuserbalance(x_admin_api_key, body)
Trigger the recalculation of user in certain collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model449**](Model449.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_retryrabbitqueue

> String post_admin_retryrabbitqueue(x_admin_api_key, body)
Retry all the messages within the given dead letter rabbit queue

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model450**](Model450.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_revalidatemint

> String post_admin_revalidatemint(x_admin_api_key, body)
Revalidate an existing mint

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model451**](Model451.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_revalidateorder

> String post_admin_revalidateorder(x_admin_api_key, body)
Revalidate an existing order

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model452**](Model452.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_routers

> String post_admin_routers(x_admin_api_key, body)
Add a new router contract

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model454**](Model454.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_setcommunity

> String post_admin_setcommunity(x_admin_api_key, body)
Set a community for a specific collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model455**](Model455.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_setindexingmethod

> String post_admin_setindexingmethod(x_admin_api_key, body)
Set the tokens indexing method for all tokens in certain collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model456**](Model456.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_syncdailyvolumes

> String post_admin_syncdailyvolumes(x_admin_api_key, body)
Trigger a re-sync of daily volume calculations, volumes should only be calculated when fill_events have been fully synced

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model457**](Model457.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_syncevents

> String post_admin_syncevents(x_admin_api_key, body)
Trigger syncing of events.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model459**](Model459.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_triggerjob

> String post_admin_triggerjob(x_admin_api_key, body)
Trigger bullmq job

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model460**](Model460.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_triggerrabbitjob

> String post_admin_triggerrabbitjob(x_admin_api_key, body)
Trigger rabbit job

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model461**](Model461.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_updateapikey

> String post_admin_updateapikey(x_admin_api_key, body)
Update the given api key

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model462**](Model462.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_updateimageversion

> String post_admin_updateimageversion(x_admin_api_key, body)
Increment the metadata version for a collection to bust the cache

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model463**](Model463.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_updateratelimitrule

> String post_admin_updateratelimitrule(x_admin_api_key, body)
Update the rate limit for the given ID

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model464**](Model464.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_admin_updatesource

> String post_admin_updatesource(x_admin_api_key, body)
Trigger re-syncing of specific source domain

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**body** | Option<[**Model465**](Model465.md)> |  |  |

### Return type

**String**

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_apikeys

> models::GetNewApiKeyResponse post_apikeys(x_admin_api_key, app_name, email, website, tier)
Generate API Key

The API key can be used in every route, by setting it as a request header **x-api-key**.  <a href='https://docs.reservoir.tools/reference/getting-started'>Learn more</a> about API Keys and Rate Limiting

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_admin_api_key** | **String** |  | [required] |
**app_name** | **String** | The name of your app | [required] |
**email** | **String** | An e-mail address where you can be reached, in case of issues, to avoid service disruption | [required] |
**website** | **String** | The website of your project | [required] |
**tier** | Option<**f64**> |  |  |

### Return type

[**models::GetNewApiKeyResponse**](getNewApiKeyResponse.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## put_collections_collection_community_v1

> models::PutSetCollectionCommunityV1Response put_collections_collection_community_v1(x_api_key, collection, body)
Set a community for a specific collection

This API requires an administrator API for execution. Explore and try the `/collections-sets/v1` or `/contracts-sets/v1` endpoints. Please contact technical support with more questions.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**x_api_key** | **String** |  | [required] |
**collection** | **String** | Update community for a particular collection, e.g. `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63`. Requires an authorized api key to be passed. | [required] |
**body** | Option<[**Model429**](Model429.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

