# Model53

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Deprecated. Use `saleId` instead. | [optional]
**sale_id** | Option<**String**> | Unique identifier made from txn hash, price, etc. | [optional]
**token** | Option<[**crate::models::Model45**](Model45.md)> |  | [optional]
**order_source** | Option<**String**> |  | [optional]
**order_side** | Option<**String**> | Can be `ask` or `bid`. | [optional]
**order_kind** | Option<**String**> |  | [optional]
**order_id** | Option<**String**> |  | [optional]
**from** | Option<**String**> |  | [optional]
**to** | Option<**String**> |  | [optional]
**amount** | Option<**String**> |  | [optional]
**fill_source** | Option<**String**> |  | [optional]
**block** | Option<**f32**> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**log_index** | Option<**f32**> |  | [optional]
**batch_index** | Option<**f32**> |  | [optional]
**timestamp** | Option<**f32**> | Time added on the blockchain | [optional]
**price** | Option<[**crate::models::Price**](price.md)> |  | [optional]
**wash_trading_score** | Option<**f32**> |  | [optional]
**royalty_fee_bps** | Option<**f32**> |  | [optional]
**marketplace_fee_bps** | Option<**f32**> |  | [optional]
**paid_full_royalty** | Option<**bool**> |  | [optional]
**fee_breakdown** | Option<[**Vec<crate::models::Model51>**](Model51.md)> | `kind` can be `marketplace` or `royalty` | [optional]
**is_deleted** | Option<**bool**> |  | [optional]
**created_at** | Option<**String**> | Time when added to indexer | [optional]
**updated_at** | Option<**String**> | Time when updated in indexer | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


