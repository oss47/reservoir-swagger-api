# GetTokensV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokens** | Option<[**Vec<models::Model68>**](Model68.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


