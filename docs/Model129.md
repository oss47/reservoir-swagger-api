# Model129

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**slug** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**metadata** | Option<[**serde_json::Value**](.md)> |  | [optional]
**sample_images** | Option<**Vec<String>**> |  | [optional]
**token_count** | Option<**String**> |  | [optional]
**on_sale_count** | Option<**String**> |  | [optional]
**token_set_id** | Option<**String**> |  | [optional]
**royalties** | Option<[**crate::models::Royalties**](royalties.md)> |  | [optional]
**last_buy** | Option<[**crate::models::LastBuy**](lastBuy.md)> |  | [optional]
**last_sell** | Option<[**crate::models::LastBuy**](lastBuy.md)> |  | [optional]
**floor_ask** | Option<[**crate::models::FloorAsk**](floorAsk.md)> |  | [optional]
**top_bid** | Option<[**crate::models::TopBid**](topBid.md)> |  | [optional]
**rank** | Option<[**crate::models::Rank**](rank.md)> |  | [optional]
**volume** | Option<[**crate::models::Rank**](rank.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


