/*
 * Reservoir API
 *
 * You are viewing the reference docs for the Reservoir API.        For a more complete overview with guides and examples, check out the <a href='https://reservoirprotocol.github.io'>Reservoir Protocol Docs</a>.
 *
 * The version of the OpenAPI document: 5.296.1
 *
 * Generated by: https://openapi-generator.tech
 */

use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Model237 {
    #[serde(rename = "contract")]
    pub contract: String,
    #[serde(rename = "tokenId")]
    pub token_id: String,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "description", skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(rename = "image", skip_serializing_if = "Option::is_none")]
    pub image: Option<String>,
    #[serde(rename = "media", skip_serializing_if = "Option::is_none")]
    pub media: Option<String>,
    #[serde(rename = "kind", skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,
    #[serde(rename = "isFlagged", skip_serializing_if = "Option::is_none")]
    pub is_flagged: Option<bool>,
    #[serde(rename = "lastFlagUpdate", skip_serializing_if = "Option::is_none")]
    pub last_flag_update: Option<String>,
    #[serde(rename = "collection", skip_serializing_if = "Option::is_none")]
    pub collection: Option<Box<models::Model69>>,
    #[serde(rename = "lastBuy", skip_serializing_if = "Option::is_none")]
    pub last_buy: Option<Box<models::LastBuy>>,
    #[serde(rename = "lastSell", skip_serializing_if = "Option::is_none")]
    pub last_sell: Option<Box<models::LastBuy>>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "attributes", skip_serializing_if = "Option::is_none")]
    pub attributes: Option<Vec<models::Model235>>,
}

impl Model237 {
    pub fn new(contract: String, token_id: String) -> Model237 {
        Model237 {
            contract,
            token_id,
            name: None,
            description: None,
            image: None,
            media: None,
            kind: None,
            is_flagged: None,
            last_flag_update: None,
            collection: None,
            last_buy: None,
            last_sell: None,
            owner: None,
            attributes: None,
        }
    }
}
