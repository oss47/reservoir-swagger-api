# \MintTokensApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**post_execute_mint_v1**](MintTokensApi.md#post_execute_mint_v1) | **POST** /execute/mint/v1 | Mint tokens



## post_execute_mint_v1

> crate::models::PostExecuteMintV1Response post_execute_mint_v1(body)
Mint tokens

Use this API to mint tokens. We recommend using the SDK over this API as the SDK will iterate through the steps and return callbacks.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model513**](Model513.md)> |  |  |

### Return type

[**crate::models::PostExecuteMintV1Response**](postExecuteMintV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

