# Model516

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | Option<**String**> |  | [optional]
**contract** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**quantity** | Option<**f64**> |  | [optional]
**source** | Option<**String**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**quote** | Option<**f64**> |  | [optional]
**raw_quote** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


