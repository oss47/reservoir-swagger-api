# Model615

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<**String**> |  | [optional]
**result** | Option<**String**> |  | [optional]
**is_error** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


