# Model210

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | Option<**String**> |  | [optional]
**value** | **String** |  | 
**token_count** | Option<**f32**> |  | [optional]
**on_sale_count** | Option<**f32**> |  | [optional]
**floor_ask_price** | Option<**f32**> |  | [optional]
**top_bid_value** | Option<**f32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


