# GetSourcesV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sources** | Option<[**Vec<models::Model60>**](Model60.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


