# \StatsApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_collections_dailyvolumes_v1**](StatsApi.md#get_collections_dailyvolumes_v1) | **GET** /collections/daily-volumes/v1 | Daily collection volume
[**get_stats_v2**](StatsApi.md#get_stats_v2) | **GET** /stats/v2 | Stats



## get_collections_dailyvolumes_v1

> models::GetDailyVolumesV1Response get_collections_dailyvolumes_v1(id, limit, start_timestamp, end_timestamp)
Daily collection volume

Get date, volume, rank and sales count for each collection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` | [required] |
**limit** | Option<**f64**> | Amount of items returned in response. |  |[default to 60.0]
**start_timestamp** | Option<**f64**> | The start timestamp you want to filter on (UTC) |  |
**end_timestamp** | Option<**f64**> | The end timestamp you want to filter on (UTC) |  |

### Return type

[**models::GetDailyVolumesV1Response**](getDailyVolumesV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_stats_v2

> models::GetStatsV2Response get_stats_v2(collection, token, attributes, normalize_royalties, display_currency)
Stats

Get aggregate stats for a particular set (collection, attribute or single token)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**collection** | Option<**String**> | Filter to a particular collection with collection-id. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63` |  |
**token** | Option<**String**> | Filter to a particular token. Example: `0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63:123` |  |
**attributes** | Option<**String**> | Filter to a particular attribute. Attributes are case sensitive. Note: Our docs do not support this parameter correctly. To test, you can use the following URL in your browser. Example: `https://api.reservoir.tools/stats/v2?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original` or `https://api.reservoir.tools/stats/v2?collection=0x8d04a8c79ceb0889bdd12acdf3fa9d207ed3ff63&attributes[Type]=Original&attributes[Type]=Sibling` |  |
**normalize_royalties** | Option<**bool**> | If true, prices will include missing royalties to be added on-top. |  |[default to false]
**display_currency** | Option<**String**> | Input any ERC20 address to return result in given currency |  |

### Return type

[**models::GetStatsV2Response**](getStatsV2Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

