# Model216

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**param_1day** | Option<[**models::Price**](price.md)> |  | [optional]
**param_7day** | Option<[**models::Price**](price.md)> |  | [optional]
**param_30day** | Option<[**models::Price**](price.md)> |  | [optional]
**all_time** | Option<[**models::Price**](price.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


