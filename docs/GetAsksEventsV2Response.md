# GetAsksEventsV2Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | Option<[**Vec<models::Model148>**](Model148.md)> |  | [optional]
**continuation** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


