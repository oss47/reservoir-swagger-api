# Model483

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seaport_offers** | Option<[**Vec<models::Model482>**](Model482.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


