use async_graphql::scalar;

use crate::models::{
    Attribute, BulkData, Model407, Model409, Model411, Model443, Model472, Model495, Model529,
};

scalar!(Model495);
scalar!(Model411);
scalar!(Model409);
scalar!(Model407);
scalar!(Model443);
scalar!(Model472);
scalar!(Model529);
scalar!(Attribute);
scalar!(BulkData);
