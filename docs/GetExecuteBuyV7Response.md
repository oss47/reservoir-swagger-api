# GetExecuteBuyV7Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | Option<**String**> |  | [optional]
**steps** | Option<[**Vec<models::Model531>**](Model531.md)> |  | [optional]
**errors** | Option<[**Vec<models::Model533>**](Model533.md)> |  | [optional]
**path** | Option<[**Vec<models::Model537>**](Model537.md)> |  | [optional]
**max_quantities** | Option<[**Vec<models::Model539>**](Model539.md)> |  | [optional]
**fees** | Option<[**models::Model540**](Model540.md)> |  | [optional]
**gas_estimate** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


