# Model165

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**action** | **String** |  | 
**description** | **String** |  | 
**kind** | **String** |  | 
**items** | [**Vec<models::Model164>**](Model164.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


