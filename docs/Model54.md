# Model54

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**sale_id** | Option<**String**> |  | [optional]
**token** | Option<[**models::Model50**](Model50.md)> |  | [optional]
**order_source** | Option<**String**> |  | [optional]
**order_source_domain** | Option<**String**> |  | [optional]
**order_side** | Option<**String**> |  | [optional]
**order_kind** | Option<**String**> |  | [optional]
**from** | Option<**String**> |  | [optional]
**to** | Option<**String**> |  | [optional]
**amount** | Option<**String**> |  | [optional]
**fill_source** | Option<**String**> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**log_index** | Option<**f64**> |  | [optional]
**batch_index** | Option<**f64**> |  | [optional]
**timestamp** | Option<**f64**> |  | [optional]
**price** | Option<**f64**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**currency_price** | Option<**f64**> |  | [optional]
**usd_price** | Option<**f64**> |  | [optional]
**wash_trading_score** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


