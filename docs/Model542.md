# Model542

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | **String** | User requesting the calls | 
**txs** | [**Vec<models::Model541>**](Model541.md) | List of transactions to execute | 
**origin_chain_id** | **f64** | Origination chain id (where solver needs to get paid) | 
**source** | Option<**String**> | Filling source used for attribution. Example: `reservoir.market` | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


