# \MiscApi

All URIs are relative to *https://api.reservoir.tools*

Method | HTTP request | Description
------------- | ------------- | -------------
[**post_execute_authsignature_v1**](MiscApi.md#post_execute_authsignature_v1) | **POST** /execute/auth-signature/v1 | Attach a signature to an existing auth challenge
[**post_execute_call_v1**](MiscApi.md#post_execute_call_v1) | **POST** /execute/call/v1 | Make arbitrary same-chain and cross-chain calls via solver
[**post_execute_cancelsignature_v1**](MiscApi.md#post_execute_cancelsignature_v1) | **POST** /execute/cancel-signature/v1 | Off-chain cancel orders
[**post_execute_deposit_v1**](MiscApi.md#post_execute_deposit_v1) | **POST** /execute/deposit/v1 | Deposit funds to the solver
[**post_execute_permitsignature_v1**](MiscApi.md#post_execute_permitsignature_v1) | **POST** /execute/permit-signature/v1 | Attach a signature to an existing permit
[**post_execute_presignature_v1**](MiscApi.md#post_execute_presignature_v1) | **POST** /execute/pre-signature/v1 | Attach a signature to an existing pre-signature
[**post_execute_results_v1**](MiscApi.md#post_execute_results_v1) | **POST** /execute/results/v1 | Send the success status of an execution
[**post_execute_solve_capacity_v1**](MiscApi.md#post_execute_solve_capacity_v1) | **POST** /execute/solve/capacity/v1 | Get the capacity for indirect filling via a solver
[**post_execute_solve_v1**](MiscApi.md#post_execute_solve_v1) | **POST** /execute/solve/v1 | Indirectly fill an order via a solver
[**post_execute_status_v1**](MiscApi.md#post_execute_status_v1) | **POST** /execute/status/v1 | Get the status of an execution



## post_execute_authsignature_v1

> models::PostAuthSignatureV1Response post_execute_authsignature_v1(signature, body)
Attach a signature to an existing auth challenge

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | **String** | Signature to attach to the auth challenge | [required] |
**body** | Option<[**Model495**](Model495.md)> |  |  |

### Return type

[**models::PostAuthSignatureV1Response**](postAuthSignatureV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_call_v1

> models::PostExecuteCallV1Response post_execute_call_v1(body)
Make arbitrary same-chain and cross-chain calls via solver

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model542**](Model542.md)> |  |  |

### Return type

[**models::PostExecuteCallV1Response**](postExecuteCallV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_cancelsignature_v1

> models::PutSetCollectionCommunityV1Response post_execute_cancelsignature_v1(signature, auth, body)
Off-chain cancel orders

If your order was created using the Seaport Oracle to allow off chain & gasless cancellations, you can just use the Kit's cancel modals, SDK's `cancelOrder`, or `/execute/cancel/`. Those tools will automatically access this endpoint for an oracle cancellation without you directly calling this endpoint.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | Option<**String**> | Cancellation signature |  |
**auth** | Option<**String**> | Optional auth token used instead of the signature |  |
**body** | Option<[**Model554**](Model554.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_deposit_v1

> models::PostExecuteDepositV1Response post_execute_deposit_v1(body)
Deposit funds to the solver

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model555**](Model555.md)> |  |  |

### Return type

[**models::PostExecuteDepositV1Response**](postExecuteDepositV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_permitsignature_v1

> models::PutSetCollectionCommunityV1Response post_execute_permitsignature_v1(signature, body)
Attach a signature to an existing permit

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | **String** | Signature to attach to the permit | [required] |
**body** | Option<[**Model578**](Model578.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_presignature_v1

> models::PutSetCollectionCommunityV1Response post_execute_presignature_v1(signature, body)
Attach a signature to an existing pre-signature

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | **String** | Signature to attach to the pre-signature | [required] |
**body** | Option<[**Model579**](Model579.md)> |  |  |

### Return type

[**models::PutSetCollectionCommunityV1Response**](putSetCollectionCommunityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_results_v1

> models::PostExecuteResultsV1Response post_execute_results_v1(body)
Send the success status of an execution

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model580**](Model580.md)> |  |  |

### Return type

[**models::PostExecuteResultsV1Response**](postExecuteResultsV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_solve_capacity_v1

> models::PostExecuteSolveCapacityV1Response post_execute_solve_capacity_v1(body)
Get the capacity for indirect filling via a solver

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model623**](Model623.md)> |  |  |

### Return type

[**models::PostExecuteSolveCapacityV1Response**](postExecuteSolveCapacityV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_solve_v1

> models::PostExecuteSolveV1Response post_execute_solve_v1(signature, body)
Indirectly fill an order via a solver

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | Option<**String**> | Signature for the solve request |  |
**body** | Option<[**Model595**](Model595.md)> |  |  |

### Return type

[**models::PostExecuteSolveV1Response**](postExecuteSolveV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## post_execute_status_v1

> models::PostExecuteStatusV1Response post_execute_status_v1(body)
Get the status of an execution

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | Option<[**Model596**](Model596.md)> |  |  |

### Return type

[**models::PostExecuteStatusV1Response**](postExecuteStatusV1Response.md)

### Authorization

[API_KEY](../README.md#API_KEY)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

