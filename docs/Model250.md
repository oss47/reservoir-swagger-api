# Model250

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**f32**> |  | [optional]
**kind** | Option<**String**> |  | [optional]
**previous_price** | Option<**f32**> |  | [optional]
**tx_hash** | Option<**String**> |  | [optional]
**tx_timestamp** | Option<**f32**> | Time when added on the blockchain. | [optional]
**created_at** | Option<**String**> | Time when added to indexer | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


