# Model130

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**timestamp** | Option<**f64**> |  | [optional]
**volume** | Option<**f64**> |  | [optional]
**rank** | Option<**f64**> |  | [optional]
**floor_sell_value** | Option<**f64**> | Native currency to chain. | [optional]
**sales_count** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


