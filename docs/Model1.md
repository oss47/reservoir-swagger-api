# Model1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | Option<**String**> |  | [optional]
**image_url** | Option<**String**> |  | [optional]
**fee** | Option<[**models::Fee**](fee.md)> |  | [optional]
**fee_bps** | Option<**f64**> |  | [optional]
**orderbook** | Option<**String**> |  | [optional]
**order_kind** | Option<**String**> |  | [optional]
**listing_enabled** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


