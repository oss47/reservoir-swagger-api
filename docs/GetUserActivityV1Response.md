# GetUserActivityV1Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**continuation** | Option<**f64**> |  | [optional]
**activities** | Option<[**Vec<models::Model359>**](Model359.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


