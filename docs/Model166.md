# Model166

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | Option<**String**> |  | [optional]
**rank** | **f64** |  | 
**token_count** | **String** |  | 
**liquidity** | **f64** |  | 
**max_top_buy_value** | **f64** |  | 
**weth_balance** | **f64** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


